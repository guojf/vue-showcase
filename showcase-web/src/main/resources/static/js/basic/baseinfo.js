$(function () {
	var app = new Vue({
		el:'#app',
		data:{
			isCollapse: true,
			queryIng : false,//正在查询
			queryData:{activeFlag:'1',page:1,rows:10},//查询条件
			datagrid:[],//数据列表数据
			totalNum:0,//分页控件配置
			data:[],
			isEdit:false,
			defaultCheckedKeys:[],
	        defaultProps: {
	            children: 'children',
	            label: 'text',
	            value: "id"
	        }
			,addData:{orderNum:1}
			,dialogConfig:{
				formLabelWidth:'120px'
				,dialogFormVisible:false
			}
			,rules:{
				parentCode:[
					{ required: true, message: '请选择父节点', trigger: 'change' }
				]
				,itemName:[
					{ required: true, message: '请输入名称', trigger: 'blur' }
				]
				,orderNum:[
					{ required: true, message: '请输入序号', trigger: 'blur' }
				]
			}
			,selectedRow:null
			,cascaderValue:[]
		},
		methods: {
			init:function () {
				var vm = this
				vm.query();
				vm.queryLeftTree();
				
			}
			,queryLeftTree:function(){
				$.ajax({url :staticURL+'/basic/baseInfoAction/findTrees',
					success : function(json) {
						app.data = json;
					}
				});
			}
			,query:function () {
				var vm = this
				if(vm.queryIng){
					return;
				}
				vm.queryIng = true;
				$.ajax({
					url :staticURL+'/basic/baseInfoAction/datagrid',
					data : vm.queryData,
					dataType : 'json',
					success : function(json) {
						app.datagrid = json.rows;
						app.totalNum = json.total;
						app.queryIng = false;
						app.selectedRow = null;
					}
				});
			}
			,addItem:function(){
				app.openDialog();
			}
			,deleteItem:function(){
				if(!app.selectedRow){
					app.$notify({showClose: true,
				          message: '请选择一条要删除的记录',
				          type: 'warning',
				          position: 'bottom-right'});
				}else{
					app.$confirm('是否确认删除操作?', '提示', {
			          confirmButtonText: '确定',
			          cancelButtonText: '取消',
			          type: 'warning'
			        }).then(function () {
			        	$.ajax({
							url : staticURL+"/basic/baseInfoAction/delete?activeFlag=0&id="+app.selectedRow.id,
							dataType : 'json',
							success : function(r) {
								if (r.success) {
									app.$notify({showClose: true,
								          message: r.msg,
								          type: 'success',
								          position: 'bottom-right'});
									app.query();
									app.queryLeftTree();
								} else {
									app.$notify({showClose: true,
								          message: r.msg,
								          type: 'error',
								          position: 'bottom-right'});
								}
							}
						});
			        }).catch(function (action) {
			        	  //取消
			        });
				}
				return;
				
			}
			,editItem:function(){
				if(!app.selectedRow){
					app.$notify({showClose: true,
				          message: '请选择一条要编辑的记录',
				          type: 'warning',
				          position: 'bottom-right'});
				}else{
					app.openDialog(app.selectedRow);
					app.setCascaderValue();
				}
			}
			,handleSizeChange:function(val){
				app.queryData.rows=val;
				app.query();
			}
			,handleCurrentChange:function(val){
				app.queryData.page=val;
				app.query();
			}
			,handlePrevClick:function(val){
				app.queryData.page=val;
				app.query();
			}
			,handleNextClick:function(val){
				app.queryData.page=val;
				app.query();
			}
			,handleTreeCheckChange:function(data){
				app.queryData.parentCode = data.id;
				app.query();
			}
			,cascaderChange:function(data){
				//data是一个数组，取最后一个
				app.addData.parentCode = data[data.length-1]
			}
			,resetForm:function(formName) {
				if(app.$refs[formName]){
					app.$refs[formName].resetFields();
				}
				if(formName=='dialogForm'){
					app.dialogConfig.dialogFormVisible=false;
				}
		    }
			,submitForm:function(formName){
				app.$refs[formName].validate(function(res){
					if(res){
						app.getCascaderLable();
						//验证通过
						$.ajax({
							url : app.saveUrl,
							data : app.addData,
							dataType : 'json',
							success : function(r) {
								if (r.success) {
									app.dialogConfig.dialogFormVisible=false;
									app.$notify({showClose: true,
								          message: r.msg,
								          type: 'success',
								          position: 'bottom-right'});
									app.query();
									app.queryLeftTree();
								} else {
									app.$notify({showClose: true,
								          message: r.msg,
								          type: 'error',
								          position: 'bottom-right'});
								}
							}
						});
					}
				});
			}
			,openDialog:function(param){
				if(param){
					app.isEdit = true;
					app.addData = JSON.parse(JSON.stringify(param));
				}else{
					app.isEdit = false;
					app.addData = {orderNum:1};
				}
				//显示dialog
				app.dialogConfig.dialogFormVisible=true;
				//重置form
				if(app.$refs['dialogForm']){
					app.$refs['dialogForm'].resetFields();
				}
			}
			,datagridSelect:function(row, event, column){
				app.selectedRow = row;
			}
			,getCascaderLable:function(){
				let data = app.data;
				app.addData.parentName = app.getNextCascaderLable(0,data);
			}
			,getNextCascaderLable:function(i,data){
				let cascaderValue = app.cascaderValue;
				for(var j=0;j<data.length;j++){
					console.log(cascaderValue[i],data[j])
					if(cascaderValue[i]==data[j].id){
						if(i == (cascaderValue.length-1)){
							return data[j].text;
						}else{
							app.getNextCascaderLable(cascaderValue[i+1],data[j].children);
						}
					}
				}
			}
			,setCascaderValue:function(){
				var a = new Array();
				var app = this;
				if(app.addData && app.addData.parentCode){
					var length = app.addData.parentCode.length;
					if(length>=1){
						a.push(app.addData.parentCode.substring(0,1));
					}
					if(length>=3){
						a.push(app.addData.parentCode.substring(0,3));
					}
					if(length>=5){
						a.push(app.addData.parentCode.substring(0,5));
					}
					if(length>=7){
						a.push(app.addData.parentCode.substring(0,7));
					}
				}
				app.cascaderValue = a;
			}
		},
		computed:{
			saveUrl:function(){
				if(app.isEdit){
					return staticURL+'/basic/baseInfoAction/edit'
				}else{
					return staticURL+'/basic/baseInfoAction/add'
				}
			}
			/*,cascaderValue:function(){
				var a = new Array();
				var app = this;
				if(app.addData && app.addData.parentCode){
					var length = app.addData.parentCode.length;
					if(length>=1){
						a.push(app.addData.parentCode.substring(0,1));
					}
					if(length>=3){
						a.push(app.addData.parentCode.substring(0,3));
					}
					if(length>=5){
						a.push(app.addData.parentCode.substring(0,5));
					}
					if(length>=7){
						a.push(app.addData.parentCode.substring(0,7));
					}
				}
				return a;
			}*/
		}
		,created: function () {
			this.init();
		},
		updated:function(){
		}
	});
});