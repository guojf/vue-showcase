$(function () {
	var app = new Vue({
		el:'#app',
		data:{
			isCollapse: true
			,queryIng : false//正在查询
			,queryData:{activeFlag:'1',page:1,rows:10}//查询条件
			,datagrid:[]//数据列表数据
			,totalNum:0//分页控件配置
			,data:[]
			,defaultCheckedKeys:[]
			,addData:{}
			,selectedRow:null
			,isEdit:false
			,rules:{
				name: [{ required: true, message: '请输入名称', trigger: 'change' }]
				,description: [{ required: true, message: '请输入描述', trigger: 'change' }]

			}
			,dialogConfig:{
				formLabelWidth:'120px'
				,dialogFormVisible:false
			}
			,treeData:[]
	        ,defaultProps: {
	            children: 'children',
	            label: 'text'
	        }
		},
		methods: {
			init:function () {
				var vm = this
				vm.query();
				vm.queryTreeData();
			}
			,queryTreeData:function (){
				var vm = this
				var treeUrl = staticURL+'/basic/resourceInfo/resourceTree'
				$.ajax({url :treeUrl,
					cache : false,
					success : function(json) {
						vm.treeData = json;
					}
				});
			}
			,query:function () {
				var vm = this
				if(vm.queryIng){
					return;
				}
				vm.queryIng = true;
				$.ajax({
					url :staticURL+'/basic/roleInfo/datagrid',
					data : vm.queryData,
					dataType : 'json',
					success : function(json) {
						app.datagrid = json.rows;
						app.totalNum = json.total;
						app.queryIng = false;
						app.selectedRow = null;
					}
				});
			}
			,addItem:function(){
				app.openDialog();
			}
			,deleteItem:function(){
				if(!app.selectedRow){
					app.$notify({showClose: true,
				          message: '请选择一条要删除的记录',
				          type: 'warning',
				          position: 'bottom-right'});
				}else{
					app.$confirm('是否确认删除操作?', '提示', {
			          confirmButtonText: '确定',
			          cancelButtonText: '取消',
			          type: 'warning'
			        }).then(function () {
			        	$.ajax({
							url : staticURL+"/basic/roleInfo/delete?ids="+app.selectedRow.id,
							dataType : 'json',
							success : function(r) {
								if (r.success) {
									app.$notify({showClose: true,
								          message: r.msg,
								          type: 'success',
								          position: 'bottom-right'});
									app.query();
								} else {
									app.$notify({showClose: true,
								          message: r.msg,
								          type: 'error',
								          position: 'bottom-right'});
								}
							}
						});
			        }).catch(function (action) {
			        	  //取消
			        });
				}
				return;
				
			}
			,editItem:function(){
				if(!app.selectedRow){
					app.$notify({showClose: true,
				          message: '请选择一条要编辑的记录',
				          type: 'warning',
				          position: 'bottom-right'});
				}else{
					$.ajax({url :staticURL+"/basic/roleInfo/showDesc",
						data : {
							id : app.selectedRow.id
						},
						dataType : 'json',
						cache : false,
						async : false,
						success : function(json) {
							if(json.upRsIds){
								app.defaultCheckedKeys = json.upRsIds;
							}
						}
					});
					app.openDialog(app.selectedRow);
				}
			}
			,handleSizeChange:function(val){
				app.queryData.rows=val;
				app.query();
			}
			,handleCurrentChange:function(val){
				app.queryData.page=val;
				app.query();
			}
			,handlePrevClick:function(val){
				app.queryData.page=val;
				app.query();
			}
			,handleNextClick:function(val){
				app.queryData.page=val;
				app.query();
			}
			,handleTreeCheckChange:function(data){
				app.queryData.parentCode = data.id;
				app.query();
			}
			,resetForm:function(formName) {
				app.$refs[formName].resetFields();
		    }
			,submitForm:function(formName){
				app.$refs[formName].validate(function(res){
					if(res){
						app.userResouces();
						//验证通过
						$.ajax({
							url : app.saveUrl,
							data : app.addData,
							dataType : 'json',
							success : function(r) {
								if (r.success) {
									app.dialogConfig.dialogFormVisible=false;
									app.$notify({showClose: true,
								          message: r.msg,
								          type: 'success',
								          position: 'bottom-right'});
									app.query();
								} else {
									app.$notify({showClose: true,
								          message: r.msg,
								          type: 'error',
								          position: 'bottom-right'});
								}
							}
						});
					}
				});
			}
			,userResouces:function(){
				 var checkedNodes = this.$refs.tree.getCheckedNodes(false,true); //获取末级且选中的checkbox原dom对象，返回的类型:Array
				 var array = [];
		         for (var i = 0; i < checkedNodes.length; i++) {
		              array.push(checkedNodes[i].id);
		         }
		         this.addData.rsIds = array.join(',');
			}
			,openDialog:function(param){
				if(param){
					app.isEdit = true;
					//这里如下操作，而不是 app.addData = param,是为了实现JSON对象的复制
					//如果app.addData = param， 当closeDialog()时，dialogForm 被reset
					//app.addDatat同时被reset,会导致bug
					app.addData = JSON.parse(JSON.stringify(param)); 
				}else{
					app.isEdit = false;
					app.addData = {orderNum:1};
				}
				//显示dialog
				app.dialogConfig.dialogFormVisible=true;
				//重置form
				if(app.$refs['dialogForm']){
					app.$refs['dialogForm'].resetFields();
				}
				if(app.$refs.tree){
					app.$refs.tree.setCheckedKeys([]);
				}
			}
			,closeDialog:function(){
				app.dialogConfig.dialogFormVisible=false;
				app.resetForm('dialogForm')
			}
			,datagridSelect:function(row, event, column){
				app.selectedRow = row;
			}
		},
		computed:{
			saveUrl:function(){
				if(app.isEdit){
					return staticURL+'/basic/roleInfo/edit'
				}else{
					return staticURL+'/basic/roleInfo/add'
				}
			}
		}
		,created: function () {
			this.init();
		},
		updated:function(){
		}
	});
});