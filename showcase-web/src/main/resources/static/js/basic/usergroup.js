$(function () {
	var app = new Vue({
		el:'#app'
		,data:{
			selectedRow:null
			,datagrid:[]
			,groupId:$("#groupId").val()
			,isAdmin:$("#isAdmin").val()
			,userSelectDialogVisible:false
		}
		,methods:{
			init:function(){
				this.query();
			}
			,goBack:function(){
				parent._goBack()
			}
			,datagridSelect:function(row, event, column){
				app.selectedRow = row;
			}
			,query:function () {
				var groupId = this.groupId;
				$.ajax({
					url :staticURL+'/basic/userGroup/datagrid?rows=999&groupId='+groupId,
					dataType : 'json',
					success : function(json) {
						app.datagrid = json.rows;
						app.selectedRow = null;
					}
				});
			}
			,addUserToGroup:function(){
				parent._openUserDialog();
			}
			,selectUser:function(row, event, column){
				var user = {};
				user.userId = row.id;
				user.groupId = app.groupId;
				user.isAdmin = '0';
				$.ajax({
					url : staticURL+"/basic/userGroup/add",
					data : user,
					dataType : 'json',
					success : function(r) {
						console.log(r);
						if (r.success) {
							app.query();
							app.userSelectDialogVisible = false;
							
						}
						app.$notify({showClose: true,
					          message: r.msg,
					          type: 'info',
					          position: 'top-right'});
					}
				});
			}
			,removeUser:function(){
				if(!app.selectedRow){
					return;
				}
				app.$confirm('是否确认删除操作?', '提示', {
			          confirmButtonText: '确定',
			          cancelButtonText: '取消',
			          type: 'warning'
			        }).then(function () {
			        	$.ajax({
							url : staticURL+"/basic/userGroup/delete",
							data : JSON.stringify(app.selectedRow),
							contentType:"application/json",
							success : function(r) {
								if (r.success) {
									app.$notify({showClose: true,
								          message: r.msg,
								          type: 'success',
								          position: 'bottom-right'});
									app.query();
								} else {
									app.$notify({showClose: true,
								          message: r.msg,
								          type: 'error',
								          position: 'bottom-right'});
								}
							}
						});
		        }).catch(function (action) {
		        	  //取消
		        });
			}
			,setAdmin:function(data){
				if(!app.selectedRow){
					return;
				}
				app.selectedRow.isAdmin = data;
				$.ajax({
					url : staticURL+'/basic/userGroup/edit',
					data : JSON.stringify(app.selectedRow),
					contentType:"application/json",
					success : function(r) {
						if (r.success) {
							app.$notify({showClose: true,
						          message: r.msg,
						          type: 'success',
						          position: 'bottom-right'});
							app.query();
						} else {
							app.$notify({showClose: true,
						          message: r.msg,
						          type: 'error',
						          position: 'bottom-right'});
						}
						app.query();
					}
				});
			}
		}
		,created:function () {
			this.init();
			window._selectUser = this.selectUser;
		}
	});
});