$(function () {
	var app = new Vue({
		el:'#app'
		,components:{
			'roleDialog': roleDialogComponentJs
		}
		,data:function(){
			return{
				 table:{}
				,form:{}
				,queryData:{}
				,groupAddIndex:''
				,saveUrl:''
				,groupAddId:''
				,groupId:$("#groupId").val()
				,isAdmin:$("#isAdmin").val()
				,datagrid:[]
				,selectedRow:null
				,roleDialogVisible:false
			}
		},
		methods: {
			init:function () {
				this.query();
			}
			,query:function () {
				var groupId = this.groupId;
				$.ajax({
					url :staticURL+'/basic/roleGroup/datagrid?rows=999&groupId='+groupId,
					dataType : 'json',
					success : function(json) {
						app.datagrid = json.rows;
						app.selectedRow = null;
					}
				});
			}
			,addRoleToGroup:function(){
				this.roleDialogVisible = true;
			}
			,deleteItem:function(){
				if(!app.selectedRow){
					return;
				}
				app.$confirm('是否确认删除操作?', '提示', {
			          confirmButtonText: '确定',
			          cancelButtonText: '取消',
			          type: 'warning'
			        }).then(function () {
			        	$.ajax({
							url : staticURL+'/basic/roleGroup/delete',
							data : JSON.stringify(app.selectedRow),
							contentType:"application/json",
							success : function(r) {
								if (r.success) {
									app.$notify({showClose: true,
								          message: r.msg,
								          type: 'success',
								          position: 'bottom-right'});
									app.query();
								} else {
									app.$notify({showClose: true,
								          message: r.msg,
								          type: 'error',
								          position: 'bottom-right'});
								}
							}
						});
		        }).catch(function (action) {
		        	  //取消
		        });
				
			}
			,goBack:function () { 
				parent.layer.close(parent.layer.getFrameIndex(window.name));
			}
			,datagridSelect:function(row, event, column){
				app.selectedRow = row;
			}
			,selectRole:function(row){
				var data = {};
				data.roleId = row.id;
				data.groupId = app.groupId;
				$.ajax({
						url : staticURL+"/basic/roleGroup/add",
						data : data,
						dataType : 'json',
						success : function(r) {
							if (r.success) {
								app.query();
								app.roleDialogVisible = false;
							} 
							app.$notify({showClose: true,
						          message: r.msg,
						          type: 'info',
						          position: 'bottom-right'});
						}
				});
			}
		},
		created: function () {
			this.init();
		},
		updated:function(){
		}
	});
});