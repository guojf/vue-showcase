$(function () {
	var app = new Vue({
		el:'#app',
		data:{
			isCollapse: true
			,queryIng : false//正在查询
			,queryData:{activeFlag:'1',page:1,rows:10}//查询条件
			,datagrid:[]//数据列表数据
			,totalNum:0//分页控件配置
			,data:[]
			,defaultCheckedKeys:[]
			,addData:{}
			,selectedRow:null
			,isEdit:false
			,rules:{}
			,dialogConfig:{
				formLabelWidth:'120px'
				,dialogFormVisible:false
				,fileDialogVisible:false
			}
			,fileLoading:false
		},
		methods: {
			init:function () {
				var vm = this
				vm.query();
			}
			,query:function () {
				var vm = this
				if(vm.queryIng){
					return;
				}
				vm.queryIng = true;
				$.ajax({
					url :staticURL+'/basic/uploadFile/datagrid',
					data : vm.queryData,
					dataType : 'json',
					success : function(json) {
						app.datagrid = json.rows;
						app.totalNum = json.total;
						app.queryIng = false;
						app.selectedRow = null;
					}
				});
			}
			,addItem:function(){
				app.openDialog();
			}
			,deleteItem:function(index,data){
				app.$confirm('是否确认删除操作?', '提示', {
		          confirmButtonText: '确定',
		          cancelButtonText: '取消',
		          type: 'warning'
		        }).then(function () {
		        	$.ajax({
						url : staticURL+"/basic/uploadFile/deleteFile?id="+data.id,
						dataType : 'json',
						success : function(r) {
							if (r.success) {
								app.$notify({showClose: true,
							          message: r.msg,
							          type: 'success',
							          position: 'bottom-right'});
								app.query();
							} else {
								app.$notify({showClose: true,
							          message: r.msg,
							          type: 'error',
							          position: 'bottom-right'});
							}
						}
					});
		        }).catch(function (action) {
		        	  //取消
		        });
				return;
				
			}
			,editItem:function(){
				if(!app.selectedRow){
					app.$notify({showClose: true,
				          message: '请选择一条要编辑的记录',
				          type: 'warning',
				          position: 'bottom-right'});
				}else{
					app.openDialog(app.selectedRow);
				}
			}
			,handleSizeChange:function(val){
				app.queryData.rows=val;
				app.query();
			}
			,handleCurrentChange:function(val){
				app.queryData.page=val;
				app.query();
			}
			,handlePrevClick:function(val){
				app.queryData.page=val;
				app.query();
			}
			,handleNextClick:function(val){
				app.queryData.page=val;
				app.query();
			}
			,handleTreeCheckChange:function(data){
				app.queryData.parentCode = data.id;
				app.query();
			}
			,resetForm:function(formName) {
				app.$refs[formName].resetFields();
		    }
			,submitForm:function(formName){
				app.$refs[formName].validate(function(res){
					if(res){
						//验证通过
						$.ajax({
							url : app.saveUrl,
							data : app.addData,
							dataType : 'json',
							success : function(r) {
								if (r.success) {
									app.dialogConfig.dialogFormVisible=false;
									app.$notify({showClose: true,
								          message: r.msg,
								          type: 'success',
								          position: 'bottom-right'});
									app.query();
								} else {
									app.$notify({showClose: true,
								          message: r.msg,
								          type: 'error',
								          position: 'bottom-right'});
								}
							}
						});
					}
				});
			}
			,openDialog:function(param){
				if(param){
					app.isEdit = true;
					//这里如下操作，而不是 app.addData = param,是为了实现JSON对象的复制
					//如果app.addData = param， 当closeDialog()时，dialogForm 被reset
					//app.addDatat同时被reset,会导致bug
					app.addData = JSON.parse(JSON.stringify(param)); 
				}else{
					app.isEdit = false;
					app.addData = {orderNum:1};
				}
				//显示dialog
				app.dialogConfig.dialogFormVisible=true;
				//重置form
				if(app.$refs['dialogForm']){
					app.$refs['dialogForm'].resetFields();
				}
			}
			,closeDialog:function(){
				app.dialogConfig.dialogFormVisible=false;
				app.resetForm('dialogForm')
			}
			,datagridSelect:function(row, event, column){
				app.selectedRow = row;
			}
			,uploadFileSuccess:function(response, file, fileList){
		    	if(response.success){
		    		app.dialogConfig.fileDialogVisible = false;
		    		app.$notify({showClose: true,
		    			message: '上传成功',
		    			type: 'success',
		    			position: 'bottom-right'});
		    		app.query();
		    	}else{
		    		app.$notify({showClose: true,
		    			message: response.msg,
		    			type: 'error',
		    			position: 'bottom-right'});
		    	}
		    	app.$refs['fileUploader'].clearFiles();
		    	this.fileLoading = false;
		    }
		    ,uploadFileError:function(){
		    	app.$notify({showClose: true,
			          message: '上传失败',
			          type: 'error',
			          position: 'bottom-right'});
		    	app.$refs['fileUploader'].clearFiles();
		    	this.fileLoading = false;
		    }
		    ,submitUpload:function(){
		    	this.$refs.fileUploader.submit();
		    }
		    ,doUpload:function(index,data){
		    	this.dialogConfig.fileDialogVisible = true
		    }
		    ,doDownload:function(index,data){
		    	window.open(staticURL+"/basic/uploadFile/findAttach?id="+data.id);
		    }
		    ,beforeUpload:function(file){
		    	//文件大小kb
		    	var fileSize = file.size / 1024
		    	console.log("fileSize "+fileSize);
		    	if(fileSize > 500){
		    		this.$message({  
                        message: '上传文件大小不能超过 500KB!',  
                        type: 'warning'  
                    });  
		    		return false;
		    	}
		    	this.fileLoading = true;
		    }
		},
		computed:{
			saveUrl:function(){
				if(app.isEdit){
					return staticURL+'/basic/uploadFile/edit'
				}else{
					return staticURL+'/basic/uploadFile/add'
				}
			}
		}
		,created: function () {
			this.init();
		},
		updated:function(){
		}
	});
});