$(function () {
	var vm = new Vue({
		el:'#app',
		data:{
			formData:{id:$("#id").val(),rsIds:""},
			ifEdit:false,
			form:{},
			saveUrl:'',
			data:[],
		    defaultCheckedKeys:[],
	        defaultProps: {
	            children: 'children',
	            label: 'text'
	          }
		},
		methods: {
			handleNodeClick(data) {
		          console.log(vm.$refs.tree.getCheckedKeys());
		          console.log();
		    },
			init:function(){
				var that = this;
				layui.use(['table','form','layer','tree','treeselect'], function(){
					var form = layui.form;
					that.treeUrl = staticURL+'/basic/resourceInfoAction/resourceTree'
					$.ajax({url :that.treeUrl,
						cache : false,
						success : function(json) {
							that.data = json;
						}
					});
					if(that.formData.id){
						//修改编辑数据
						that.ifEdit = true
						that.saveUrl = staticURL+"/basic/roleInfoAction/edit"
//						that.treeUrl = staticURL+'/basic/userResourceAction/findResource?userId='+that.formData.id
						that.loadData(form);
					}else{
						//新增数据
						that.treeUrl = staticURL+'/basic/resourceInfoAction/resourceTree'
//						that.saveUrl = staticURL+"/basic/roleInfoAction/add"
					}
					/*var xtree = new layuiXtree({
						elem: 'authTree'           //放xtree的容器（必填）
							, form: form                       //layui form对象 （必填）
							, data: that.treeUrl   //服务端地址（必填）
							, isopen: false                     //初次加载时全部展开，默认true （选填）
							, color: "#000"                    //图标颜色 （选填）
							, icon: {                          //图标样式 （选填）
								open: "&#xe7a0;"               //节点打开的图标
								,close: "&#xe622;"            //节点关闭的图标
								,end: "&#xe621;"              //末尾节点的图标
							  }
					});
					that.xtree = xtree;*/
				});
				
			},
			goBack:function () { 
				parent.layer.close(parent.layer.getFrameIndex(window.name));
			},
			doSave:function () {
				if(!sy.validateLayuiForm($("#roleInfoForm"))){
					return;
				}
				vm.userResouces();
				$.ajax({url : vm.saveUrl,
					type : "post",
					async:false,
					dataType:'json',
					data:vm.formData,
					cache : false,
					success : function(json) {
						if (json && json.success) {
							$.when(parent._refresh()).always(vm.goBack());
						}else{
							layer.msg(json.msg);
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						layer.msg('操作失败！');
			       }
				}); 
			},
			loadData:function(form,treeselect){
				var formData = this.formData;
				var vm  = this;
				$.ajax({url :staticURL+"/basic/roleInfoAction/showDesc",
					data : {
						id : vm.formData.id
					},
					dataType : 'json',
					cache : false,
					async : false,
					success : function(json) {
						sy.formLoadDataVue($(".layui-form"),json,vm.formData);
						form.render();
						if(json.upRsIds){
							vm.defaultCheckedKeys = json.upRsIds;
						}
					}
				});
			},
			userResouces:function(){
				 var checkedNodes = vm.$refs.tree.getCheckedNodes(); //获取末级且选中的checkbox原dom对象，返回的类型:Array
				 var array = [];
		         for (var i = 0; i < checkedNodes.length; i++) {
		              array.push(checkedNodes[i].id);
		         }
		         vm.formData.rsIds = array.join(',');
			},
		},
		created: function () {
			this.init();
		},
		updated:function(){
			
		}
	});
});