$(function () {
	var app = new Vue({
		el:'#app',
		data:{
			isCollapse: true
			,queryIng : false//正在查询
			,queryData:{activeFlag:'1',page:1,rows:10}//查询条件
			,datagrid:[]//数据列表数据
			,totalNum:0//分页控件配置
			,data:[]
			,defaultCheckedKeys:[]
			,addData:{status:1}
			,selectedRow:null
			,isEdit:false
			,rules:{}
			,dialogConfig:{
				formLabelWidth:'120px'
				,dialogFormVisible:false
			}
		},
		methods: {
			init:function () {
				var vm = this
				vm.query();
			}
			,query:function () {
				var vm = this
				if(vm.queryIng){
					return;
				}
				vm.queryIng = true;
				$.ajax({
					url :staticURL+'/basic/scheduleJob/datagrid',
					data : vm.queryData,
					dataType : 'json',
					success : function(json) {
						app.datagrid = json.rows;
						app.totalNum = json.total;
						app.queryIng = false;
						app.selectedRow = null;
					}
				});
			}
			,addItem:function(){
				app.openDialog();
			}
			,deleteItem:function(){
				if(!app.selectedRow){
					app.$notify({showClose: true,
				          message: '请选择一条要删除的记录',
				          type: 'warning',
				          position: 'bottom-right'});
				}else{
					app.$confirm('是否确认删除操作?', '提示', {
			          confirmButtonText: '确定',
			          cancelButtonText: '取消',
			          type: 'warning'
			        }).then(function () {
			        	$.ajax({
							url : staticURL+"/basic/scheduleJob/delete?ids="+app.selectedRow.id,
							dataType : 'json',
							success : function(r) {
								if (r.success) {
									app.$notify({showClose: true,
								          message: r.msg,
								          type: 'success',
								          position: 'bottom-right'});
									app.query();
								} else {
									app.$notify({showClose: true,
								          message: r.msg,
								          type: 'error',
								          position: 'bottom-right'});
								}
							}
						});
			        }).catch(function (action) {
			        	  //取消
			        });
				}
				return;
				
			}
			,runNow:function(){
				if(!app.selectedRow){
					app.$notify({showClose: true,
				          message: '请选择一条要执行的记录',
				          type: 'warning',
				          position: 'bottom-right'});
				}else{
					app.$confirm('是否确认立即执行?', '提示', {
			          confirmButtonText: '确定',
			          cancelButtonText: '取消',
			          type: 'warning'
			        }).then(function () {
			        	$.ajax({
							url : staticURL+"/basic/scheduleJob/runNow?ids="+app.selectedRow.id,
							dataType : 'json',
							success : function(r) {
								if (r.success) {
									app.$notify({showClose: true,
								          message: r.msg,
								          type: 'success',
								          position: 'bottom-right'});
									app.query();
								} else {
									app.$notify({showClose: true,
								          message: r.msg,
								          type: 'error',
								          position: 'bottom-right'});
								}
							}
						});
			        }).catch(function (action) {
			        	  //取消
			        });
				}
				return;
				
			}
			,pause:function(){
				if(!app.selectedRow){
					app.$notify({showClose: true,
				          message: '请选择一条要暂停的记录',
				          type: 'warning',
				          position: 'bottom-right'});
				}else{
					app.$confirm('是否确认暂停操作?', '提示', {
			          confirmButtonText: '确定',
			          cancelButtonText: '取消',
			          type: 'warning'
			        }).then(function () {
			        	$.ajax({
							url : staticURL+"/basic/scheduleJob/pause?ids="+app.selectedRow.id,
							dataType : 'json',
							success : function(r) {
								if (r.success) {
									app.$notify({showClose: true,
								          message: r.msg,
								          type: 'success',
								          position: 'bottom-right'});
									app.query();
								} else {
									app.$notify({showClose: true,
								          message: r.msg,
								          type: 'error',
								          position: 'bottom-right'});
								}
							}
						});
			        }).catch(function (action) {
			        	  //取消
			        });
				}
				return;
				
			}
			,resume:function(){
				if(!app.selectedRow){
					app.$notify({showClose: true,
				          message: '请选择一条要恢复的记录',
				          type: 'warning',
				          position: 'bottom-right'});
				}else{
					app.$confirm('是否确认恢复操作?', '提示', {
			          confirmButtonText: '确定',
			          cancelButtonText: '取消',
			          type: 'warning'
			        }).then(function () {
			        	$.ajax({
							url : staticURL+"/basic/scheduleJob/resume?ids="+app.selectedRow.id,
							dataType : 'json',
							success : function(r) {
								if (r.success) {
									app.$notify({showClose: true,
								          message: r.msg,
								          type: 'success',
								          position: 'bottom-right'});
									app.query();
								} else {
									app.$notify({showClose: true,
								          message: r.msg,
								          type: 'error',
								          position: 'bottom-right'});
								}
							}
						});
			        }).catch(function (action) {
			        	  //取消
			        });
				}
				return;
				
			}
			,editItem:function(){
				if(!app.selectedRow){
					app.$notify({showClose: true,
				          message: '请选择一条要编辑的记录',
				          type: 'warning',
				          position: 'bottom-right'});
				}else{
					app.openDialog(app.selectedRow);
				}
			}
			,handleSizeChange:function(val){
				app.queryData.rows=val;
				app.query();
			}
			,handleCurrentChange:function(val){
				app.queryData.page=val;
				app.query();
			}
			,handlePrevClick:function(val){
				app.queryData.page=val;
				app.query();
			}
			,handleNextClick:function(val){
				app.queryData.page=val;
				app.query();
			}
			,handleTreeCheckChange:function(data){
				app.queryData.parentCode = data.id;
				app.query();
			}
			,resetForm:function(formName) {
				app.$refs[formName].resetFields();
		    }
			,submitForm:function(formName){
				app.$refs[formName].validate(function(res){
					if(res){
						//验证通过
						$.ajax({
							url : app.saveUrl,
							data : app.addData,
							dataType : 'json',
							success : function(r) {
								if (r.success) {
									app.dialogConfig.dialogFormVisible=false;
									app.$notify({showClose: true,
								          message: r.msg,
								          type: 'success',
								          position: 'bottom-right'});
									app.query();
								} else {
									app.$notify({showClose: true,
								          message: r.msg,
								          type: 'error',
								          position: 'bottom-right'});
								}
							}
						});
					}
				});
			}
			,openDialog:function(param){
				if(param){
					app.isEdit = true;
					//这里如下操作，而不是 app.addData = param,是为了实现JSON对象的复制
					//如果app.addData = param， 当closeDialog()时，dialogForm 被reset
					//app.addDatat同时被reset,会导致bug
					app.addData = JSON.parse(JSON.stringify(param)); 
				}else{
					app.isEdit = false;
					app.addData = {orderNum:1};
				}
				//显示dialog
				app.dialogConfig.dialogFormVisible=true;
				//重置form
				if(app.$refs['dialogForm']){
					app.$refs['dialogForm'].resetFields();
				}
			}
			,closeDialog:function(){
				app.dialogConfig.dialogFormVisible=false;
				app.resetForm('dialogForm')
			}
			,datagridSelect:function(row, event, column){
				app.selectedRow = row;
			}
			,formatterStatus:function(row, column, cellValue, index){
				if(row.status == '0'){
					return '启用';
				}else if(row.status == '1'){
					return '暂停';
				}
				return '其他'
			}
		},
		computed:{
			saveUrl:function(){
				if(app.isEdit){
					return staticURL+'/basic/scheduleJob/edit'
				}else{
					return staticURL+'/basic/scheduleJob/add'
				}
			}
		}
		,created: function () {
			this.init();
		},
		updated:function(){
		}
	});
});