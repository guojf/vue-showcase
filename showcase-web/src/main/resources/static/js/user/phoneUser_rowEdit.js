$(function () {
	var app = new Vue({
		el:'#app',
		data:{
			formData:{id:$("#id").val()}
		},
		methods: {
			init:function () {
				var vm = this
			}
			,goBack:function () { 
				parent.layer.close(parent.layer.getFrameIndex(window.name));
			}
			,resetForm:function(formName) {
				app.$refs[formName].resetFields();
		    }
			,submitForm:function(formName){
				app.$refs[formName].validate(function(res){
					if(res){
						//验证通过
						$.ajax({
							url : app.saveUrl,
							data : app.formData,
							dataType : 'json',
							success : function(r) {
								if (r.success) {
									app.$notify({showClose: true,
								          message: r.msg,
								          type: 'success',
								          position: 'bottom-right'});
									app.query();
								} else {
									app.$notify({showClose: true,
								          message: r.msg,
								          type: 'error',
								          position: 'bottom-right'});
								}
							}
						});
					}
				});
			}
		},
		computed:{
			saveUrl:function(){
				if(app.formData.id){
					return staticURL+'/user/phoneUser/edit'
				}else{
					return staticURL+'/user/phoneUser/add'
				}
			}
		}
		,created: function () {
			this.init();
		},
		updated:function(){
		}
	});
});