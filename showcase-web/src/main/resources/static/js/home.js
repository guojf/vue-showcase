$(function () {
	var vm = new Vue({
	    el: '#layout',
	    data: {
	        menuList: {}
			,isCollapse:false
			,selectedTab:'home'
			,tabs:[
				{
					name:'home'
					,title:'首页'
					,content:'welcome'
					,closable:false
				}
			]
			,asideWidth:'200px'
	    },
	    methods: {
	        getMenuList: function () {
	            $.getJSON(staticURL+"/login/meneuList", function (r) {
	                vm.menuList = r.resourceList;
	            });
	        },
	        updatePassword: function () {
	      	  layer.prompt({title: '请输入新密码', formType: 1}, function(pwd1, index){
	    	  	  layer.close(index);
	    		  layer.prompt({title: '请确认新密码', formType: 1}, function(pwd2, index){
	    		    layer.close(index);
	    		    if(pwd1==pwd2){
	    		    	$.post(staticURL+"/basic/userInfo/editPassword?password="+pwd2, function(msg) {
	    		    		layer.msg('恭喜，密码修改成功！');
	    		    	});
	    		    }else{
	    			    layer.msg('您两次输入的密码不一致！');
	    		    }
	    		  });
	    		});
	        },
	        logout: function(){
	      	  layer.confirm('您确定要退出系统?', {
	    		  btn: ['确定','取消'] //按钮
	    		}, function(){
	    			location.href = staticURL+"/login/logout";
	    		}, function(){
	    	  	}
	    	  );
	        }
	        ,addTabs:function(item){
	        	var tab = {};
	        	tab.name = item.name;
	        	tab.title = item.name;
	        	tab.closable = true;
	        	tab.content = staticURL+item.url;
	        	this.tabs.push(tab);
	        	this.selectedTab = item.name;
	        }
	        ,clickMenu:function(item){
	        	var menuOpend = false;
	        	/*this.tabs.find(function(data){
	        		if(data.name == item.name){
	        			vm.selectedTab = item.name;
	        			menuOpend = true;
	        		}
	        	})*/
	        	var tabs = this.tabs;
	        	for(var i=0;i<tabs.length;i++){
	        		if(tabs[i].name == item.name){
	        			vm.selectedTab = item.name;
	        			menuOpend = true;
	        		}
	        	}
	        	if(!menuOpend){
	        		this.addTabs(item);
	        	}
	        }
	        ,removeTab:function(targetName){
	            let tabs = this.tabs;
	            let activeName = this.selectedTab;
	            if (activeName === targetName) {
	              tabs.forEach(function(tab,index){
	                if (tab.name === targetName) {
	                  let nextTab = tabs[index + 1] || tabs[index - 1];
	                  if (nextTab) {
	                    activeName = nextTab.name;
	                  }
	                }
	              });
	            }
	            
	            this.selectedTab = activeName;
	            this.tabs = tabs.filter(function(tab){return tab.name !== targetName});
	        }
	        ,collapseLeftMenu:function(){
	        	this.isCollapse = !this.isCollapse;
	        	if(this.isCollapse){
	        		this.asideWidth = '50px';
	        	}else{
	        		this.asideWidth = '250px';
	        	}
	        }
	        ,contextmenu:function(){
	        	//tab 鼠标右键
	        }
	    },
	    created: function () {
	    	this.getMenuList();
	    },updated:function(){
	    }
	});
});