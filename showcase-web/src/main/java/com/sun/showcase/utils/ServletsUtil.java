package com.sun.showcase.utils;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletResponse;

public class ServletsUtil {

	public static void setHeader(HttpServletResponse response,String fileName) throws UnsupportedEncodingException{
		response.setContentType(Servlets.EXCEL_TYPE_NEW);
		fileName = new String(fileName.getBytes("gb2312"), "ISO8859-1");// headerString为中文时转码
		response.setHeader("Content-disposition", "attachment; filename="+ fileName + ".xlsx");
		response.setContentType("application/msexcel;charset=UTF-8");// 设置类型
		response.setHeader("Pragma", "No-cache");// 设置头
		response.setHeader("Cache-Control", "no-cache");// 设置头
		response.setDateHeader("Expires", 0);// 设置日期头
	}
}
