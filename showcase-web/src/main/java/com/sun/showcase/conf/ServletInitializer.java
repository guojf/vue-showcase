package com.sun.showcase.conf;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import com.sun.showcase.DemoApplication;
/**
 * spring boot 默认打成jar包 ，jar -jar demo.jar运行
 * 如果要打成war包，需要该类，同时pom中spring-boot-starter-tomcat，改为<scope>provided</scope>
 * @author 伟超
 *
 */
public class ServletInitializer extends SpringBootServletInitializer {  

    @Override  	
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {  
        return application.sources(DemoApplication.class);  
    }  

}  
