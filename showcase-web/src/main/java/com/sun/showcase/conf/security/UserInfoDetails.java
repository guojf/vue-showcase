package com.sun.showcase.conf.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.sun.showcase.client.domain.basic.UserInfo;

public class UserInfoDetails extends UserInfo implements UserDetails{

	private static final long serialVersionUID = 1l;
	
	private Collection<GrantedAuthority> auths;

	public UserInfoDetails(UserInfo t,Collection<GrantedAuthority> auths){
		
		this.setId(t.getId());
		
		this.setName(t.getName());
		
		this.setPassword(t.getPassword());
		
		this.setNickName(t.getNickName());
		
		this.setEmail(t.getEmail());
		
		this.setPhone(t.getPhone());
		
		this.setStatus(t.getStatus());

		this.auths = auths;
	}
	

	public Collection<? extends GrantedAuthority> getAuthorities() {
        
		return auths;
	}

	public String getPassword() {
		return super.getPassword();
	}
	
	public String getNickName(){
		return super.getNickName();
	}

	public String getUsername() {
		return super.getName();
	}

	public boolean isAccountNonExpired() {
		return true;
	}

	public boolean isAccountNonLocked() {
		return true;
	}

	public boolean isCredentialsNonExpired() {
		return true;
	}

	public boolean isEnabled() {
		return "1".equals(super.getStatus())?true:false;
	}

}
