package com.sun.showcase.thymeleaf;

import java.util.LinkedHashSet;
import java.util.Set;

import org.springframework.stereotype.Component;
import org.thymeleaf.dialect.AbstractProcessorDialect;
import org.thymeleaf.processor.IProcessor;

@Component
public class SecurityDialect extends AbstractProcessorDialect{
	private static final String ATTRIBUTE_NAME = "auth";
	private static final String PREFIX = "security";
	private static final int processorPrecedence=1000;
	protected SecurityDialect() {
		super(ATTRIBUTE_NAME, PREFIX, processorPrecedence);
	}

	@Override
	public Set<IProcessor> getProcessors(String dialectPrefix) {
		LinkedHashSet<IProcessor> processors = new LinkedHashSet<IProcessor>();  
        processors.add(new SecurityProcessor(dialectPrefix,ATTRIBUTE_NAME,processorPrecedence));  
        return processors;  
	}

}
