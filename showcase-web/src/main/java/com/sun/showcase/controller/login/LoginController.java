package com.sun.showcase.controller.login;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sun.showcase.biz.basic.service.ResourceInfoService;
import com.sun.showcase.biz.basic.service.UserInfoService;
import com.sun.showcase.client.domain.basic.UserInfo;
import com.sun.showcase.client.query.basic.ResourceInfoQuery;
import com.sun.showcase.client.query.basic.UserInfoQuery;
import com.sun.showcase.pojo.Result;
import com.sun.showcase.pojo.SessionConstants;
import com.sun.showcase.utils.ExecuteResult;
import com.sun.showcase.utils.LoginContextHolder;

@Controller
@RequestMapping(value="/login") 
public class LoginController {
	@Autowired
	private UserInfoService userInfoService;
	@Autowired
	private ResourceInfoService resourceInfoService;
	@RequestMapping(value = "",method = RequestMethod.GET)
	public String gologin(){
		return "login_new";
	}
	@RequestMapping(value = "/dologinNew",method = RequestMethod.POST)
	@ResponseBody
	public Result dologinNew(UserInfoQuery userInfoQuery, HttpServletResponse response, HttpServletRequest request){
		ExecuteResult<UserInfo> res = userInfoService.login(userInfoQuery);
		Result j = new Result();
		if(res.isSuccess()){
			request.getSession().setAttribute(SessionConstants.KEY_USER_ID, res.getResult().getId());
			request.getSession().setAttribute(SessionConstants.KEY_USER_NAME, res.getResult().getName());
			request.getSession().setAttribute(SessionConstants.KEY_USER_NICK_NAME, res.getResult().getNickName());
			j.setSuccess(true);
		}else{
			j.setSuccess(false);
			j.setMsg(res.getErrorMessages().get(0));
		}
		return j;
	}
	@RequestMapping(value = "/dologin",method = RequestMethod.POST)
	public String dologin(UserInfoQuery userInfoQuery, HttpServletResponse response, HttpServletRequest request){
		ExecuteResult<UserInfo> res = userInfoService.login(userInfoQuery);
		if(res.isSuccess()){
			request.getSession().setAttribute(SessionConstants.KEY_USER_ID, res.getResult().getId());
			request.getSession().setAttribute(SessionConstants.KEY_USER_NAME, res.getResult().getName());
			request.getSession().setAttribute(SessionConstants.KEY_USER_NICK_NAME, res.getResult().getNickName());
			return "redirect:/login/logedNew";
		}else{
			return "login";
		}
	}
	@RequestMapping(value = "/loged",method = RequestMethod.GET)
	public String loged(ModelMap map){
		String userId=LoginContextHolder.get().getUserId();
		//设置左侧菜单权限
		Map<ResourceInfoQuery, List<ResourceInfoQuery>> resourceMap = resourceInfoService.getResourceByUserID(userId);
		map.put("resourceMap", resourceMap);
		return "home";
	}
	@RequestMapping(value = "/logedNew",method = RequestMethod.GET)
	public String logedNew(ModelMap map){
		String userId=LoginContextHolder.get().getUserId();
		//设置左侧菜单权限
		List<ResourceInfoQuery> resourceList = resourceInfoService.getResourceListByUserID(userId);
		map.put("resourceList", resourceList);
		return "home_new";
	}
	@RequestMapping(value = "/meneuList",method = RequestMethod.GET)
	@ResponseBody
	public Map<String,Object> meneuList(){
		String userId=LoginContextHolder.get().getUserId();
		//设置左侧菜单权限
		List<ResourceInfoQuery> resourceList = resourceInfoService.getResourceListByUserID(userId);
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("resourceList", resourceList);
		return map;
	}
	@RequestMapping(value = "/logout",method = RequestMethod.GET)
	public String logout(UserInfoQuery userInfoQuery, HttpServletResponse response, HttpServletRequest request){
		String userName=(String)request.getSession().getAttribute(SessionConstants.KEY_USER_NAME);
		String userId=(String)request.getSession().getAttribute(SessionConstants.KEY_USER_ID);
		userInfoQuery.setId(userId);
		userInfoQuery.setName(userName);
		request.getSession().invalidate();//销毁session
		userInfoService.logout(userInfoQuery);
		return "redirect:/login/gologin";
	}

}
