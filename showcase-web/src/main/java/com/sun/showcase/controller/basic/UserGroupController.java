package com.sun.showcase.controller.basic;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sun.showcase.biz.basic.service.UserGroupService;
import com.sun.showcase.client.domain.basic.UserGroup;
import com.sun.showcase.client.query.basic.UserGroupQuery;
import com.sun.showcase.pojo.DataGrid;
import com.sun.showcase.pojo.Result;
import com.sun.showcase.utils.LoginContextHolder;
import com.sun.showcase.utils.TreeNode;

@Controller
@RequestMapping(value="/basic/userGroup")
public class UserGroupController {
	
	@Autowired
	private UserGroupService userGroupService;
	
	private Result json = new Result();
	/**
	 * 跳转到UserGroup管理页面
	 * 
	 * @return
	 */
	@RequestMapping(value="goUserGroup")
	public String goUserGroup(@RequestParam("groupId") String groupId , ModelMap map) {
		UserGroupQuery userGroupQuery = new UserGroupQuery();
		userGroupQuery.setUserId(LoginContextHolder.get().getUserId());
		userGroupQuery.setGroupId(groupId);
		List<UserGroupQuery> userGroupList = userGroupService.listAll(userGroupQuery);
		
		if(userGroupList!=null&&userGroupList.size()>0)
			BeanUtils.copyProperties(userGroupList.get(0), userGroupQuery);
		 	map.addAttribute("groupId", groupId);
		 	map.addAttribute("isAdmin", userGroupQuery.getIsAdmin());
		return "/basic/userGroup";
	}
	/**
	 * 跳转到查看desc页面
	 * 
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="showDesc")
	public UserGroupQuery showDesc(UserGroupQuery userGroupQuery) {
		UserGroup userGroup = userGroupService.get(userGroupQuery);
		BeanUtils.copyProperties(userGroup, userGroupQuery);
		return userGroupQuery;
	}

	/**
	 * 获得pageHotel数据表格
	 */
	@ResponseBody
	@RequestMapping(value="datagrid")
	public DataGrid datagrid(UserGroupQuery userGroupQuery) {
		return userGroupService.datagrid(userGroupQuery);
	}
	
	
	/**
	 * 获得无分页的所有数据
	 */
	@ResponseBody
	@RequestMapping(value="combox")
	public List<UserGroupQuery>  combox(UserGroupQuery userGroupQuery){
		return userGroupService.listAll(userGroupQuery);
	}

	/**
	 * 添加一个UserGroup
	 */
	@ResponseBody
	@RequestMapping(value="add")
	public Result add(UserGroupQuery userGroupQuery) {
		
		UserGroup userGroup = userGroupService.get(userGroupQuery);
		
		if(userGroup!=null){
			json.setMsg("该用户已存在组内！");
			return json;
		}
		userGroupService.add(userGroupQuery);
		json.setSuccess(true);
		json.setData(userGroupQuery);
		json.setMsg("添加成功！");
		return json;
	}

	/**
	 * 编辑UserGroup
	 */
	@ResponseBody
	@RequestMapping(value="edit")
	public Result edit(@RequestBody UserGroupQuery userGroupQuery) {
		userGroupService.update(userGroupQuery);
		json.setSuccess(true);
		json.setData(userGroupQuery);
		json.setMsg("编辑成功！");
		return json;
	}

	/**
	 * 删除UserGroup
	 */
	@ResponseBody
	@RequestMapping(value="delete")
	public Result delete(@RequestBody UserGroupQuery userGroupQuery) {
		userGroupService.delete(userGroupQuery);
		json.setSuccess(true);
		return json;
	}
    
	//获取特定组内权限
	@ResponseBody
	@RequestMapping(value="findGroupsInfo")
	public List<TreeNode> findGroupsInfo(UserGroupQuery userGroupQuery){
		return userGroupService.findGroupsInfo(userGroupQuery);
	}
}
