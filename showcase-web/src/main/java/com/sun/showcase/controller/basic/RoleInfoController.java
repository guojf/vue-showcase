package com.sun.showcase.controller.basic;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sun.showcase.biz.basic.service.ResourceInfoService;
import com.sun.showcase.biz.basic.service.RoleInfoService;
import com.sun.showcase.pojo.DataGrid;
import com.sun.showcase.client.domain.basic.ResourceInfo;
import com.sun.showcase.client.domain.basic.RoleInfo;
import com.sun.showcase.client.domain.basic.RoleResource;
import com.sun.showcase.client.query.basic.RoleInfoQuery;
import com.sun.showcase.pojo.Result;
import com.sun.showcase.utils.ExecuteResult;

@Controller
@RequestMapping(value="/basic/roleInfo") 
public class RoleInfoController {
	@Autowired
	private RoleInfoService roleInfoService;
	@Autowired
	private ResourceInfoService resourceInfoService;
	
	private Result json = new Result();
	
	/**
	 * 跳转到RoleInfo管理页面
	 * 
	 * @return
	 */
	@RequestMapping(value = "/goRoleInfo")
	public String goRoleInfo(@RequestParam(value="dialogModel",defaultValue="false",required=false) boolean dialogModel,ModelMap map) {
		map.put("dialogModel", dialogModel);
		return "/basic/roleInfo";
	}
	@RequestMapping(value = "/goAddRoleInfo")
	public String goAddRoleInfo(@RequestParam(value="id",required=false) Long id ,ModelMap map) {
		map.put("id", id);
		return "/basic/roleInfo_rowEdit";
	}
	/**
	 * 跳转到查看desc页面
	 * 
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/showDesc")
	public RoleInfoQuery showDesc(RoleInfoQuery roleInfoQuery) {
		List<RoleResource> listRoleResource=roleInfoService.getRoleResourceByRoleID(roleInfoQuery);
		RoleInfo roleInfo = roleInfoService.get(roleInfoQuery);
		BeanUtils.copyProperties(roleInfo, roleInfoQuery);
		List<String> listIds = new ArrayList<String>();
		for(int i=0;i<listRoleResource.size();i++){
			List<ResourceInfo> listResourceInfo=resourceInfoService.getChildByPid(listRoleResource.get(i).getResourceId());
			if(listResourceInfo.size()==0){//如果没有子资源 算作末级  将其加到列表上
				listIds.add(listRoleResource.get(i).getResourceId());
			}
		}
		//不能直接使用有子资源的资源 这样的话 该资源的下所有的子资源都会被选中
		String[] resId=new String[listIds.size()];
		for(int i=0;i<resId.length;i++){
			resId[i]=listIds.get(i);
		}
		roleInfoQuery.setUpRsIds(resId);
		return roleInfoQuery;
	}

	/**
	 * 获得pageHotel数据表格
	 */
	@ResponseBody
	@RequestMapping(value = "/datagrid")
	public DataGrid datagrid(RoleInfoQuery roleInfoQuery) {
		return roleInfoService.datagrid(roleInfoQuery);
	}
	
	
	/**
	 * 获得无分页的所有数据
	 */
	@ResponseBody
	@RequestMapping(value = "/combox")
	public List<RoleInfoQuery>  combox(RoleInfoQuery roleInfoQuery){
		return roleInfoService.listAll(roleInfoQuery);
	}

	/**
	 * 添加一个RoleInfo
	 */
	@ResponseBody
	@RequestMapping(value = "/add")
	public Result add(RoleInfoQuery roleInfoQuery) {
		ExecuteResult<RoleInfo> executeResult=roleInfoService.add(roleInfoQuery);
		if(!executeResult.isSuccess()){
			json.setSuccess(false);
			json.setMsg(executeResult.getErrorMessages().get(0));
		}else{
			json.setSuccess(true);
			json.setMsg("添加成功！");
		}
		json.setData(roleInfoQuery);
		return json;
	}

	/**
	 * 编辑RoleInfo
	 */
	@ResponseBody
	@RequestMapping(value = "/edit")
	public Result edit(RoleInfoQuery roleInfoQuery) {
		ExecuteResult<RoleInfo> executeResult=roleInfoService.update(roleInfoQuery);
		if(!executeResult.isSuccess()){
			json.setSuccess(false);
			json.setMsg(executeResult.getErrorMessages().get(0));
		}else{
			json.setSuccess(true);
			json.setMsg("添加成功！");
		}
		json.setData(roleInfoQuery);
		return json;
	}

	/**
	 * 删除RoleInfo
	 */
	@ResponseBody
	@RequestMapping(value = "/delete")
	public Result delete(RoleInfoQuery roleInfoQuery) {
		roleInfoService.delete(roleInfoQuery.getIds());
		json.setSuccess(true);
		return json;
	}
}
