package com.sun.showcase.controller.basic;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sun.showcase.aop.BussinessLog;
import com.sun.showcase.biz.basic.service.BaseInfoService;
import com.sun.showcase.client.domain.basic.BaseInfo;
import com.sun.showcase.client.query.basic.BaseInfoQuery;
import com.sun.showcase.pojo.DataGrid;
import com.sun.showcase.pojo.Result;
import com.sun.showcase.utils.TreeNode;

@Controller
@RequestMapping(value="/basic/baseInfoAction") 
public class BaseInfoController {
	
	@Autowired
	private BaseInfoService baseInfoService;
	
	private Result json = new Result();
	
	
	/**
	 * 跳转到BaseInfo管理页面
	 * 
	 * @return
	 */
	@RequestMapping(value = "/goBaseInfo")
	public String goBaseInfo(BaseInfoQuery baseInfoQuery) {
		return "basic/baseInfo";
	}
	/**
	 * 跳转到BaseInfo编辑页面
	 * 
	 * @return
	 */
	@RequestMapping(value = "/goEditInfo")
	public String goEditInfo(String opts,String parentCode,String id,ModelMap map) {
		map.put("opts", opts);
		map.put("id", id);
		map.put("parentCode", parentCode);
		return "basic/baseInfo_rowEdit";
	}
	/**
	 * 跳转到查看desc页面
	 * 
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/showDesc")
	public BaseInfoQuery showDesc(BaseInfoQuery baseInfoQuery) {
		BaseInfo baseInfo=  baseInfoService.get(baseInfoQuery);
		BeanUtils.copyProperties(baseInfo, baseInfoQuery);
		return baseInfoQuery;
	}

	/**
	 * 获得pageHotel数据表格
	 */
	
	@ResponseBody
	@BussinessLog(name="基础数据列表查询")
	@RequestMapping(value = "/datagrid")
	public DataGrid datagrid(BaseInfoQuery baseInfoQuery) {
		return baseInfoService.datagrid(baseInfoQuery);
	}
	
	
	/**
	 * 获得无分页的所有数据
	 */
	
	@ResponseBody
	@RequestMapping(value = "/combox")
	public List<BaseInfoQuery> combox(BaseInfoQuery baseInfoQuery){
		baseInfoQuery.setOrderColum("order_num");
		baseInfoQuery.setOrderRule("asc");
		return baseInfoService.listAll(baseInfoQuery);
	}

	/**
	 * 添加一个BaseInfo
	 */
	
	@ResponseBody
	@RequestMapping(value = "/add")
	public Result add(BaseInfoQuery baseInfoQuery) {
		
		if(baseInfoService.findValidates(baseInfoQuery)){
			json.setSuccess(false);
			json.setMsg("编码或者名称重复！");
			return json;
		}
		baseInfoService.add(baseInfoQuery);
		json.setSuccess(true);
		json.setData(baseInfoQuery);
		json.setMsg("添加成功！");
		return json;
	}

	/**
	 * 编辑BaseInfo
	 */
	@ResponseBody
	@RequestMapping(value = "/edit")
	public Result edit(BaseInfoQuery baseInfoQuery) {		
		baseInfoService.update(baseInfoQuery);
		json.setSuccess(true);
		json.setData(baseInfoQuery);
		json.setMsg("编辑成功！");
		return json;
	}

	/**
	 * 删除BaseInfo
	 */
	@ResponseBody
	@RequestMapping(value = "/delete")
	public Result delete(BaseInfoQuery baseInfoQuery) {
		baseInfoService.delete(baseInfoQuery);
		json.setSuccess(true);
		json.setMsg("删除成功！");
		return json;
	}
	/**
	 * 树形结构
	 */
	@ResponseBody
	@RequestMapping(value = "/findTrees")
	public List<TreeNode> findTrees(BaseInfoQuery baseInfoQuery) {
		return baseInfoService.findTrees(baseInfoQuery);
	}
	
}
