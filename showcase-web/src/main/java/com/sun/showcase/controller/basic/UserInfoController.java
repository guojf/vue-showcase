package com.sun.showcase.controller.basic;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sun.showcase.biz.basic.excel.UserInfoTemplate;
import com.sun.showcase.biz.basic.service.UserInfoService;
import com.sun.showcase.client.domain.basic.UserInfo;
import com.sun.showcase.client.query.basic.UserInfoQuery;
import com.sun.showcase.excel.ExcelExportTemplate;
import com.sun.showcase.pojo.DataGrid;
import com.sun.showcase.pojo.Result;
import com.sun.showcase.utils.LoginContextHolder;
import com.sun.showcase.utils.ServletsUtil;
@Controller
@RequestMapping(value="/basic/userInfo")
public class UserInfoController {
	@Autowired
	private UserInfoService userInfoService;
	
	private Result json = new Result();
	/**
	 * 跳转到UserInfo管理页面
	 * 
	 * @return
	 */
	@RequestMapping(value="goUserInfo")
	public String goUserInfo(@RequestParam(value="dialogModel",defaultValue="false",required=false) boolean dialogModel,ModelMap map) {
		map.put("dialogModel", dialogModel);
		return "basic/userInfo";
	}
	/**
	 * 跳转到UserInfo添加操作页面
	 * 
	 * @return
	 */
	@RequestMapping(value="goAddHandle")
	public String goAddHandle() {
		return "basic/userInfo_add";
	}
	/**
	 * 跳转到UserInfo编辑操作页面
	 * 
	 * @return
	 */
	@RequestMapping(value="goEditHandle")
	public String goEditHandle(@RequestParam("id")String id ,ModelMap map) {
		map.put("id", id);
		return "basic/userInfo_add";
	}
	/**
	 * 跳转到查看desc页面
	 * 
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="showDesc")
	public UserInfoQuery showDesc(UserInfoQuery userInfoQuery) {
		UserInfo userInfo = userInfoService.get(userInfoQuery);
		BeanUtils.copyProperties(userInfo, userInfoQuery);
		return userInfoQuery;
	}

	/**
	 * 获得pageHotel数据表格
	 */
	@ResponseBody
	@RequestMapping(value="datagrid")
	public DataGrid datagrid(UserInfoQuery userInfoQuery) {
		return userInfoService.datagrid(userInfoQuery);
	}
	
	
	/**
	 * 获得无分页的所有数据
	 */
	@ResponseBody
	@RequestMapping(value="combox")
	public List<UserInfoQuery> combox(UserInfoQuery userInfoQuery){
		return userInfoService.listAll(userInfoQuery);
	}

	/**
	 * 添加一个UserInfo
	 */
	@ResponseBody
	@RequestMapping(value="add")
	public Result add(UserInfoQuery userInfoQuery) {
		
		UserInfo ui = userInfoService.getByName(userInfoQuery);
		
		if(ui != null){
			json.setSuccess(false);
			json.setMsg("账号重复,请重新核实!");
			return json;
		}
		
		userInfoService.add(userInfoQuery);
		json.setSuccess(true);
		json.setData(userInfoQuery);
		json.setMsg("添加成功！");
		return json;
	}

	/**
	 * 编辑UserInfo
	 */
	@ResponseBody
	@RequestMapping(value="edit")
	public Result edit(UserInfoQuery userInfoQuery) {
		userInfoService.update(userInfoQuery);
		json.setSuccess(true);
		json.setData(userInfoQuery);
		json.setMsg("编辑成功！");
		return json;
	}
	
	@ResponseBody
	@RequestMapping(value="editPassword")
	public Result editPassword(UserInfoQuery userInfoQuery){
		userInfoQuery.setId(LoginContextHolder.get().getUserId());
		userInfoService.editPassword(userInfoQuery);
		json.setSuccess(true);
		json.setData(userInfoQuery);
		json.setMsg("编辑成功！");
		return json;
	}
	/**
	 * 删除UserInfo
	 */
	@ResponseBody
	@RequestMapping(value="delete")
	public Result delete(UserInfoQuery userInfoQuery) {
		userInfoService.delete(userInfoQuery.getIds());
		json.setSuccess(true);
		json.setMsg("删除成功！");
		return json;
	}
	@RequestMapping(value="export")
	public void export(UserInfoQuery userInfoQuery,HttpServletResponse response) throws IOException, Exception{
		List<UserInfoQuery> userList = userInfoService.listAll(userInfoQuery);
		ExcelExportTemplate<List<UserInfoQuery>> exportListTemplate = new UserInfoTemplate();
		ServletsUtil.setHeader(response, "用户列表");
		exportListTemplate.doExport(response.getOutputStream(), userList);
	}
}
