package com.sun.showcase.controller.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sun.showcase.biz.user.service.PhoneUserService;
import com.sun.showcase.pojo.DataGrid;
import com.sun.showcase.pojo.Result;
import com.sun.showcase.client.domain.user.PhoneUser;
import com.sun.showcase.client.query.user.PhoneUserQuery;

@Controller
@RequestMapping(value="/user/phoneUser") 
public class PhoneUserController{
	
	@Autowired
	private PhoneUserService phoneUserService;
	
	private Result result = new Result();
	/**
	 * 跳转到PhoneUser管理页面
	 * 
	 * @return
	 */
	@RequestMapping(value="goPhoneUser")
	public String goPhoneUser() {
		return "user/phoneUser";
	}
	/**
	 * 跳转到PhoneUser新增页面
	 * 
	 * @return
	 */
	@RequestMapping(value="handlePhoneUser")
	public String handlePhoneUser(@RequestParam(value="id",required=false) Long id ,ModelMap map) {
		return "user/phoneUser_rowEdit";
	}

	/**
	 * 跳转到查看desc页面
	 * 
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="showDesc")
	public PhoneUser showDesc(PhoneUserQuery phoneUserQuery) {
		return phoneUserService.get(phoneUserQuery);
	}

	/**
	 * 获得分页数据表格
	 */
	@ResponseBody
	@RequestMapping(value = "/datagrid")
	public DataGrid datagrid(PhoneUserQuery phoneUserQuery) {
		return phoneUserService.datagrid(phoneUserQuery);
	}
	

	/**
	 * 添加一个PhoneUser
	 */
	@ResponseBody
	@RequestMapping(value="add")
	public Result add(PhoneUserQuery phoneUserQuery) {
		phoneUserService.add(phoneUserQuery);
		result.setSuccess(true);
		result.setMsg("添加成功！");
		return result;
	}

	/**
	 * 编辑PhoneUser
	 */
	@ResponseBody
	@RequestMapping(value="edit")
	public Result edit(PhoneUserQuery phoneUserQuery) {
		phoneUserService.update(phoneUserQuery);
		result.setSuccess(true);
		result.setMsg("编辑成功！");
		return result;
	}

	/**
	 * 物理删除PhoneUser
	 */
	@ResponseBody
	@RequestMapping(value="delete")
	public Result delete(PhoneUserQuery phoneUserQuery) {
		phoneUserService.delete(phoneUserQuery.getIds());
		result.setSuccess(true);
		result.setMsg("删除成功！");
		return result;
	}
	
	/**
	 * 逻辑删除PhoneUser
	 */
	@ResponseBody
	@RequestMapping(value="deletePt")
	public Result deletePt(PhoneUserQuery phoneUserQuery) {
		phoneUserService.deletePt(phoneUserQuery.getIds());
		result.setSuccess(true);
		result.setMsg("删除成功！");
		return result;
	}
}
