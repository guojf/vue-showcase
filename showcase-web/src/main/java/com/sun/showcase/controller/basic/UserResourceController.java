package com.sun.showcase.controller.basic;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sun.showcase.biz.basic.service.UserResourceService;
import com.sun.showcase.pojo.DataGrid;
import com.sun.showcase.client.domain.basic.UserResource;
import com.sun.showcase.client.query.basic.UserResourceQuery;
import com.sun.showcase.pojo.Result;
import com.sun.showcase.utils.TreeNode;

@Controller
@RequestMapping(value="/basic/userResourceAction")
public class UserResourceController {
	@Autowired
	private UserResourceService userResourceService;
	
	private Result json = new Result();
	
	/**
	 * 跳转到UserResource管理页面
	 * 
	 * @return
	 */
	@RequestMapping(value="goUserResource")
	public String goUserResource() {
		return "basic/userResource";
	}
	/**
	 * 跳转到查看desc页面
	 * 
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="showDesc")
	public UserResourceQuery showDesc(UserResourceQuery userResourceQuery) {
		UserResource userResource = userResourceService.get(userResourceQuery);
		BeanUtils.copyProperties(userResource, userResourceQuery);
		return userResourceQuery;
	}

	/**
	 * 获得pageHotel数据表格
	 */
	@ResponseBody
	@RequestMapping(value="datagrid")
	public DataGrid datagrid(UserResourceQuery userResourceQuery) {
		return userResourceService.datagrid(userResourceQuery);
	}
	
	
	/**
	 * 获得无分页的所有数据
	 */
	@ResponseBody
	@RequestMapping(value="combox")
	public List<UserResourceQuery> combox(UserResourceQuery userResourceQuery){
		return userResourceService.listAll(userResourceQuery);
	}

	/**
	 * 添加一个UserResource
	 */
	@ResponseBody
	@RequestMapping(value="add")
	public Result add(UserResourceQuery userResourceQuery) {
		userResourceService.add(userResourceQuery);
		json.setSuccess(true);
		json.setData(userResourceQuery);
		json.setMsg("添加成功！");
		return json;
	}

	/**
	 * 编辑UserResource
	 */
	@ResponseBody
	@RequestMapping(value="edit")
	public Result edit(UserResourceQuery userResourceQuery) {
		userResourceService.update(userResourceQuery);
		json.setSuccess(true);
		json.setData(userResourceQuery);
		json.setMsg("编辑成功！");
		return json;
	}
	
	@ResponseBody
	@RequestMapping(value="findResource")
	public List<TreeNode> findResource(UserResourceQuery userResourceQuery) {
		return userResourceService.findResourceByUser(userResourceQuery.getUserId());
	}
	
	@ResponseBody
	@RequestMapping(value="fingResourceByUserID")
	public List<TreeNode> fingResourceByUserID(UserResourceQuery userResourceQuery){
		if(userResourceQuery.getId()==null){
			return userResourceService.findResourceByUserID(userResourceQuery.getUserId());
		}
		return null;
	}
}
