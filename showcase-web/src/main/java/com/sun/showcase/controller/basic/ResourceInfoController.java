package com.sun.showcase.controller.basic;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sun.showcase.biz.basic.service.ResourceInfoService;
import com.sun.showcase.pojo.DataGrid;
import com.sun.showcase.client.domain.basic.ResourceInfo;
import com.sun.showcase.client.query.basic.ResourceInfoQuery;
import com.sun.showcase.pojo.Result;
import com.sun.showcase.utils.TreeNode;

@Controller
@RequestMapping(value="/basic/resourceInfo") 
public class ResourceInfoController {
	@Autowired
	private ResourceInfoService resourceInfoService;
	
	private Result json = new Result();
	
	@RequestMapping(value = "/goResourceInfo")
	public String goResourceInfo() {
		return "basic/resourceInfo";
	}
	@RequestMapping(value = "/goHandle")
	public String goHandle(@RequestParam(value="id",required=false)String id ,ModelMap map) {
		map.put("id", id);
		return "basic/resourceInfo_rowEdit";
	}
	/**
	 * 获得pageHotel数据表格
	 */
	@ResponseBody
	@RequestMapping(value = "/datagrid")
	public DataGrid datagrid(ResourceInfoQuery resourceInfoQuery) {
		DataGrid datagrid = resourceInfoService.datagrid(resourceInfoQuery);
		return datagrid;
	}
	
	//创建角色时  查询资源树
	@ResponseBody
	@RequestMapping(value = "/resourceTree")
	public List<TreeNode> resourceTree(){
		List<TreeNode> listnode = new ArrayList<TreeNode>();
		listnode=resourceInfoService.findParentResource();
	//	this.moduleNodes = JSONArray.fromObject(listnode);
		return listnode;
	}
	
	/**
	 * 添加一个ResourceInfo
	 */
	@ResponseBody
	@RequestMapping(value = "/add")
	public Result add(ResourceInfoQuery resourceInfoQuery) {
		resourceInfoService.add(resourceInfoQuery);
		json.setSuccess(true);
		json.setData(resourceInfoQuery);
		json.setMsg("添加成功！");
		return json;
	}

	/**
	 * 编辑ResourceInfo
	 */
	@ResponseBody
	@RequestMapping(value = "/edit")
	public Result edit(ResourceInfoQuery resourceInfoQuery) {
		resourceInfoService.update(resourceInfoQuery);
		json.setSuccess(true);
		json.setData(resourceInfoQuery);
		json.setMsg("编辑成功！");
		return json;
	}

	/**
	 * 删除ResourceInfo
	 */
	@ResponseBody
	@RequestMapping(value = "/delete")
	public Result delete(ResourceInfoQuery resourceInfoQuery) {
		resourceInfoService.delete(resourceInfoQuery.getIds());
		json.setSuccess(true);
		return json;
	}
	
	/**
	 * 跳转到查看desc页面
	 * 
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/showDesc")
	public ResourceInfoQuery showDesc(ResourceInfoQuery resourceInfoQuery) {
		ResourceInfo resourceInfo = resourceInfoService.get(resourceInfoQuery);
		BeanUtils.copyProperties(resourceInfo, resourceInfoQuery);
		return resourceInfoQuery;
	}
}
