package com.sun.showcase.controller.basic;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sun.showcase.biz.basic.service.RoleGroupService;
import com.sun.showcase.biz.basic.service.UserGroupService;
import com.sun.showcase.pojo.DataGrid;
import com.sun.showcase.client.domain.basic.RoleGroup;
import com.sun.showcase.client.query.basic.RoleGroupQuery;
import com.sun.showcase.client.query.basic.UserGroupQuery;
import com.sun.showcase.pojo.Result;
import com.sun.showcase.utils.LoginContextHolder;

@Controller
@RequestMapping(value="/basic/roleGroup") 
public class RoleGroupController {
	
	@Autowired
	private RoleGroupService roleGroupService;
	
	@Autowired
	private UserGroupService userGroupService;
	
	private Result json = new Result();
	
	
	/**
	 * 跳转到RoleGroup管理页面
	 * 
	 * @return
	 */
	@RequestMapping(value = "/goRoleGroup")
	public String goRoleGroup(@RequestParam("groupId") String groupId,ModelMap map) {
		
		UserGroupQuery userGroupQuery = new UserGroupQuery();
		userGroupQuery.setUserId(LoginContextHolder.get().getUserId());
		userGroupQuery.setGroupId(groupId);
		List<UserGroupQuery> userGroupList = userGroupService.listAll(userGroupQuery);
		
		if(userGroupList!=null&&userGroupList.size()>0){
			String isAdmin = userGroupList.get(0).getIsAdmin();
			map.addAttribute("groupId", userGroupQuery.getGroupId());
			map.addAttribute("isAdmin", isAdmin);
		}
		return "/basic/roleGroup";
	}
	/**
	 * 跳转到查看desc页面
	 * 
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/showDesc")
	public RoleGroupQuery showDesc(RoleGroupQuery roleGroupQuery) {
		RoleGroup roleGroup = roleGroupService.get(roleGroupQuery);
		BeanUtils.copyProperties(roleGroup, roleGroupQuery);
		return roleGroupQuery;
	}

	/**
	 * 获得pageHotel数据表格
	 */
	@ResponseBody
	@RequestMapping(value = "/datagrid")
	public DataGrid datagrid(RoleGroupQuery roleGroupQuery) {
		return roleGroupService.datagrid(roleGroupQuery);
	}
	
	
	/**
	 * 获得无分页的所有数据
	 */
	@ResponseBody
	@RequestMapping(value = "/combox")
	public List<RoleGroupQuery>  combox(RoleGroupQuery roleGroupQuery){
		return roleGroupService.listAll(roleGroupQuery);
	}

	/**
	 * 添加一个RoleGroup
	 */
	@ResponseBody
	@RequestMapping(value = "/add")
	public Result add(RoleGroupQuery roleGroupQuery) {
		RoleGroup rg = roleGroupService.get(roleGroupQuery);
		if(rg != null){
			json.setSuccess(false);
			json.setMsg("该角色已存在组内！");
			return json;
		}
		roleGroupService.add(roleGroupQuery);
		json.setSuccess(true);
		json.setData(roleGroupQuery);
		json.setMsg("添加成功！");
		return json;
	}

	/**
	 * 编辑RoleGroup
	 */
	@ResponseBody
	@RequestMapping(value = "/edit")
	public Result edit(RoleGroupQuery roleGroupQuery) {
		roleGroupService.update(roleGroupQuery);
		json.setSuccess(true);
		json.setData(roleGroupQuery);
		json.setMsg("编辑成功！");
		return json;
	}

	/**
	 * 删除RoleGroup
	 */
	@ResponseBody
	@RequestMapping(value = "/delete")
	public Result delete(@RequestBody RoleGroupQuery roleGroupQuery) {
		roleGroupService.delete(roleGroupQuery);
		json.setSuccess(true);
		json.setMsg("操作成功！");
		return json;
	}
}
