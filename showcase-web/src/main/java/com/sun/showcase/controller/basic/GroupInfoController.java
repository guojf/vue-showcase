package com.sun.showcase.controller.basic;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sun.showcase.biz.basic.service.GroupInfoService;
import com.sun.showcase.pojo.DataGrid;
import com.sun.showcase.client.domain.basic.GroupInfo;
import com.sun.showcase.client.query.basic.GroupInfoQuery;
import com.sun.showcase.pojo.Result;

@Controller
@RequestMapping(value="/basic/groupInfo") 
public class GroupInfoController {
	@Autowired
	private GroupInfoService groupInfoService;
	
	private Result json = new Result();
	/**
	 * 跳转到GroupInfo管理页面
	 * 
	 * @return
	 */
	@RequestMapping(value = "/goGroupInfo")
	public String goGroupInfo() {
		return "basic/groupInfo";
	}
	/**
	 * 跳转到查看desc页面
	 * 
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/showDesc")
	public GroupInfoQuery showDesc(GroupInfoQuery groupInfoQuery) {
		GroupInfo groupInfo = groupInfoService.get(groupInfoQuery);
		BeanUtils.copyProperties(groupInfo, groupInfoQuery);
		return groupInfoQuery;
	}

	/**
	 * 获得pageHotel数据表格
	 */
	@ResponseBody
	@RequestMapping(value = "/datagrid")
	public DataGrid datagrid(GroupInfoQuery groupInfoQuery) {
		return groupInfoService.datagrid(groupInfoQuery);
	}
	
	
	/**
	 * 获得无分页的所有数据
	 */
	@ResponseBody
	@RequestMapping(value = "/combox")
	public List<GroupInfoQuery>  combox(GroupInfoQuery groupInfoQuery){
		return  groupInfoService.listAll(groupInfoQuery);
	}
	/**
	 * 添加一个GroupInfo
	 */
	@ResponseBody
	@RequestMapping(value = "/add")
	public Result add(GroupInfoQuery groupInfoQuery) {
		groupInfoService.add(groupInfoQuery);
		json.setSuccess(true);
		json.setData(groupInfoQuery);
		json.setMsg("添加成功！");
		return json;
	}

	/**
	 * 编辑GroupInfo
	 */
	@ResponseBody
	@RequestMapping(value = "/edit")
	public Result edit(GroupInfoQuery groupInfoQuery) {
		groupInfoService.update(groupInfoQuery);
		json.setSuccess(true);
		json.setData(groupInfoQuery);
		json.setMsg("编辑成功！");
		return json;
	}

	/**
	 * 删除GroupInfo
	 */
	@ResponseBody
	@RequestMapping(value = "/delete")
	public Result delete(GroupInfoQuery groupInfoQuery) {
		groupInfoService.delete(groupInfoQuery.getId());
		json.setSuccess(true);
		return json;
	}
}
