package com.sun.showcase.controller;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sun.showcase.client.domain.basic.UserInfo;
import com.sun.showcase.config.redis.RedisSingleUtil;
import com.sun.showcase.config.redisson.RedissLockUtil;
import com.sun.showcase.utils.SpringApplicationContextHolder;


@Controller
public class HomeController {
	@Autowired
	RedisSingleUtil redisSingleUtil;
	@RequestMapping(value = {"/lock"})
	@ResponseBody
	public Object lock(boolean r,String name){
		//尝试3秒获取锁，
		//锁成功后20秒自动释放
		boolean res = RedissLockUtil.tryLock("lock_key", TimeUnit.SECONDS, 3, 20);
		if(res){
			redisSingleUtil.set("OK", r);
			UserInfo user = new UserInfo();
			user.setName(name);
			redisSingleUtil.set("USER", user);
			if(r) {
				//释放锁
				RedissLockUtil.unlock("lock_key");
			}
		}
		return redisSingleUtil.get("USER");
	}
	@RequestMapping(value = {"/","/home"})
	public String home(){
		return "home";
	}
	@RequestMapping(value = {"/welcome"})
	public String welcome(){
		return "welcome";
	}
	@RequestMapping(value = "/noAuth",method = RequestMethod.GET)
	public String noAuth(){
		return "noAuth";
	}
	@RequestMapping(value = "/icons",method = RequestMethod.GET)
	public String icons(){
		return "icons";
	}
	//异常处理
	@RequestMapping("/error/{code}")
	public String noAccess(@PathVariable String code){
		
	    return "error/" + code;
	}
	
	//请求超时
	@RequestMapping("/timeout")
	public void timeout(@RequestParam(value = "id", required = false) String id,HttpServletRequest request, HttpServletResponse response) throws IOException{
		response.addHeader("sessionstatus", "timeout");
		//ajax 超时处理
		if(request.getHeader("x-requested-with") !=null 
				&& request.getHeader("x-requested-with").equalsIgnoreCase("XMLHttpRequest")){  
			
           response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
     
        }else{
        	
             response.sendRedirect(request.getContextPath()+"/login");  
        }
	}
}
