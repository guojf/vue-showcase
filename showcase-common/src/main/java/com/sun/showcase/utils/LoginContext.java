package com.sun.showcase.utils;

import java.io.Serializable;
import java.util.Properties;

/**
 * 当前登录用户上下文信息
 *
 */
public class LoginContext implements Serializable{
	private static final long serialVersionUID = -8215311021159364931L;
	/**
	 * 用户id
	 */
	private String userId;
	/**
	 * 用户名
	 */
	private String userName;
	/**
	 * 昵称
	 */
	private String userNickName;
	/**
	 * 访问IP地址
	 */
	private String ip;
	/**
	 * 其他属性信息
	 */
	private Properties properties = new Properties();
	/**
	 * 客户端访问语言
	 */
	private String language;
	/**
	 * 区域
	 */
	private String areaCode;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public Properties getProperties() {
		return properties;
	}
	public void setProperties(Properties properties) {
		this.properties = properties;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getAreaCode() {
		return areaCode;
	}
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	public String getUserNickName() {
		return userNickName;
	}
	public void setUserNickName(String userNickName) {
		this.userNickName = userNickName;
	}
	
	
}
