package com.sun.showcase.utils;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;

/**
 * 格式化类
 * 
 * @author QiuHuafeng
 */
public class Format {

	public static final String DATE_PATTERN = "MM/dd/yyyy";

	/**
	 * 格式化日期
	 * 
	 * @param dateString
	 *            日期字符串
	 * @param fromPattern
	 *            字符串的日期格式。yyyy-MM-dd y 年 M 月 d 日 h 时 在上午或下午 (1~12) H 时 在一天中
	 *            (0~23) m 分 s 秒
	 * @param toPattern
	 *            目标日期的格式
	 * @return
	 */
	public static String formatDate(String dateString, String fromPattern,
			String toPattern) {
		DateFormat dateFormat;
		Date date = null;
		try {
			dateFormat = new SimpleDateFormat(fromPattern);
			date = dateFormat.parse(dateString);
		} catch (ParseException e) {
			//e.printStackTrace();
			return null;
		}
		dateFormat = new SimpleDateFormat(toPattern);
		return dateFormat.format(date);
	}

	/**
	 * 将字符串转成日期类型
	 * 
	 * @param dateString
	 *            日期字符串
	 * @param pattern
	 *            字符串的日期格式。yyyy-MM-dd y 年 M 月 d 日 h 时 在上午或下午 (1~12) H 时 在一天中
	 *            (0~23) m 分 s 秒
	 * @return
	 */
	public static Date parseDate(String dateString, String pattern) {
		if (StringUtils.isBlank(dateString)){
			return null;
		}
		DateFormat dateFormat;
		Date date = null;
		try {
			dateFormat = new SimpleDateFormat(pattern);
			date = dateFormat.parse(dateString);
		} catch (ParseException e) {
			//e.printStackTrace();
		}
		return date;
	}

	/**
	 * 将字符串转成日期类型 默认MM/dd/yyyy格式
	 * 
	 * @param dateString
	 *            日期字符串
	 * @return
	 */
	public static Date parseDate(String dateString) {
		if (StringUtils.isBlank(dateString)){
			return null;
		}
		DateFormat dateFormat;
		Date date = null;
		try {
			dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			date = dateFormat.parse(dateString);
		} catch (ParseException e) {
			//e.printStackTrace();
		}
		return date;
	}

	/**
	 * 将日期类型转成字符串 默认MM/dd/yyyy格式
	 * 
	 * @param date
	 *            日期字符串
	 * @return
	 */
	public static String parseDate(Date date) {
		if (date == null){
			return null;
		}
		DateFormat dateFormat = null;
		try {
			dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			return dateFormat.format(date);
		} catch (Exception e) {
			//e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 将日期类型转成字符串 默认MM/dd/yyyy格式
	 * 
	 * @param date
	 *            日期字符串
	 * @return
	 */
	public static String parseDate(Date date,String pattern) {
		if (date == null){
			return null;
		}
		DateFormat dateFormat = null;
		try {
			dateFormat = new SimpleDateFormat(pattern);
			return dateFormat.format(date);
		} catch (Exception e) {
			//e.printStackTrace();
		}
		return null;
	}

	/**
	 * 数字前补0
	 * 
	 * @param pattern
	 *            格式，例如：000000
	 * @param num
	 *            数字
	 * @return
	 */
	public static String padLeft(String pattern, int num) {
		java.text.DecimalFormat df = new java.text.DecimalFormat(pattern);
		return df.format(num);
	}

	/**
	 * 将给定的数字按给定的形式输出
	 * 
	 * @param d
	 *            double
	 * @param pattern
	 *            String #:表示有数字则输出数字，没有则空，如果输出位数多于＃的位数，则超长输入 0:有数字则输出数字，没有补0
	 *            对于小数，有几个＃或0，就保留几位的小数； 例如： "###.00"
	 *            -->表示输出的数值保留两位小数，不足两位的补0，多于两位的四舍五入 "###.0#"
	 *            -->表示输出的数值可以保留一位或两位小数；整数显示为有一位小数，一位或两位小数的按原样显示，多于两位的四舍五入；
	 *            "###" --->表示为整数，小数部分四舍五入 ".###" -->12.234显示为.234 "#,###.0#"
	 *            -->表示整数每隔3位加一个"，";
	 * @return String
	 */
	public static String formatNumber(double d, String pattern) {
		String s = "";
		try {
			DecimalFormat nf = (DecimalFormat) NumberFormat.getInstance(Locale
					.getDefault());
			nf.applyPattern(pattern);
			s = nf.format(d);

		} catch (Exception e) {
			//e.printStackTrace();
		}
		return s;

	}

	public static String formatNumber(Object obj, String pattern) {
		if (obj == null){
			return null;
		}
		String s = "";
		try {
			DecimalFormat df = new DecimalFormat(pattern);
			s = df.format(obj);
		} catch (Exception e) {
			//e.printStackTrace();
		}
		return s;
	}

}
