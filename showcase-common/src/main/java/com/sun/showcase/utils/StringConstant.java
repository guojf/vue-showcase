package com.sun.showcase.utils;

public class StringConstant {
	public static final String EMPTY="";
	public static final String COLON=":";
	public static final String COMMA=",";
	public static final String ZERO="0";
	public static final String ONE="1";
	public static final String TWO="2";
	public static final String THREE="3";
	public static final String FOUR="4";
	public static final String FIVE="5";
	public static final String SIX="6";
	public static final String SEVEN="7";
	public static final String EIGHT="8";
	public static final String NINE="9";
	public static final String ID="id";
	public static final String SUCCESS="SUCCESS";
	public static final String FIAL="FIAL";
	public static final String OK="OK";
}
