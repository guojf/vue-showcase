package com.sun.showcase.utils;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class JwtSubject {
	
	private Long userId;
	private String ip;
	private String version;
	
	public JwtSubject(Long userId) {
		super();
		this.userId = userId;
	}
	public JwtSubject(Long userId, String ip, String version) {
		super();
		this.userId = userId;
		this.ip = ip;
		this.version = version;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.DEFAULT_STYLE);
	}
	
}
