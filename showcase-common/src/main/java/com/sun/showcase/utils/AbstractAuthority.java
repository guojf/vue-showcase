package com.sun.showcase.utils;
public abstract class AbstractAuthority {
	private final Authority authority;
	
	public AbstractAuthority(Authority authority) {
		this.authority = authority;
	}

	public Authority getAuthentication() {
		return authority;
	}

	/**
	 * 是否拥有对url的访问
	 * @param url
	 * @return
	 */
	public abstract boolean hasUrlAuth(String url);
	
	/**
	 * 是否具有对某个资源的访问权限
	 * @param componentCode
	 * @return
	 */
	public boolean hasComponentAuth(String componentCode){
		return authority.getComponentResources().contains(componentCode);
	}
}
