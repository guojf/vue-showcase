package com.sun.showcase.exception;

public class DefaultException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 322541215395933591L;
    private String msg;
    private int code = 500;
    
	public DefaultException() {
		super();
	}
	
	public DefaultException(String msg) {
		super();
		this.msg = msg;
	}

	public DefaultException(String msg, int code) {
		super();
		this.msg = msg;
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
    
}
