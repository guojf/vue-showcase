package com.sun.showcase.pojo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.showcase.utils.GenericsUtils;
import com.sun.showcase.utils.ValidateUtil;
public class SearchModel<T> {

	
	/**
	 * 为mybatias 生成表文件的时候
	 */
	private java.lang.String tableAlias= GenericsUtils.getSuperClassGenricType(getClass()).getSimpleName()+"_";
	@JsonIgnore
	private Pager<T> pager = new Pager<T>();
	private Long  page=1L;	
	private Long  rows=10L;
	private Long  totalNum;
	protected String orderColum = "id";
	protected String orderRule = "desc";
	protected String opts;
	
	public String getOpts() {
		return opts;
	}
	public void setOpts(String opts) {
		this.opts = opts;
	}
	public Long getPage() {
		return page;
	}
	public void setPage(Long page) {
		if(!ValidateUtil.isValid(page)){
			page=Long.valueOf(1);
		}
		getPager().setCurrentPage(page);
		this.page = page;
	}
	public Long getRows() {
		
		return rows;
	}
	public void setRows(Long rows) {
		if(!ValidateUtil.isValid(rows)){
			rows=Long.valueOf(10);
		}
		getPager().setPageSize(rows);
		this.rows = rows;
	}
	public java.lang.String getTableAlias() {
		return tableAlias;
	}
	public void setTableAlias(java.lang.String tableAlias) {
		this.tableAlias = tableAlias;
	}
	@JsonIgnore
	public java.lang.String getColumAlias() {
		return tableAlias+".";
	}
	public Long getTotalNum() {
		totalNum = getPager().getTotalRecords();
		return totalNum;
	}
	public void setTotalNum(Long totalNum) {
		this.totalNum = totalNum;
	}
	public String getOrderColum() {
		return orderColum;
	}
	public void setOrderColum(String orderColum) {
		this.orderColum = orderColum;
	}
	public String getOrderRule() {
		return orderRule;
	}
	public void setOrderRule(String orderRule) {
		this.orderRule = orderRule;
	}
	public Pager<T> getPager() {
		return pager;
	}
	public void setPager(Pager<T> pager) {
		this.pager = pager;
	}
	
}
