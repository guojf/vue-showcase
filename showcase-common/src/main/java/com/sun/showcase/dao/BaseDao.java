package com.sun.showcase.dao;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
public interface BaseDao<E, PK extends Serializable>{
	public static final Logger log = LoggerFactory.getLogger("mybatis");
	public E getById(PK primaryKey);
	public void deleteById(PK id);
	public void save(E entity);
	public void update(E entity);
}
