package com.sun.showcase.excel;

import java.io.OutputStream;

public abstract interface ExcelExportTemplate<T> {
	public abstract void doExport(OutputStream paramOutputStream, T paramT) throws Exception;

	public abstract String[] getSheetNames();

	public abstract String[][] getTitles();

	public abstract String[] getCaptions();

	public abstract int getRowAccessWindowSize();
}
