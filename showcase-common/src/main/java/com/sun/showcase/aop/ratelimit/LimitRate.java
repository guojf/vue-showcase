package com.sun.showcase.aop.ratelimit;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface LimitRate {
	/**
	 * 每秒处理请求数量
	 * @return
	 */
	int rate() default 1;
	
}
