package com.sun.showcase.strategy.limit;

/**
 * 限流策略接口
 * @author SUN
 *
 */
public interface RateLimitStrategy {
	/**
	 * 
	 * @param key 限流维度，例如ip或用户id
	 * @param rate 流量大小
	 * @return 是否限流成功
	 */
	boolean doLimit(String key,int rate);
}
