package com.sun.showcase.entity;

import java.util.Objects;

public class Scope {
	private String code;
	private String name;
	private boolean cancellable;
	
	public Scope(String code) {
		super();
		this.code = code;
		this.name = code;
		this.cancellable = true;
	}
	public Scope(String code, boolean cancellable) {
		super();
		this.code = code;
		this.name = code;
		this.cancellable = cancellable;
	}
	public Scope(String code, String name, boolean cancellable) {
		super();
		this.code = code;
		this.name = name;
		this.cancellable = cancellable;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isCancellable() {
		return cancellable;
	}
	public void setCancellable(boolean cancellable) {
		this.cancellable = cancellable;
	}
	@Override
	public int hashCode() {
		return this.code.hashCode();
	}
	@Override
	public boolean equals(Object obj) {
	    if (this == obj) {               
	        return true;                  
	    }         
	    if (!(obj instanceof Scope)) {  
	        return false;               
	    } 
	    Scope that = (Scope)obj;
	    return Objects.equals(this.getCode(), that.getCode());
	}
	
}
