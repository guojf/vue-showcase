package com.sun.showcase.biz.basic.service;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import com.sun.showcase.client.domain.basic.UploadFile;

public interface FileDealService {
	
	//上传文件
	public UploadFile saveFile(File file,UploadFile upload);
	//上传文件
	public UploadFile saveFileAsStream(BufferedInputStream fiStream,UploadFile upload);
	//查询
	public UploadFile findFile(String id);
	
	//下载文件
	public InputStream findFile(UploadFile upload,OutputStream output);
	
	//删除文件
	public void deleteFile(String id);
	
    //检索附件
	List<UploadFile> getByIds(List<Long> list);

	//下载 
	public InputStream findAttach(UploadFile upload);
	//下载 
	public OutputStream findAttachOut(UploadFile upload);
	
}
