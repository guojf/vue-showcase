package com.sun.showcase.biz.user.service;

import java.util.List;

import com.sun.showcase.client.domain.user.PhoneUser;
import com.sun.showcase.client.query.user.PhoneUserQuery;
import com.sun.showcase.pojo.DataGrid;
import com.sun.showcase.pojo.Result;
public interface PhoneUserService{

	/**
	 * 获得数据表格
	 * 
	 * @param bug
	 * @return
	 */
	public DataGrid datagrid(PhoneUserQuery phoneUserQuery);

	/**
	 * 添加
	 * 
	 * @param phoneUserQuery
	 */
	public PhoneUser add(PhoneUserQuery phoneUserQuery);

	/**
	 * 修改
	 * 
	 * @param phoneUserQuery
	 */
	public void update(PhoneUserQuery phoneUserQuery) ;
	
	/**
	 *  根据条件更新部分字段
	 * 
	 * @param phoneUserQuery
	 */
	public void updatePart(PhoneUserQuery phoneUserQuery) ;

	/**
	 * 物理删除
	 * 
	 * @param ids
	 */
	public void delete(java.lang.Long[] ids);
	
	/**
	 * 逻辑删除 更新状态位
	 * 
	 * @param ids
	 */
	public void deletePt(java.lang.Long[] ids);

	/**
	 * 获得
	 * 
	 * @param PhoneUser
	 * @return
	 */
	public PhoneUser get(PhoneUserQuery phoneUserQuery);
	
	
	/**
	 * 获得
	 * 
	 * @param obid
	 * @return
	 */
	public PhoneUser get(Long id);
	
	/**
	 * 获取所有数据
	 */
	public List<PhoneUserQuery> listAll(PhoneUserQuery phoneUserQuery);
	/**
	 * 登陆
	 * @param phoneUserQuery
	 * @return
	 */
	
	public Result login(PhoneUserQuery phoneUserQuery);
	/**
	 * 注册
	 * @param phoneUserQuery
	 * @return
	 */
	
	public Result regist(PhoneUserQuery phoneUserQuery);
	public PhoneUser getByUserPhone(String userPhone);
}
