package com.sun.showcase.biz.basic.service;

import java.util.List;

import com.sun.showcase.pojo.DataGrid;
import com.sun.showcase.client.domain.basic.GroupInfo;
import com.sun.showcase.client.query.basic.GroupInfoQuery;
public interface GroupInfoService{

	/**
	 * 获得数据表格
	 * 
	 * @param bug
	 * @return
	 */
	public DataGrid datagrid(GroupInfoQuery groupInfoQuery);

	/**
	 * 添加
	 * 
	 * @param groupInfoQuery
	 */
	public GroupInfo add(GroupInfoQuery groupInfoQuery);

	/**
	 * 修改
	 * 
	 * @param groupInfoQuery
	 */
	public void update(GroupInfoQuery groupInfoQuery) ;
	

	/**
	 * 删除
	 * 
	 * @param ids
	 */
	public void delete(java.lang.String id);

	/**
	 * 获得
	 * 
	 * @param GroupInfo
	 * @return
	 */
	public GroupInfo get(GroupInfoQuery groupInfoQuery);
	
	
	/**
	 * 获得
	 * 
	 * @param obid
	 * @return
	 */
	public GroupInfo get(String id);
	
	/**
	 * 获取所有数据
	 */
	public List<GroupInfoQuery> listAll(GroupInfoQuery groupInfoQuery);

	
	
}
