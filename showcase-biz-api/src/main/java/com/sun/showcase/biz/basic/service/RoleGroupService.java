package com.sun.showcase.biz.basic.service;
import java.util.List;

import com.sun.showcase.pojo.DataGrid;
import com.sun.showcase.client.domain.basic.RoleGroup;
import com.sun.showcase.client.query.basic.RoleGroupQuery;
public interface RoleGroupService{

	/**
	 * 获得数据表格
	 * 
	 * @param bug
	 * @return
	 */
	public DataGrid datagrid(RoleGroupQuery roleGroupQuery);

	/**
	 * 添加
	 * 
	 * @param roleGroupQuery
	 */
	public RoleGroup add(RoleGroupQuery roleGroupQuery);

	/**
	 * 修改
	 * 
	 * @param roleGroupQuery
	 */
	public void update(RoleGroupQuery roleGroupQuery) ;
	

	/**
	 * 删除
	 * 
	 * @param ids
	 */
	public void delete(RoleGroupQuery roleGroupQuery);

	/**
	 * 获得
	 * 
	 * @param RoleGroup
	 * @return
	 */
	public RoleGroup get(RoleGroupQuery roleGroupQuery);
	
	
	/**
	 * 获得
	 * 
	 * @param obid
	 * @return
	 */
	public RoleGroup get(String id);
	
	/**
	 * 获取所有数据
	 */
	public List<RoleGroupQuery> listAll(RoleGroupQuery roleGroupQuery);

	
	
}
