package com.sun.showcase.biz.basic.service;

import java.util.List;

import com.sun.showcase.pojo.DataGrid;
import com.sun.showcase.client.domain.basic.RoleInfo;
import com.sun.showcase.client.domain.basic.RoleResource;
import com.sun.showcase.client.query.basic.RoleInfoQuery;
import com.sun.showcase.utils.ExecuteResult;
public interface RoleInfoService{

	/**
	 * 获得数据表格
	 * 
	 * @param bug
	 * @return
	 */
	public DataGrid datagrid(RoleInfoQuery roleInfoQuery);

	/**
	 * 添加
	 * 
	 * @param roleInfoQuery
	 */
	public ExecuteResult<RoleInfo> add(RoleInfoQuery roleInfoQuery);

	/**
	 * 修改
	 * 
	 * @param roleInfoQuery
	 */
	public ExecuteResult<RoleInfo> update(RoleInfoQuery roleInfoQuery) ;
	

	/**
	 * 删除
	 * 
	 * @param ids
	 */
	public void delete(java.lang.String[] ids);

	/**
	 * 获得
	 * 
	 * @param RoleInfo
	 * @return
	 */
	public RoleInfo get(RoleInfoQuery roleInfoQuery);
	
	
	/**
	 * 获得
	 * 
	 * @param obid
	 * @return
	 */
	public RoleInfo get(String id);
	
	/**
	 * 获取所有数据
	 */
	public List<RoleInfoQuery> listAll(RoleInfoQuery roleInfoQuery);
	
	//通过角色id 插叙角色资源表数据
	public List<RoleResource> getRoleResourceByRoleID(RoleInfoQuery roleInfoQuery);

	/**
	 * 通过资源id删除角色资源表数据
	 * @param resourceId
	 * @return
	 */
	public Integer delResourcceRole(String resourceId);
}
