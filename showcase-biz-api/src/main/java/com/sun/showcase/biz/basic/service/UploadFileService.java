package com.sun.showcase.biz.basic.service;

import java.util.List;

import com.sun.showcase.client.domain.basic.UploadFile;
import com.sun.showcase.client.query.basic.UploadFileQuery;
import com.sun.showcase.pojo.DataGrid;
public interface UploadFileService{

	/**
	 * 获得数据表格
	 * 
	 * @param bug
	 * @return
	 */
	public DataGrid datagrid(UploadFileQuery uploadFileQuery);

	/**
	 * 添加
	 * 
	 * @param uploadFileQuery
	 */
	public UploadFile add(UploadFileQuery uploadFileQuery);

	/**
	 * 修改
	 * 
	 * @param uploadFileQuery
	 */
	public void update(UploadFileQuery uploadFileQuery) ;
	
	/**
	 *  根据条件更新部分字段
	 * 
	 * @param uploadFileQuery
	 */
	public void updatePart(UploadFileQuery uploadFileQuery) ;

	/**
	 * 物理删除
	 * 
	 * @param ids
	 */
	public void delete(java.lang.Long[] ids);
	
	/**
	 * 逻辑删除 更新状态位
	 * 
	 * @param ids
	 */
	public void deletePt(java.lang.Long[] ids);

	/**
	 * 获得
	 * 
	 * @param UploadFile
	 * @return
	 */
	public UploadFile get(UploadFileQuery uploadFileQuery);
	
	
	/**
	 * 获得
	 * 
	 * @param obid
	 * @return
	 */
	public UploadFile get(Long id);
	
	/**
	 * 获取所有数据
	 */
	public List<UploadFileQuery> listAll(UploadFileQuery uploadFileQuery);

	
	
}
