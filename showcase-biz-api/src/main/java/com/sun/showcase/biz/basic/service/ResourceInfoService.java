package com.sun.showcase.biz.basic.service;

import java.util.List;
import java.util.Map;

import com.sun.showcase.pojo.DataGrid;
import com.sun.showcase.client.domain.basic.ResourceInfo;
import com.sun.showcase.client.query.basic.ResourceInfoQuery;
import com.sun.showcase.utils.TreeNode;
public interface ResourceInfoService{

	/**
	 * 获得数据表格
	 * 
	 * @param bug
	 * @return
	 */
	public DataGrid datagrid(ResourceInfoQuery resourceInfoQuery);

	/**
	 * 添加
	 * 
	 * @param resourceInfoQuery
	 */
	public ResourceInfo add(ResourceInfoQuery resourceInfoQuery);

	/**
	 * 修改
	 * 
	 * @param resourceInfoQuery
	 */
	public void update(ResourceInfoQuery resourceInfoQuery) ;
	

	/**
	 * 删除
	 * 
	 * @param ids
	 */
	public void delete(java.lang.String[] ids);

	/**
	 * 获得
	 * 
	 * @param ResourceInfo
	 * @return
	 */
	public ResourceInfo get(ResourceInfoQuery resourceInfoQuery);
	
	
	/**
	 * 获得
	 * 
	 * @param obid
	 * @return
	 */
	public ResourceInfo get(String id);
	
	/**
	 * 获取所有数据
	 */
	public List<ResourceInfoQuery> listAll(ResourceInfoQuery resourceInfoQuery);
	
	//通过用户ID 获取用户资源  权限拦截使用
	public List<ResourceInfoQuery> findByUserID(String userId);
	
	//根据用户ID 查询用户菜单权限
	public Map<ResourceInfoQuery, List<ResourceInfoQuery>> getResourceByUserID(String userId);
	//根据用户ID 查询用户菜单权限
	public List<ResourceInfoQuery> getResourceListByUserID(String userId);
	
	//创建角色时  资源树查询
	public List<TreeNode> findParentResource();
	
	//根据父资源id 获取子资源
	public List<ResourceInfo> getChildByPid(String prentId);
	
	//根据用户个人资源查询资源信息
	public List<ResourceInfo> findByPersonResource(String userId);
}
