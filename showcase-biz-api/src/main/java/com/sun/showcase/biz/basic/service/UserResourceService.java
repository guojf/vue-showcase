package com.sun.showcase.biz.basic.service;
import java.util.List;

import com.sun.showcase.pojo.DataGrid;
import com.sun.showcase.client.domain.basic.UserResource;
import com.sun.showcase.client.query.basic.UserResourceQuery;
import com.sun.showcase.utils.TreeNode;

public interface UserResourceService{

	/**
	 * 获得数据表格
	 * 
	 * @param bug
	 * @return
	 */
	public DataGrid datagrid(UserResourceQuery userResourceQuery);

	/**
	 * 添加
	 * 
	 * @param userResourceQuery
	 */
	public UserResource add(UserResourceQuery userResourceQuery);

	/**
	 * 修改
	 * 
	 * @param userResourceQuery
	 */
	public void update(UserResourceQuery userResourceQuery) ;
	

	/**
	 * 删除
	 * 
	 * @param ids
	 */
	public void delete(java.lang.String id);

	/**
	 * 获得
	 * 
	 * @param UserResource
	 * @return
	 */
	public UserResource get(UserResourceQuery userResourceQuery);
	
	
	/**
	 * 获得
	 * 
	 * @param obid
	 * @return
	 */
	public UserResource get(String id);
	
	/**
	 * 获取所有数据
	 */
	public List<UserResourceQuery> listAll(UserResourceQuery userResourceQuery);
	
	/**
	 * 检索个人资源信息
	 */
	public List<TreeNode> findResourceByUser(String userId);

	/**
	 * 依据资源删除
	 * @param resouceId
	 */
	public void deleteByResource(String resouceId);

	public List<TreeNode> findResourceByUserID(String userId);
	
}
