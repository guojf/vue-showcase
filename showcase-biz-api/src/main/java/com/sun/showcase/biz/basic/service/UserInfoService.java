package com.sun.showcase.biz.basic.service;
import java.util.List;

import com.sun.showcase.pojo.DataGrid;
import com.sun.showcase.client.domain.basic.UserInfo;
import com.sun.showcase.client.query.basic.UserInfoQuery;
import com.sun.showcase.utils.ExecuteResult;
public interface UserInfoService{

	/**
	 * 获得数据表格
	 * 
	 * @param bug
	 * @return
	 */
	public DataGrid datagrid(UserInfoQuery userInfoQuery);

	/**
	 * 添加
	 * 
	 * @param userInfoQuery
	 */
	public UserInfo add(UserInfoQuery userInfoQuery);

	/**
	 * 修改
	 * 
	 * @param userInfoQuery
	 */
	public void update(UserInfoQuery userInfoQuery) ;
	

	/**
	 * 删除
	 * 
	 * @param ids
	 */
	public void delete(java.lang.Long[] ids);

	/**
	 * 获得
	 * 
	 * @param UserInfo
	 * @return
	 */
	public UserInfo get(UserInfoQuery userInfoQuery);

	/**
	 * 获得
	 * 
	 * @param obid
	 * @return
	 */
	public UserInfo get(String id);
	
	/**
	 * 获取所有数据
	 */
	public List<UserInfoQuery> listAll(UserInfoQuery userInfoQuery);
	
	/**
	 * 登录验证
	 * @param userInfoQuery
	 * @return
	 */
	public ExecuteResult<UserInfo> login(UserInfoQuery userInfoQuery);
	
	/**
	 * 退出
	 * @param userInfoQuery
	 */
	public void logout(UserInfoQuery userInfoQuery);

	/**
	 * 修改密码
	 * @param userInfoQuery
	 */
	public void editPassword(UserInfoQuery userInfoQuery);
	
	/**
	 * 根据账号检索
	 * @param userInfoQuery
	 * @return
	 */
	public UserInfo getByName(UserInfoQuery userInfoQuery);
	/**
	 * 根据账号检索
	 * @param userInfoQuery
	 * @return
	 */
	public UserInfo getByName(String name);
	/**
	 * 编辑
	 * @param userInfoQuery
	 */
	public void modify(UserInfoQuery userInfoQuery);
	/**
	 * 编辑员工信息
	 * @param userInfoQuery
	 */
	public void editEmployee(UserInfoQuery userInfoQuery);

	
	
}
