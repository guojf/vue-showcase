package com.sun.showcase.biz.basic.service;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;

import com.sun.showcase.client.domain.basic.UploadFile;
public interface FtpClientService {

	//获取远程服务器文件
	public void findByFile(UploadFile uploadFile,OutputStream output);
	
	//保存到远程服务器
	public boolean saveFile(File file,UploadFile uploadFile);
	//保存到远程服务器
	public boolean saveFile(File file,String filePath,String fileName);
	//保存到远程服务器
	public boolean saveFileAsStream(BufferedInputStream fiStream,String filePath,String fileName);
	//保存到远程服务器
	public boolean saveFileAsStream(BufferedInputStream fiStream,UploadFile upload);
	//删除远程服务器文件
	public boolean deleteFile(UploadFile uploadFile);

	//下载附件
	public InputStream findAttach(UploadFile uploadFile);
	
	//生成目录
	public boolean generateDir();
}
