package com.sun.showcase.biz.basic.service;
import java.util.List;

import com.sun.showcase.pojo.DataGrid;
import com.sun.showcase.client.domain.basic.BaseInfo;
import com.sun.showcase.client.query.basic.BaseInfoQuery;
import com.sun.showcase.utils.TreeNode;
public interface BaseInfoService{

	/**
	 * 获得数据表格
	 * 
	 * @param bug
	 * @return
	 */
	public DataGrid datagrid(BaseInfoQuery baseseInfoQuery);

	/**
	 * 添加
	 * 
	 * @param baseseInfoQuery
	 */
	public BaseInfo add(BaseInfoQuery baseseInfoQuery);

	/**
	 * 修改
	 * 
	 * @param baseseInfoQuery
	 */
	public void update(BaseInfoQuery baseseInfoQuery) ;
	

	/**
	 * 删除
	 */
	public void delete(BaseInfoQuery baseseInfoQuery);

	/**
	 * 获得
	 * 
	 * @param BaseInfo
	 * @return
	 */
	public BaseInfo get(BaseInfoQuery baseseInfoQuery);
	
	
	/**
	 * 获得
	 * 
	 * @param obid
	 * @return
	 */
	public BaseInfo get(String id);
	
	/**
	 * 获取所有数据
	 */
	public List<BaseInfoQuery> listAll(BaseInfoQuery baseseInfoQuery);

	/**
	 * 获取树形结构
	 * @param baseseInfoQuery
	 * @return
	 */
	public List<TreeNode> findTrees(BaseInfoQuery baseseInfoQuery);

	/**
	 * 编码验证
	 * @param baseseInfoQuery
	 * @return
	 */
	public boolean findValidates(BaseInfoQuery baseseInfoQuery);
	/**
	 * 根据名称查找任意一个匹配的
	 */
	public BaseInfo getByItemName(String itemName);
}
