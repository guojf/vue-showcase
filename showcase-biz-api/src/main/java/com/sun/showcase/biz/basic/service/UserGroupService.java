package com.sun.showcase.biz.basic.service;
import java.util.List;

import com.sun.showcase.pojo.DataGrid;
import com.sun.showcase.client.domain.basic.UserGroup;
import com.sun.showcase.client.query.basic.UserGroupQuery;
import com.sun.showcase.utils.TreeNode;
public interface UserGroupService{

	/**
	 * 获得数据表格
	 * 
	 * @param bug
	 * @return
	 */
	public DataGrid datagrid(UserGroupQuery userGroupQuery);

	/**
	 * 添加
	 * 
	 * @param userGroupQuery
	 */
	public UserGroup add(UserGroupQuery userGroupQuery);

	/**
	 * 修改
	 * 
	 * @param userGroupQuery
	 */
	public void update(UserGroupQuery userGroupQuery) ;
	

	/**
	 * 删除
	 * 
	 * @param ids
	 */
	public void delete(UserGroupQuery userGroupQuery);

	/**
	 * 获得
	 * 
	 * @param UserGroup
	 * @return
	 */
	public UserGroup get(UserGroupQuery userGroupQuery);
	
	
	/**
	 * 获得
	 * 
	 * @param obid
	 * @return
	 */
	public UserGroup get(String id);
	
	/**
	 * 获取所有数据
	 */
	public List<UserGroupQuery> listAll(UserGroupQuery userGroupQuery);

	/*
	 * 获取特定组内权限
	 */
	public List<TreeNode> findGroupsInfo(UserGroupQuery userGroupQuery);
    /**
     * 删除
     * @param delUserGroupQuery
     */
	public void deleteByType(UserGroupQuery delUserGroupQuery);

	
	
}
