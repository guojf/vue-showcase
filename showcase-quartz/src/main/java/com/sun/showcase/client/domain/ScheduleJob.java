package com.sun.showcase.client.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class ScheduleJob implements java.io.Serializable{
	private static final long serialVersionUID = 5454155825314635342L;
	/**
	 * 任务调度参数key
	 */
    public static final String JOB_PARAM_KEY = "JOB_PARAM_KEY";
    /**
     * 任务id       db_column: id 
     */	
	private java.lang.Long id;
    /**
     * spring bean名称       db_column: bean_name 
     */	
	private java.lang.String beanName;
    /**
     * 方法名       db_column: method_name 
     */	
	private java.lang.String methodName;
    /**
     * 参数       db_column: params 
     */	
	private java.lang.String params;
    /**
     * cron表达式       db_column: cron_expression 
     */	
	private java.lang.String cronExpression;
    /**
     * 任务状态  0：正常  1：暂停       db_column: status 
     */	
	private Integer status;
    /**
     * 备注       db_column: remark 
     */	
	private java.lang.String remark;
    /**
     * 创建时间       db_column: create_time 
     */	
	private java.util.Date createTime;
	//columns END

	public ScheduleJob(){
	}

	public ScheduleJob(
		java.lang.Long id
	){
		this.id = id;
	}

	public void setId(java.lang.Long value) {
		this.id = value;
	}
	
	public java.lang.Long getId() {
		return this.id;
	}
	public void setBeanName(java.lang.String value) {
		this.beanName = value;
	}
	
	public java.lang.String getBeanName() {
		return this.beanName;
	}
	public void setMethodName(java.lang.String value) {
		this.methodName = value;
	}
	
	public java.lang.String getMethodName() {
		return this.methodName;
	}
	public void setParams(java.lang.String value) {
		this.params = value;
	}
	
	public java.lang.String getParams() {
		return this.params;
	}
	public void setCronExpression(java.lang.String value) {
		this.cronExpression = value;
	}
	
	public java.lang.String getCronExpression() {
		return this.cronExpression;
	}
	public void setStatus(Integer value) {
		this.status = value;
	}
	
	public Integer getStatus() {
		return this.status;
	}
	public void setRemark(java.lang.String value) {
		this.remark = value;
	}
	
	public java.lang.String getRemark() {
		return this.remark;
	}
	public void setCreateTime(java.util.Date value) {
		this.createTime = value;
	}
	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
	public int hashCode() {
		return new HashCodeBuilder()
			.append(getId())
			.toHashCode();
	}
	
	public boolean equals(Object obj) {
		if(obj instanceof ScheduleJob == false) return false;
		if(this == obj) return true;
		ScheduleJob other = (ScheduleJob)obj;
		return new EqualsBuilder()
			.append(getId(),other.getId())
			.isEquals();
	}
}

