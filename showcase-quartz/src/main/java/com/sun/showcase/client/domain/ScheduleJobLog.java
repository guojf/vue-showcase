package com.sun.showcase.client.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class ScheduleJobLog implements java.io.Serializable{
	private static final long serialVersionUID = 5454155825314635342L;
    /**
     * 任务日志id       db_column: id 
     */	
	private java.lang.Long id;
    /**
     * 任务id       db_column: job_id 
     */	
	private java.lang.Long jobId;
    /**
     * spring bean名称       db_column: bean_name 
     */	
	private java.lang.String beanName;
    /**
     * 方法名       db_column: method_name 
     */	
	private java.lang.String methodName;
    /**
     * 参数       db_column: params 
     */	
	private java.lang.String params;
    /**
     * 任务状态    0：成功    1：失败       db_column: status 
     */	
	private Integer status;
    /**
     * 失败信息       db_column: error 
     */	
	private java.lang.String error;
    /**
     * 耗时(单位：毫秒)       db_column: times 
     */	
	private java.lang.Integer times;
    /**
     * 创建时间       db_column: create_time 
     */	
	private java.util.Date createTime;
	//columns END

	public ScheduleJobLog(){
	}

	public ScheduleJobLog(
		java.lang.Long id
	){
		this.id = id;
	}

	public void setId(java.lang.Long value) {
		this.id = value;
	}
	
	public java.lang.Long getId() {
		return this.id;
	}
	public void setJobId(java.lang.Long value) {
		this.jobId = value;
	}
	
	public java.lang.Long getJobId() {
		return this.jobId;
	}
	public void setBeanName(java.lang.String value) {
		this.beanName = value;
	}
	
	public java.lang.String getBeanName() {
		return this.beanName;
	}
	public void setMethodName(java.lang.String value) {
		this.methodName = value;
	}
	
	public java.lang.String getMethodName() {
		return this.methodName;
	}
	public void setParams(java.lang.String value) {
		this.params = value;
	}
	
	public java.lang.String getParams() {
		return this.params;
	}
	public void setStatus(Integer value) {
		this.status = value;
	}
	
	public Integer getStatus() {
		return this.status;
	}
	public void setError(java.lang.String value) {
		this.error = value;
	}
	
	public java.lang.String getError() {
		return this.error;
	}
	public void setTimes(java.lang.Integer value) {
		this.times = value;
	}
	
	public java.lang.Integer getTimes() {
		return this.times;
	}
	public void setCreateTime(java.util.Date value) {
		this.createTime = value;
	}
	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
	public int hashCode() {
		return new HashCodeBuilder()
			.append(getId())
			.toHashCode();
	}
	
	public boolean equals(Object obj) {
		if(obj instanceof ScheduleJobLog == false) return false;
		if(this == obj) return true;
		ScheduleJobLog other = (ScheduleJobLog)obj;
		return new EqualsBuilder()
			.append(getId(),other.getId())
			.isEquals();
	}
}

