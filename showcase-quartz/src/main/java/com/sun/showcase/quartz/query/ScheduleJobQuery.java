package com.sun.showcase.quartz.query;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.sun.showcase.client.domain.ScheduleJob;
import com.sun.showcase.pojo.SearchModel;

public class ScheduleJobQuery extends SearchModel<ScheduleJob> implements Serializable {
    
  private static final long serialVersionUID = 3148176768559230877L;
    

	  /**
     * 任务id       db_column: id 
     */	
	private java.lang.Long id;
	  /**
     * spring bean名称       db_column: bean_name 
     */	
	private java.lang.String beanName;
	  /**
     * 方法名       db_column: method_name 
     */	
	private java.lang.String methodName;
	  /**
     * 参数       db_column: params 
     */	
	private java.lang.String params;
	  /**
     * cron表达式       db_column: cron_expression 
     */	
	private java.lang.String cronExpression;
	  /**
     * 任务状态  0：正常  1：暂停       db_column: status 
     */	
	private Integer status;
	  /**
     * 备注       db_column: remark 
     */	
	private java.lang.String remark;
	  /**
     * 创建时间       db_column: create_time 
     */	
	private java.util.Date createTime;
	/**
	 * 封装修改时的where条件  key为数据库字段值  value为条件值
	 * */
	private Map<String,Object> searchMap = new HashMap<String,Object>();
	/**
	 * 数据表主键id的数组
	 * */
	private java.lang.Long ids[];

	public java.lang.Long getId() {
		return this.id;
	}
	
	public void setId(java.lang.Long value) {
		this.id = value;
	}
	
	public java.lang.String getBeanName() {
		return this.beanName;
	}
	
	public void setBeanName(java.lang.String value) {
		this.beanName = value;
	}
	
	public java.lang.String getMethodName() {
		return this.methodName;
	}
	
	public void setMethodName(java.lang.String value) {
		this.methodName = value;
	}
	
	public java.lang.String getParams() {
		return this.params;
	}
	
	public void setParams(java.lang.String value) {
		this.params = value;
	}
	
	public java.lang.String getCronExpression() {
		return this.cronExpression;
	}
	
	public void setCronExpression(java.lang.String value) {
		this.cronExpression = value;
	}
	
	public Integer getStatus() {
		return this.status;
	}
	
	public void setStatus(Integer value) {
		this.status = value;
	}
	
	public java.lang.String getRemark() {
		return this.remark;
	}
	
	public void setRemark(java.lang.String value) {
		this.remark = value;
	}
	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	public void setCreateTime(java.util.Date value) {
		this.createTime = value;
	}
	
	public Map<String, Object> getSearchMap() {
		return searchMap;
	}
	
	public void setSearchMap(Map<String, Object> searchMap) {
		this.searchMap = searchMap;
	}
	
	public java.lang.Long[] getIds() {
		return ids;
	}
	
	public void setIds(java.lang.Long[] ids) {
		this.ids = ids;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.DEFAULT_STYLE);
	}
	
}

