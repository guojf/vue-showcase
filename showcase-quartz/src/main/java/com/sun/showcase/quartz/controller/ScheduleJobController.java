package com.sun.showcase.quartz.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sun.showcase.client.domain.ScheduleJob;
import com.sun.showcase.pojo.DataGrid;
import com.sun.showcase.pojo.Result;
import com.sun.showcase.quartz.query.ScheduleJobQuery;
import com.sun.showcase.quartz.service.ScheduleJobService;

@Controller
@RequestMapping(value="/basic/scheduleJob") 
public class ScheduleJobController{
	
	@Autowired
	private ScheduleJobService scheduleJobService;
	
	private Result json = new Result();
	/**
	 * 跳转到ScheduleJob管理页面
	 * 
	 * @return
	 */
	@RequestMapping(value="goScheduleJob")
	public String goScheduleJob() {
		return "basic/scheduleJob";
	}
	/**
	 * 跳转到ScheduleJob新增页面
	 * 
	 * @return
	 */
	@RequestMapping(value="handleScheduleJob")
	public String handleScheduleJob(@RequestParam(value="id",required=false) Long id ,ModelMap map) {
		return "basic/scheduleJob_rowEdit";
	}

	/**
	 * 跳转到查看desc页面
	 * 
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="showDesc")
	public ScheduleJob showDesc(ScheduleJobQuery scheduleJobQuery) {
		return scheduleJobService.get(scheduleJobQuery);
	}

	/**
	 * 获得分页数据表格
	 */
	@ResponseBody
	@RequestMapping(value = "/datagrid")
	public DataGrid datagrid(ScheduleJobQuery scheduleJobQuery) {
		return scheduleJobService.datagrid(scheduleJobQuery);
	}
	

	/**
	 * 添加一个ScheduleJob
	 */
	@ResponseBody
	@RequestMapping(value="add")
	public Result add(ScheduleJobQuery scheduleJobQuery) {
		scheduleJobService.add(scheduleJobQuery);
		json.setSuccess(true);
		json.setMsg("添加成功！");
		return json;
	}

	/**
	 * 编辑ScheduleJob
	 */
	@ResponseBody
	@RequestMapping(value="edit")
	public Result edit(ScheduleJobQuery scheduleJobQuery) {
		scheduleJobService.update(scheduleJobQuery);
		json.setSuccess(true);
		json.setMsg("编辑成功！");
		return json;
	}

	/**
	 * 物理删除ScheduleJob
	 */
	@ResponseBody
	@RequestMapping(value="delete")
	public Result delete(ScheduleJobQuery scheduleJobQuery) {
		scheduleJobService.delete(scheduleJobQuery.getIds());
		json.setSuccess(true);
		json.setMsg("删除成功！");
		return json;
	}
	
	/**
	 * 逻辑删除ScheduleJob
	 */
	@ResponseBody
	@RequestMapping(value="deletePt")
	public Result deletePt(ScheduleJobQuery scheduleJobQuery) {
		scheduleJobService.deletePt(scheduleJobQuery.getIds());
		json.setSuccess(true);
		json.setMsg("删除成功！");
		return json;
	}
	/**
	 * 立即执行
	 */
	@ResponseBody
	@RequestMapping(value="runNow")
	public Result runNow(ScheduleJobQuery scheduleJobQuery) {
		scheduleJobService.runNow(scheduleJobQuery.getIds());
		json.setSuccess(true);
		json.setMsg("执行成功！");
		return json;
	}
	/**
	 * 暂停
	 */
	@ResponseBody
	@RequestMapping(value="pause")
	public Result pause(ScheduleJobQuery scheduleJobQuery) {
		scheduleJobService.pause(scheduleJobQuery.getIds());
		json.setSuccess(true);
		json.setMsg("已暂停！");
		return json;
	}
	/**
	 * 恢复
	 */
	@ResponseBody
	@RequestMapping(value="resume")
	public Result resume(ScheduleJobQuery scheduleJobQuery) {
		scheduleJobService.resume(scheduleJobQuery.getIds());
		json.setSuccess(true);
		json.setMsg("已恢复！");
		return json;
	}
}
