package com.sun.showcase.quartz.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sun.showcase.client.domain.ScheduleJobLog;
import com.sun.showcase.pojo.DataGrid;
import com.sun.showcase.pojo.Result;
import com.sun.showcase.quartz.query.ScheduleJobLogQuery;
import com.sun.showcase.quartz.service.ScheduleJobLogService;

@Controller
@RequestMapping(value="/basic/scheduleJobLog") 
public class ScheduleJobLogController{
	
	@Autowired
	private ScheduleJobLogService scheduleJobLogService;
	
	private Result json = new Result();
	/**
	 * 跳转到ScheduleJobLog管理页面
	 * 
	 * @return
	 */
	@RequestMapping(value="goScheduleJobLog")
	public String goScheduleJobLog() {
		return "basic/scheduleJobLog";
	}
	/**
	 * 跳转到ScheduleJobLog新增页面
	 * 
	 * @return
	 */
	@RequestMapping(value="handleScheduleJobLog")
	public String handleScheduleJobLog(@RequestParam(value="id",required=false) Long id ,ModelMap map) {
		return "basic/scheduleJobLog_rowEdit";
	}

	/**
	 * 跳转到查看desc页面
	 * 
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="showDesc")
	public ScheduleJobLog showDesc(ScheduleJobLogQuery scheduleJobLogQuery) {
		return scheduleJobLogService.get(scheduleJobLogQuery);
	}

	/**
	 * 获得分页数据表格
	 */
	@ResponseBody
	@RequestMapping(value = "/datagrid")
	public DataGrid datagrid(ScheduleJobLogQuery scheduleJobLogQuery) {
		return scheduleJobLogService.datagrid(scheduleJobLogQuery);
	}
	

	/**
	 * 添加一个ScheduleJobLog
	 */
	@ResponseBody
	@RequestMapping(value="add")
	public Result add(ScheduleJobLogQuery scheduleJobLogQuery) {
		scheduleJobLogService.add(scheduleJobLogQuery);
		json.setSuccess(true);
		json.setMsg("添加成功！");
		return json;
	}

	/**
	 * 编辑ScheduleJobLog
	 */
	@ResponseBody
	@RequestMapping(value="edit")
	public Result edit(ScheduleJobLogQuery scheduleJobLogQuery) {
		scheduleJobLogService.update(scheduleJobLogQuery);
		json.setSuccess(true);
		json.setMsg("编辑成功！");
		return json;
	}

	/**
	 * 物理删除ScheduleJobLog
	 */
	@ResponseBody
	@RequestMapping(value="delete")
	public Result delete(ScheduleJobLogQuery scheduleJobLogQuery) {
		scheduleJobLogService.delete(scheduleJobLogQuery.getIds());
		json.setSuccess(true);
		json.setMsg("删除成功！");
		return json;
	}
	
	/**
	 * 逻辑删除ScheduleJobLog
	 */
	@ResponseBody
	@RequestMapping(value="deletePt")
	public Result deletePt(ScheduleJobLogQuery scheduleJobLogQuery) {
		scheduleJobLogService.deletePt(scheduleJobLogQuery.getIds());
		json.setSuccess(true);
		json.setMsg("删除成功！");
		return json;
	}
}
