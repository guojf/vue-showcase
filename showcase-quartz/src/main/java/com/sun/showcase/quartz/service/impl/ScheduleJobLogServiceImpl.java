
package com.sun.showcase.quartz.service.impl;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sun.showcase.client.domain.ScheduleJobLog;
import com.sun.showcase.pojo.DataGrid;
import com.sun.showcase.pojo.Pager;
import com.sun.showcase.quartz.dao.ScheduleJobLogDao;
import com.sun.showcase.quartz.query.ScheduleJobLogQuery;
import com.sun.showcase.quartz.service.ScheduleJobLogService;
@Service("scheduleJobLogService")
@Transactional
public class ScheduleJobLogServiceImpl implements ScheduleJobLogService{
	@Autowired
	private ScheduleJobLogDao scheduleJobLogDao;
	
	public void setScheduleJobLogDao(ScheduleJobLogDao dao) {
		this.scheduleJobLogDao = dao;
	}

	public DataGrid datagrid(ScheduleJobLogQuery scheduleJobLogQuery) {
		DataGrid j = new DataGrid();
		Pager<ScheduleJobLog> pager  = find(scheduleJobLogQuery);
		j.setRows(getQuerysFromEntitys(pager.getRecords()));
		j.setTotal(pager.getTotalRecords());
		return j;
	}

	private List<ScheduleJobLogQuery> getQuerysFromEntitys(List<ScheduleJobLog> scheduleJobLogs) {
		List<ScheduleJobLogQuery> scheduleJobLogQuerys = new ArrayList<ScheduleJobLogQuery>();
		if (scheduleJobLogs != null && scheduleJobLogs.size() > 0) {
			for (ScheduleJobLog tb : scheduleJobLogs) {
				ScheduleJobLogQuery b = new ScheduleJobLogQuery();
				BeanUtils.copyProperties(tb, b);
				scheduleJobLogQuerys.add(b);
			}
		}
		return scheduleJobLogQuerys;
	}

	private Pager<ScheduleJobLog> find(ScheduleJobLogQuery scheduleJobLogQuery) {
		Page<ScheduleJobLog> page = PageHelper.startPage(scheduleJobLogQuery.getPage().intValue(), scheduleJobLogQuery.getRows().intValue());
		List<ScheduleJobLog> list = scheduleJobLogDao.findList(scheduleJobLogQuery);
		Pager<ScheduleJobLog> pager = new Pager<ScheduleJobLog>();
		pager.setRecords(list);
		pager.setTotalRecords(page.getTotal());
		return  pager;
		
	}
	


	public ScheduleJobLog add(ScheduleJobLogQuery scheduleJobLogQuery) {
		ScheduleJobLog t = new ScheduleJobLog();
		BeanUtils.copyProperties(scheduleJobLogQuery, t);
		scheduleJobLogDao.save(t);
		scheduleJobLogQuery.setId(t.getId());
		return t;
		
	}

	public void update(ScheduleJobLogQuery scheduleJobLogQuery) {
		ScheduleJobLog t = scheduleJobLogDao.getById(scheduleJobLogQuery.getId());
	    if(t != null) {
	    	BeanUtils.copyProperties(scheduleJobLogQuery, t);
	    	scheduleJobLogDao.update(t);
		}
	}
	
	/**
	 * 根据条件更新部分字段   新建一个query封装需要更新的字段
	 * searchMap封装更新条件
	 * */
	public void updatePart(ScheduleJobLogQuery scheduleJobLogQuery) {
	    scheduleJobLogDao.updatePart(scheduleJobLogQuery);
	}

	public void delete(java.lang.Long[] ids) {
		scheduleJobLogDao.deleteAc(ids);
	}
	
	//逻辑删除 更新状态位
	public void deletePt(java.lang.Long[] ids) {
		scheduleJobLogDao.deletePt(ids);
	}

	public ScheduleJobLog get(ScheduleJobLogQuery scheduleJobLogQuery) {
		return scheduleJobLogDao.getById(new Long(scheduleJobLogQuery.getId()));
	}

	public ScheduleJobLog get(Long id) {
		return scheduleJobLogDao.getById(id);
	}

	
	public List<ScheduleJobLogQuery> listAll(ScheduleJobLogQuery scheduleJobLogQuery) {
	    List<ScheduleJobLog> list = scheduleJobLogDao.findList(scheduleJobLogQuery);
		List<ScheduleJobLogQuery> listQuery =getQuerysFromEntitys(list) ;
		return listQuery;
	}
	
	
}
