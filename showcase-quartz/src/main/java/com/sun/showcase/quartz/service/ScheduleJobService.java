package com.sun.showcase.quartz.service;

import java.util.List;

import com.sun.showcase.client.domain.ScheduleJob;
import com.sun.showcase.pojo.DataGrid;
import com.sun.showcase.quartz.query.ScheduleJobQuery;
public interface ScheduleJobService{

	/**
	 * 获得数据表格
	 * 
	 * @param bug
	 * @return
	 */
	public DataGrid datagrid(ScheduleJobQuery scheduleJobQuery);

	/**
	 * 添加
	 * 
	 * @param scheduleJobQuery
	 */
	public ScheduleJob add(ScheduleJobQuery scheduleJobQuery);

	/**
	 * 修改
	 * 
	 * @param scheduleJobQuery
	 */
	public void update(ScheduleJobQuery scheduleJobQuery) ;
	
	/**
	 *  根据条件更新部分字段
	 * 
	 * @param scheduleJobQuery
	 */
	public void updatePart(ScheduleJobQuery scheduleJobQuery) ;

	/**
	 * 物理删除
	 * 
	 * @param ids
	 */
	public void delete(java.lang.Long[] ids);
	
	/**
	 * 逻辑删除 更新状态位
	 * 
	 * @param ids
	 */
	public void deletePt(java.lang.Long[] ids);

	/**
	 * 获得
	 * 
	 * @param ScheduleJob
	 * @return
	 */
	public ScheduleJob get(ScheduleJobQuery scheduleJobQuery);
	
	
	/**
	 * 获得
	 * 
	 * @param obid
	 * @return
	 */
	public ScheduleJob get(Long id);
	
	/**
	 * 获取所有数据
	 */
	public List<ScheduleJobQuery> listAll(ScheduleJobQuery scheduleJobQuery);
	/**
	 * 立即执行
	 */
	public void runNow(Long[] ids);
	/**
	 * 暂停
	 */
	public void pause(Long[] ids);
	/**
	 * 恢复
	 */
	public void resume(Long[] ids);
}
