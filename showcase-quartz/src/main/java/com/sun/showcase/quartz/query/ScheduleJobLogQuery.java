package com.sun.showcase.quartz.query;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.sun.showcase.client.domain.ScheduleJobLog;
import com.sun.showcase.pojo.SearchModel;

public class ScheduleJobLogQuery extends SearchModel<ScheduleJobLog> implements Serializable {
    
  private static final long serialVersionUID = 3148176768559230877L;
    

	  /**
     * 任务日志id       db_column: id 
     */	
	private java.lang.Long id;
	  /**
     * 任务id       db_column: job_id 
     */	
	private java.lang.Long jobId;
	  /**
     * spring bean名称       db_column: bean_name 
     */	
	private java.lang.String beanName;
	  /**
     * 方法名       db_column: method_name 
     */	
	private java.lang.String methodName;
	  /**
     * 参数       db_column: params 
     */	
	private java.lang.String params;
	  /**
     * 任务状态    0：成功    1：失败       db_column: status 
     */	
	private Integer status;
	  /**
     * 失败信息       db_column: error 
     */	
	private java.lang.String error;
	  /**
     * 耗时(单位：毫秒)       db_column: times 
     */	
	private java.lang.Integer times;
	  /**
     * 创建时间       db_column: create_time 
     */	
	private java.util.Date createTime;
	/**
	 * 封装修改时的where条件  key为数据库字段值  value为条件值
	 * */
	private Map<String,Object> searchMap = new HashMap<String,Object>();
	/**
	 * 数据表主键id的数组
	 * */
	private java.lang.Long ids[];

	private java.util.Date startCreateTime;
	
	private java.util.Date stopCreateTime;
	
	public java.util.Date getStartCreateTime() {
		return startCreateTime;
	}

	public void setStartCreateTime(java.util.Date startCreateTime) {
		this.startCreateTime = startCreateTime;
	}

	public java.util.Date getStopCreateTime() {
		return stopCreateTime;
	}

	public void setStopCreateTime(java.util.Date stopCreateTime) {
		this.stopCreateTime = stopCreateTime;
	}

	public java.lang.Long getId() {
		return this.id;
	}
	
	public void setId(java.lang.Long value) {
		this.id = value;
	}
	
	public java.lang.Long getJobId() {
		return this.jobId;
	}
	
	public void setJobId(java.lang.Long value) {
		this.jobId = value;
	}
	
	public java.lang.String getBeanName() {
		return this.beanName;
	}
	
	public void setBeanName(java.lang.String value) {
		this.beanName = value;
	}
	
	public java.lang.String getMethodName() {
		return this.methodName;
	}
	
	public void setMethodName(java.lang.String value) {
		this.methodName = value;
	}
	
	public java.lang.String getParams() {
		return this.params;
	}
	
	public void setParams(java.lang.String value) {
		this.params = value;
	}
	
	public Integer getStatus() {
		return this.status;
	}
	
	public void setStatus(Integer value) {
		this.status = value;
	}
	
	public java.lang.String getError() {
		return this.error;
	}
	
	public void setError(java.lang.String value) {
		this.error = value;
	}
	
	public java.lang.Integer getTimes() {
		return this.times;
	}
	
	public void setTimes(java.lang.Integer value) {
		this.times = value;
	}
	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	public void setCreateTime(java.util.Date value) {
		this.createTime = value;
	}
	
	public Map<String, Object> getSearchMap() {
		return searchMap;
	}
	
	public void setSearchMap(Map<String, Object> searchMap) {
		this.searchMap = searchMap;
	}
	
	public java.lang.Long[] getIds() {
		return ids;
	}
	
	public void setIds(java.lang.Long[] ids) {
		this.ids = ids;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.DEFAULT_STYLE);
	}
	
}

