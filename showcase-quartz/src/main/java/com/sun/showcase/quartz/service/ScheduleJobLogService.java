package com.sun.showcase.quartz.service;

import java.util.List;

import com.sun.showcase.client.domain.ScheduleJobLog;
import com.sun.showcase.pojo.DataGrid;
import com.sun.showcase.quartz.query.ScheduleJobLogQuery;
public interface ScheduleJobLogService{

	/**
	 * 获得数据表格
	 * 
	 * @param bug
	 * @return
	 */
	public DataGrid datagrid(ScheduleJobLogQuery scheduleJobLogQuery);

	/**
	 * 添加
	 * 
	 * @param scheduleJobLogQuery
	 */
	public ScheduleJobLog add(ScheduleJobLogQuery scheduleJobLogQuery);

	/**
	 * 修改
	 * 
	 * @param scheduleJobLogQuery
	 */
	public void update(ScheduleJobLogQuery scheduleJobLogQuery) ;
	
	/**
	 *  根据条件更新部分字段
	 * 
	 * @param scheduleJobLogQuery
	 */
	public void updatePart(ScheduleJobLogQuery scheduleJobLogQuery) ;

	/**
	 * 物理删除
	 * 
	 * @param ids
	 */
	public void delete(java.lang.Long[] ids);
	
	/**
	 * 逻辑删除 更新状态位
	 * 
	 * @param ids
	 */
	public void deletePt(java.lang.Long[] ids);

	/**
	 * 获得
	 * 
	 * @param ScheduleJobLog
	 * @return
	 */
	public ScheduleJobLog get(ScheduleJobLogQuery scheduleJobLogQuery);
	
	
	/**
	 * 获得
	 * 
	 * @param obid
	 * @return
	 */
	public ScheduleJobLog get(Long id);
	
	/**
	 * 获取所有数据
	 */
	public List<ScheduleJobLogQuery> listAll(ScheduleJobLogQuery scheduleJobLogQuery);

	
	
}
