package com.sun.showcase.quartz.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.sun.showcase.client.domain.ScheduleJobLog;
import com.sun.showcase.dao.BaseDao;
import com.sun.showcase.quartz.query.ScheduleJobLogQuery;

@Mapper
public interface ScheduleJobLogDao extends BaseDao<ScheduleJobLog,java.lang.Long>{
	/**
	 * 查询列表
	 */
	public List<ScheduleJobLog> findList(ScheduleJobLogQuery scheduleJobLogQuery);
	
	/**
	 * 根据条件更新部分字段   新建一个query封装需要更新的字段
	 * searchMap封装更新条件
	 * */
	public void updatePart(ScheduleJobLogQuery scheduleJobLogQuery);
	/**
	 * 逻辑删除
	 * @param ids
	 */
	public void deletePt(Long[] ids);
	/**
	 * 物理删除
	 * @param ids
	 */
	public void deleteAc(Long[] ids);

}
