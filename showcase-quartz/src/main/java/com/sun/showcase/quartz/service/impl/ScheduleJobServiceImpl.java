
package com.sun.showcase.quartz.service.impl;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.quartz.Scheduler;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sun.showcase.client.domain.ScheduleJob;
import com.sun.showcase.pojo.DataGrid;
import com.sun.showcase.pojo.Pager;
import com.sun.showcase.quartz.dao.ScheduleJobDao;
import com.sun.showcase.quartz.enums.ScheduleStatus;
import com.sun.showcase.quartz.query.ScheduleJobQuery;
import com.sun.showcase.quartz.service.ScheduleJobService;
import com.sun.showcase.quartz.utils.ScheduleUtils;
@Service("scheduleJobService")
@Transactional
public class ScheduleJobServiceImpl implements ScheduleJobService{
	@Autowired
	private ScheduleJobDao scheduleJobDao;
	@Autowired
    private Scheduler scheduler;
	
	public void setScheduleJobDao(ScheduleJobDao dao) {
		this.scheduleJobDao = dao;
	}

	public DataGrid datagrid(ScheduleJobQuery scheduleJobQuery) {
		DataGrid j = new DataGrid();
		Pager<ScheduleJob> pager  = find(scheduleJobQuery);
		j.setRows(getQuerysFromEntitys(pager.getRecords()));
		j.setTotal(pager.getTotalRecords());
		return j;
	}

	private List<ScheduleJobQuery> getQuerysFromEntitys(List<ScheduleJob> scheduleJobs) {
		List<ScheduleJobQuery> scheduleJobQuerys = new ArrayList<ScheduleJobQuery>();
		if (scheduleJobs != null && scheduleJobs.size() > 0) {
			for (ScheduleJob tb : scheduleJobs) {
				ScheduleJobQuery b = new ScheduleJobQuery();
				BeanUtils.copyProperties(tb, b);
				scheduleJobQuerys.add(b);
			}
		}
		return scheduleJobQuerys;
	}

	private Pager<ScheduleJob> find(ScheduleJobQuery scheduleJobQuery) {
		Page<ScheduleJob> page = PageHelper.startPage(scheduleJobQuery.getPage().intValue(), scheduleJobQuery.getRows().intValue());
		List<ScheduleJob> list = scheduleJobDao.findList(scheduleJobQuery);
		Pager<ScheduleJob> pager = new Pager<ScheduleJob>();
		pager.setRecords(list);
		pager.setTotalRecords(page.getTotal());
		return  pager;
		
	}
	


	public ScheduleJob add(ScheduleJobQuery scheduleJobQuery) {
		ScheduleJob t = new ScheduleJob();
		scheduleJobQuery.setCreateTime(new Date());
		scheduleJobQuery.setStatus(ScheduleStatus.NORMAL.getValue());
		BeanUtils.copyProperties(scheduleJobQuery, t);
		scheduleJobDao.save(t);
		scheduleJobQuery.setId(t.getId());
		ScheduleUtils.createScheduleJob(scheduler, t);
		return t;
		
	}

	public void update(ScheduleJobQuery scheduleJobQuery) {
		ScheduleJob t = scheduleJobDao.getById(scheduleJobQuery.getId());
	    if(t != null) {
	    	BeanUtils.copyProperties(scheduleJobQuery, t);
	    	ScheduleUtils.updateScheduleJob(scheduler, t);
	    	scheduleJobDao.update(t);
		}
	}
	
	/**
	 * 根据条件更新部分字段   新建一个query封装需要更新的字段
	 * searchMap封装更新条件
	 * */
	public void updatePart(ScheduleJobQuery scheduleJobQuery) {
	    scheduleJobDao.updatePart(scheduleJobQuery);
	}

	public void delete(java.lang.Long[] ids) {
		for(Long id :ids){
			ScheduleUtils.deleteScheduleJob(scheduler, id);
		}
		scheduleJobDao.deleteAc(ids);
	}
	
	//逻辑删除 更新状态位
	public void deletePt(java.lang.Long[] ids) {
		scheduleJobDao.deletePt(ids);
	}

	public ScheduleJob get(ScheduleJobQuery scheduleJobQuery) {
		return scheduleJobDao.getById(new Long(scheduleJobQuery.getId()));
	}

	public ScheduleJob get(Long id) {
		return scheduleJobDao.getById(id);
	}

	
	public List<ScheduleJobQuery> listAll(ScheduleJobQuery scheduleJobQuery) {
	    List<ScheduleJob> list = scheduleJobDao.findList(scheduleJobQuery);
		List<ScheduleJobQuery> listQuery =getQuerysFromEntitys(list) ;
		return listQuery;
	}

	@Override
	public void runNow(Long[] ids) {
		for(Long id : ids){
			ScheduleUtils.run(scheduler, this.get(id));
		}
	}

	@Override
	public void pause(Long[] ids) {
		for(Long id : ids){
			ScheduleUtils.pauseJob(scheduler, id);
		}
		ScheduleJobQuery scheduleJobQuery = new ScheduleJobQuery();
		scheduleJobQuery.setStatus(ScheduleStatus.PAUSE.getValue());
		scheduleJobQuery.getSearchMap().put("ids", ids);
		this.updatePart(scheduleJobQuery);
	}

	@Override
	public void resume(Long[] ids) {
		for(Long id : ids){
			ScheduleUtils.resumeJob(scheduler, id);
		}
		ScheduleJobQuery scheduleJobQuery = new ScheduleJobQuery();
		scheduleJobQuery.setStatus(ScheduleStatus.NORMAL.getValue());
		scheduleJobQuery.getSearchMap().put("ids", ids);
		this.updatePart(scheduleJobQuery);
	}
	
	
}
