package com.sun.showcase.exception;

import org.springframework.http.HttpStatus;

public class InvalidTokenException extends DefaultException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1627993553667602210L;
	private static final String DEFAULT_MSG = "无效token";
	public InvalidTokenException() {
		this.setMsg(DEFAULT_MSG);
		this.setCode(HttpStatus.UNAUTHORIZED.value());
	}

}
