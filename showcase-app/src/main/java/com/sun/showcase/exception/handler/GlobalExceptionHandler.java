package com.sun.showcase.exception.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.sun.showcase.exception.DefaultException;
import com.sun.showcase.exception.InvalidTokenException;
import com.sun.showcase.exception.OutLimitRateException;
import com.sun.showcase.pojo.Result;
@RestControllerAdvice
public class GlobalExceptionHandler {
	private Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);
	
    public static final String DEFAULT_ERROR_MSG = "系统异常！";
    @ExceptionHandler(value = Exception.class)
    public Result defaultErrorHandler(Exception e) throws Exception {
    	Result r = new Result();
    	r.setSuccess(false);
    	r.setMsg(DEFAULT_ERROR_MSG);
    	logger.error("系统异常,{},{}",e.getMessage());
    	if(logger.isDebugEnabled()){
    		e.getStackTrace();
    	}
        return r;
    }
    @ExceptionHandler(value = DefaultException.class)
    public Result defaultExceptionHandler(DefaultException e) throws Exception {
    	logger.error("DefaultException,{},{},{}",e.getMessage(),e.getCode(),e.getMsg());
    	Result r = new Result();
    	r.setSuccess(false);
    	r.setMsg(e.getMsg());
    	r.setCode(String.valueOf(e.getCode()));
        return r;
    }
    @ExceptionHandler(value = InvalidTokenException.class)
    public Result invalidTokenExceptionHandler(InvalidTokenException e) throws Exception {
    	logger.info("InvalidTokenException,{},{},{}",e.getMessage(),e.getCode(),e.getMsg());
    	Result r = new Result();
    	r.setSuccess(false);
    	r.setMsg(e.getMsg());
    	r.setCode(String.valueOf(e.getCode()));
        return r;
    }
    @ExceptionHandler(value = OutLimitRateException.class)
    public Result outLimitRateExceptionHandler(OutLimitRateException e) throws Exception {
    	logger.info("OutLimitRateException,{},{},{}",e.getMessage(),e.getCode(),e.getMsg());
    	Result r = new Result();
    	r.setSuccess(false);
    	r.setMsg("您的手速太快，请稍后重试！");
    	r.setCode(String.valueOf(e.getCode()));
    	return r;
    }
}
