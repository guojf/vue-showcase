package com.sun.showcase.controller.user;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sun.showcase.annotation.LoginAuth;
import com.sun.showcase.aop.ratelimit.LimitRate;
import com.sun.showcase.biz.user.service.PhoneUserService;
import com.sun.showcase.client.query.user.PhoneUserQuery;
import com.sun.showcase.config.UserDetailsImpl;
import com.sun.showcase.pojo.Result;
import com.sun.showcase.utils.LoginContextHolder;

@RestController
public class PhoneUserController{
	
	@Autowired
	private PhoneUserService phoneUserService;
	@Autowired
	ConsumerTokenServices consumerTokenServices;
	/**
	 * 测试
	 * @param phoneUserQuery
	 * @return
	 */
	@RequestMapping(value="/limit")
	@ResponseBody
	@LimitRate(rate=5)
	public Result limit(PhoneUserQuery phoneUserQuery) {
		Result res = new Result();
		res.setSuccess(true);
		return res;
	}
	/**
	 * 测试
	 * @param phoneUserQuery
	 * @return
	 */
	@RequestMapping(value="/hello")
	@ResponseBody
	@LoginAuth
	public Result hello(PhoneUserQuery phoneUserQuery) {
		Result res = new Result();
		res.setSuccess(true);
		res.setData(LoginContextHolder.get().getUserId());
		return res;
	}
	/**
	 * 测试
	 * @param phoneUserQuery
	 * @return
	 */
	@RequestMapping(value="/authUser/auth_userinfo")
	@ResponseBody
	public Result orderB(PhoneUserQuery phoneUserQuery) {
		Result res = new Result();
		res.setSuccess(true);
		Map<String,Object> data = new HashMap<String,Object>();
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication instanceof OAuth2Authentication) {
			OAuth2Authentication auth = (OAuth2Authentication) authentication;
			UserDetailsImpl user = (UserDetailsImpl)auth.getPrincipal();
			data.put("user_id", user.getId());
			data.put("user_name", user.getUserName());
			//data.put("params", auth.getOAuth2Request().getRequestParameters());
		}
		res.setData(data);
		return res;
	}
	/**
	 * token 注销
	 * @param access_token
	 * @return
	 */
	@RequestMapping(value="/revokeToken")
	@ResponseBody
    public String revokeToken(String access_token) {
        if (consumerTokenServices.revokeToken(access_token)){
            return "注销成功";
        }else{
            return "注销失败";
        }
    }
	/**
	 * 登录
	 * @param phoneUserQuery
	 * @return
	 */
	@RequestMapping(value="/phone/login")
	@ResponseBody
	public Result login(PhoneUserQuery phoneUserQuery) {
		return phoneUserService.login(phoneUserQuery);
	}
	/**
	 * 注册
	 * @param phoneUserQuery
	 * @return
	 */
	@RequestMapping(value="/phone/regist")
	@ResponseBody
	public Result regist(PhoneUserQuery phoneUserQuery) {
		return phoneUserService.regist(phoneUserQuery);
	}
}
