package com.sun.showcase.config;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.sun.showcase.biz.basic.utils.FtpClientConstants;
@WebListener
public class MyServletContextListener implements ServletContextListener{
	private Logger logger =  LoggerFactory.getLogger(this.getClass());
	@Autowired
	private FtpClientConstants ftpClientConstants;
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		WebApplicationContextUtils.getRequiredWebApplicationContext(sce.getServletContext())  
        						  .getAutowireCapableBeanFactory().autowireBean(this);
		ServletContext servletContext = sce.getServletContext();
		servletContext.setAttribute("httpURL",ftpClientConstants.getPrefixUrl());
		logger.info("liting: contextInitialized");
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		logger.info("liting: contextDestroyed");
	}

}
