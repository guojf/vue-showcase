package com.sun.showcase.config;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.sun.showcase.biz.user.service.PhoneUserService;
import com.sun.showcase.client.domain.user.PhoneUser;

public class PhoneUserDetailsServiceImpl implements UserDetailsService{

	@Autowired
	PhoneUserService phoneUserService;
	
	public UserDetails loadUserByUsername(String userName)throws UsernameNotFoundException {
		
		PhoneUser t = phoneUserService.getByUserPhone(userName);
		
		if(null == t)
			throw new UsernameNotFoundException("用户 "+userName+" 不存在！");
		
		//加载用户权限
		UserDetailsImpl userDetail = new UserDetailsImpl(); 
		BeanUtils.copyProperties(t, userDetail);
		return userDetail;
	}

}
