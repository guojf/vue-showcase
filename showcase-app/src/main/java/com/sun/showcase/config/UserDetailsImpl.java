package com.sun.showcase.config;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.sun.showcase.client.domain.user.PhoneUser;

public class UserDetailsImpl extends PhoneUser implements UserDetails{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4028650001684762649L;

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return new ArrayList<GrantedAuthority>();
	}

	@Override
	public String getUsername() {
		return super.getUserName();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return "1".equals(super.getActiveFlag())?true:false;
	}

}
