-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        5.5.20-log - MySQL Community Server (GPL)
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- 正在导出表  showcase.basese_info 的数据：~1 rows (大约)
/*!40000 ALTER TABLE `basese_info` DISABLE KEYS */;
INSERT INTO `basese_info` (`ID`, `ITEM_NAME`, `ITEM_CODE`, `ITEM_CODE_COPY`, `PARENT_NAME`, `PARENT_CODE`, `IS_LEAF`, `ORDER_NUM`, `FILE_ID`, `FILE_NAME`, `FILE_PATH`, `REMARK`, `ACTIVE_FLAG`, `CREATE_DATE`, `CREATE_BY`, `MODIFIED_DATE`, `MODIFIED_BY`) VALUES
	(1, '基础参数', 'A', '', '基础参数', NULL, '0', NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, '2015-11-24', NULL);
/*!40000 ALTER TABLE `basese_info` ENABLE KEYS */;

-- 正在导出表  showcase.group_info 的数据：~2 rows (大约)
/*!40000 ALTER TABLE `group_info` DISABLE KEYS */;
INSERT INTO `group_info` (`ID`, `NAME`, `DESCRIPTION`, `CREATE_DATE`, `CREATE_BY`, `MODIFIED_DATE`, `MODIFIED_BY`) VALUES
	(1, '超级管理员', '超级管理员', '2017-06-23', '1', NULL, NULL),
	(2, '33', '33', '2018-07-23', '1', NULL, NULL);
/*!40000 ALTER TABLE `group_info` ENABLE KEYS */;

-- 正在导出表  showcase.oauth_client_details 的数据：~4 rows (大约)
/*!40000 ALTER TABLE `oauth_client_details` DISABLE KEYS */;
INSERT INTO `oauth_client_details` (`client_id`, `resource_ids`, `client_secret`, `scope`, `authorized_grant_types`, `web_server_redirect_uri`, `authorities`, `access_token_validity`, `refresh_token_validity`, `additional_information`, `autoapprove`) VALUES
	('client', 'authUser', '{bcrypt}$2a$10$Y8xXXVh39EA874mp3RU7hO1mQRwDvqorFTdCWTs2GEc3L/TBkoHmW', 'user_info', 'authorization_code,refresh_token', 'http://baidu.com', 'ROLE_ADMIN', 7200, 0, NULL, 'false'),
	('CouponSystem', 'authUser', '{bcrypt}$2a$10$Y8xXXVh39EA874mp3RU7hO1mQRwDvqorFTdCWTs2GEc3L/TBkoHmW', 'user_info', 'authorization_code,refresh_token,password', 'http://baidu.com', NULL, NULL, NULL, NULL, 'user_info'),
	('MemberSystem', 'authUser', '{bcrypt}$2a$10$Y8xXXVh39EA874mp3RU7hO1mQRwDvqorFTdCWTs2GEc3L/TBkoHmW', 'user_info', 'authorization_code,refresh_token', 'http://baidu.com', NULL, NULL, NULL, NULL, 'user_info'),
	('read-only-client', 'authUser', '{bcrypt}$2a$10$Y8xXXVh39EA874mp3RU7hO1mQRwDvqorFTdCWTs2GEc3L/TBkoHmW', 'user_info', 'authorization_code,refresh_token,password', 'http://baidu.com', NULL, 7200, 0, NULL, 'false');
/*!40000 ALTER TABLE `oauth_client_details` ENABLE KEYS */;

-- 正在导出表  showcase.resource_info 的数据：~12 rows (大约)
/*!40000 ALTER TABLE `resource_info` DISABLE KEYS */;
INSERT INTO `resource_info` (`ID`, `NAME`, `DESCRIPTION`, `URL`, `TYPE`, `STATUS`, `CODE`, `ORDER_INDEX`, `PARENT_ID`, `CREATE_DATE`, `IMG_NAME`, `CREATE_BY`, `MODIFIED_DATE`, `MODIFIED_BY`, `PARENT_NAME`) VALUES
	(1, '权限管理', '权限管理', '', '2', '1', '', 1, '', NULL, 'el-icon2-safetycertificate', NULL, '2015-11-17', 'sys', ''),
	(2, '基础数据', '基础数据', '', '2', '1', '', 2, '', '2015-06-16', 'el-icon2-database', NULL, NULL, NULL, NULL),
	(3, '基础参数', '基础参数', '/basic/baseInfoAction/goBaseInfo', '0', '1', '', 1, '2', NULL, 'el-icon2-project', NULL, '2016-03-04', 'sys', '基础数据'),
	(4, '用户管理', '用户管理', '/basic/userInfo/goUserInfo', '0', '1', '', 1, '1', NULL, 'el-icon2-user', NULL, '2016-03-04', 'sys', '权限管理'),
	(5, '资源管理', '资源管理', '/basic/resourceInfo/goResourceInfo', '0', '1', '', 4, '1', NULL, NULL, NULL, '2016-03-04', 'sys', '权限管理'),
	(6, '角色管理', '角色管理', '/basic/roleInfo/goRoleInfo', '0', '1', '', 3, '1', NULL, 'el-icon2-solution', '', '2018-07-04', '1', '权限管理'),
	(7, '组管理', '组管理', '/basic/groupInfo/goGroupInfo', '0', '1', '', 2, '1', NULL, 'el-icon2-team', NULL, '2016-03-04', 'sys', '权限管理'),
	(8, '按钮权限测试', '按钮权限测试', '', '1', '1', 'BUTTON_TEST', 1, '1', '2017-07-03', 'el-icon-menu', '1', NULL, NULL, NULL),
	(9, '定时任务', '定时任务', '/basic/scheduleJob/goScheduleJob', '0', '1', NULL, 5, '1', '2018-07-19', 'el-icon2-time-circle', '1', NULL, NULL, '权限管理'),
	(10, '系统图标', '系统图标', '/icons', '0', '1', '', 6, '1', NULL, 'el-icon2-orderedlist', '1', '2018-07-20', '1', '权限管理'),
	(11, '任务日志', '定时任务日志', '/basic/scheduleJobLog/goScheduleJobLog', '0', '1', NULL, 8, '1', '2018-07-20', 'el-icon2-message', '1', NULL, NULL, '权限管理'),
	(14, '文件管理', '文件管理', '/basic/uploadFile/goUploadFile', '0', '1', NULL, 2, '2', '2018-08-09', 'el-icon2-file-text', '1', NULL, NULL, '基础数据');
/*!40000 ALTER TABLE `resource_info` ENABLE KEYS */;

-- 正在导出表  showcase.role_group 的数据：~2 rows (大约)
/*!40000 ALTER TABLE `role_group` DISABLE KEYS */;
INSERT INTO `role_group` (`ROLE_ID`, `GROUP_ID`) VALUES
	('1', '1'),
	('1', '2');
/*!40000 ALTER TABLE `role_group` ENABLE KEYS */;

-- 正在导出表  showcase.role_info 的数据：~2 rows (大约)
/*!40000 ALTER TABLE `role_info` DISABLE KEYS */;
INSERT INTO `role_info` (`ID`, `NAME`, `DESCRIPTION`, `CREATE_DATE`, `CREATE_BY`, `MODIFIED_DATE`, `MODIFIED_BY`) VALUES
	(1, '超级管理员', '超级管理员', NULL, '', '2018-08-09', 'sys'),
	(2, '3', '3', '2018-07-23', 'sys', NULL, NULL);
/*!40000 ALTER TABLE `role_info` ENABLE KEYS */;

-- 正在导出表  showcase.role_resource 的数据：~12 rows (大约)
/*!40000 ALTER TABLE `role_resource` DISABLE KEYS */;
INSERT INTO `role_resource` (`ROLE_ID`, `RESOURCE_ID`) VALUES
	('1', '1'),
	('1', '10'),
	('1', '11'),
	('1', '14'),
	('1', '2'),
	('1', '3'),
	('1', '4'),
	('1', '5'),
	('1', '6'),
	('1', '7'),
	('1', '8'),
	('1', '9');
/*!40000 ALTER TABLE `role_resource` ENABLE KEYS */;

-- 正在导出表  showcase.user_group 的数据：~3 rows (大约)
/*!40000 ALTER TABLE `user_group` DISABLE KEYS */;
INSERT INTO `user_group` (`USER_ID`, `GROUP_ID`, `IS_ADMIN`) VALUES
	('1', '1', '1'),
	('1', '2', '1'),
	('2', '2', '0');
/*!40000 ALTER TABLE `user_group` ENABLE KEYS */;

-- 正在导出表  showcase.user_info 的数据：~2 rows (大约)
/*!40000 ALTER TABLE `user_info` DISABLE KEYS */;
INSERT INTO `user_info` (`ID`, `NAME`, `PASSWORD`, `NICK_NAME`, `EMAIL`, `PHONE`, `STATUS`, `LAST_LOGIN_IP`, `LAST_LOGIN_TIME`, `PASSWORD_EXPIRE_TIME`, `ACTIVE_FLAG`, `CREATE_BY`, `CREATE_BY_NAME`, `CREATE_DATE`, `MODIFIED_BY`, `MODIFIED_BY_NAME`, `MODIFIED_DATE`) VALUES
	(1, 'sys', '{bcrypt}$2a$10$Y8xXXVh39EA874mp3RU7hO1mQRwDvqorFTdCWTs2GEc3L/TBkoHmW', '系统管理员', 'sys@weizhong.cn', '13512345623', '1', '0:0:0:0:0:0:0:1', '2018-07-02 16:35:51', '2018-07-02 13:18:50', '1', '2015051900005', '', '2015-11-20 09:56:23', '1', 'sys', '2018-08-08 14:25:30'),
	(2, '1', '{bcrypt}$2a$10$hLENwrfaLnY5LbLaefZe3utGsAbQJOeLjaK9ZfBbZ8n737pBaZjiK', '1', '', '', '0', '', NULL, NULL, '1', '1', '1', '2018-07-23 12:04:37', '1', 'sys', '2018-08-08 14:29:41');
/*!40000 ALTER TABLE `user_info` ENABLE KEYS */;

-- 正在导出表  showcase.user_resource 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `user_resource` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_resource` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
