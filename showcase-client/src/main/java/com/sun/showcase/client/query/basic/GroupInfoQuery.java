package com.sun.showcase.client.query.basic;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.sun.showcase.client.domain.basic.GroupInfo;
import com.sun.showcase.pojo.SearchModel;
public class GroupInfoQuery extends SearchModel<GroupInfo> implements Serializable {
    
  private static final long serialVersionUID = 3148176768559230877L;
    

	  /**
     * id       db_column: ID 
     */	
	private java.lang.String id;
	  /**
     * 组名       db_column: NAME 
     */	
	private java.lang.String name;
	  /**
     * 组描述       db_column: DESCRIPTION 
     */	
	private java.lang.String description;
	  /**
     * 创建时间       db_column: CREATE_DATE 
     */	
	private java.util.Date createDate;
	  /**
     * 创建人ID       db_column: CREATE_BY 
     */	
	private java.lang.String createBy;
	  /**
     * 修改时间       db_column: MODIFIED_DATE 
     */	
	private java.util.Date modifiedDate;
	  /**
     * 修改人       db_column: MODIFIED_BY 
     */	
	private java.lang.String modifiedBy;
	
	private String userId;//用户ID
	
	private String ids[];

	public java.lang.String getId() {
		return this.id;
	}
	
	public void setId(java.lang.String value) {
		this.id = value;
	}
	
	public java.lang.String getName() {
		return this.name;
	}
	
	public void setName(java.lang.String value) {
		this.name = value;
	}
	
	public java.lang.String getDescription() {
		return this.description;
	}
	
	public void setDescription(java.lang.String value) {
		this.description = value;
	}
	
	public java.util.Date getCreateDate() {
		return this.createDate;
	}
	
	public void setCreateDate(java.util.Date value) {
		this.createDate = value;
	}
	
	public java.lang.String getCreateBy() {
		return this.createBy;
	}
	
	public void setCreateBy(java.lang.String value) {
		this.createBy = value;
	}
	
	public java.util.Date getModifiedDate() {
		return this.modifiedDate;
	}
	
	public void setModifiedDate(java.util.Date value) {
		this.modifiedDate = value;
	}
	
	public java.lang.String getModifiedBy() {
		return this.modifiedBy;
	}
	
	public void setModifiedBy(java.lang.String value) {
		this.modifiedBy = value;
	}
	
	public String[] getIds() {
		return ids;
	}

	public void setIds(String[] ids) {
		this.ids = ids;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.DEFAULT_STYLE);
	}
	
}

