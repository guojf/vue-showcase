package com.sun.showcase.client.query.basic;
import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.sun.showcase.client.domain.basic.BaseInfo;
import com.sun.showcase.pojo.SearchModel;
public class BaseInfoQuery extends SearchModel<BaseInfo> implements Serializable {
    
  private static final long serialVersionUID = 3148176768559230877L;
    

	  /**
     * 行号       db_column: ID 
     */	
	private java.lang.String id;
	  /**
     * 名称       db_column: ITEM_NAME 
     */	
	private java.lang.String itemName;
	  /**
     * 编码       db_column: ITEM_CODE 
     */	
	private java.lang.String itemCode;
	
	private String itemCodeCopy;//编码副本
	  /**
     * 父类名称       db_column: PARENT_NAME 
     */	
	private java.lang.String parentName;
	  /**
     * 父类编码       db_column: PARENT_CODE 
     */	
	private java.lang.String parentCode;
	  /**
     * 是否叶子节点1:是；0:否；       db_column: IS_LEAF 
     */	
	private java.lang.String isLeaf;
	  /**
     * 序号 序号小的在前       db_column: ORDER_NUM 
     */	
	private java.lang.Integer orderNum;
	/**
	 * 文件ID
	 */
	private Integer fileId;
	/**
	 * 文件名称
	 */
	private String fileName;
	/**
	 * 文件路径
	 */
	private String filePath;
	  /**
     * 备注       db_column: REMARK 
     */	
	private java.lang.String remark;
	  /**
     * 有效状态  0：无效  1：有效       db_column: ACTIVE_FLAG 
     */	
	private java.lang.String activeFlag;
	  /**
     * 创建时间       db_column: CREATE_DATE 
     */	
	private java.util.Date createDate;
	  /**
     * 创建人       db_column: CREATE_BY 
     */	
	private java.lang.String createBy;
	  /**
     * 修改时间       db_column: MODIFIED_DATE 
     */	
	private java.util.Date modifiedDate;
	  /**
     * 修改人       db_column: MODIFIED_BY 
     */	
	private java.lang.String modifiedBy;
	
	private String parentNode;//父节点
	
	private String isRoot;//是否根节点
	
	private String isWhole;//是否展示全部信息1:是
	private String dstAddress;//用户自提商铺地址APP

	public java.lang.String getId() {
		return this.id;
	}
	
	public void setId(java.lang.String value) {
		this.id = value;
	}
	
	public java.lang.String getItemName() {
		return this.itemName;
	}
	
	public void setItemName(java.lang.String value) {
		this.itemName = value;
	}
	
	public java.lang.String getItemCode() {
		return this.itemCode;
	}
	
	public void setItemCode(java.lang.String value) {
		this.itemCode = value;
	}
	
	public String getItemCodeCopy() {
		return itemCodeCopy;
	}

	public void setItemCodeCopy(String itemCodeCopy) {
		this.itemCodeCopy = itemCodeCopy;
	}

	public java.lang.String getParentName() {
		return this.parentName;
	}
	
	public void setParentName(java.lang.String value) {
		this.parentName = value;
	}
	
	public java.lang.String getParentCode() {
		return this.parentCode;
	}
	
	public void setParentCode(java.lang.String value) {
		this.parentCode = value;
	}
	
	public java.lang.String getIsLeaf() {
		return this.isLeaf;
	}
	
	public void setIsLeaf(java.lang.String value) {
		this.isLeaf = value;
	}
	
	public java.lang.Integer getOrderNum() {
		return this.orderNum;
	}
	
	public void setOrderNum(java.lang.Integer value) {
		this.orderNum = value;
	}
	
	public Integer getFileId() {
		return fileId;
	}

	public void setFileId(Integer fileId) {
		this.fileId = fileId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public java.lang.String getRemark() {
		return this.remark;
	}
	
	public void setRemark(java.lang.String value) {
		this.remark = value;
	}
	
	public java.lang.String getActiveFlag() {
		return this.activeFlag;
	}
	
	public void setActiveFlag(java.lang.String value) {
		this.activeFlag = value;
	}
	
	public java.util.Date getCreateDate() {
		return this.createDate;
	}
	
	public void setCreateDate(java.util.Date value) {
		this.createDate = value;
	}
	
	public java.lang.String getCreateBy() {
		return this.createBy;
	}
	
	public void setCreateBy(java.lang.String value) {
		this.createBy = value;
	}
	
	public java.util.Date getModifiedDate() {
		return this.modifiedDate;
	}
	
	public void setModifiedDate(java.util.Date value) {
		this.modifiedDate = value;
	}
	
	public java.lang.String getModifiedBy() {
		return this.modifiedBy;
	}
	
	public void setModifiedBy(java.lang.String value) {
		this.modifiedBy = value;
	}
	
	public String getParentNode() {
		return parentNode;
	}

	public void setParentNode(String parentNode) {
		this.parentNode = parentNode;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.DEFAULT_STYLE);
	}

	public String getIsRoot() {
		return isRoot;
	}

	public void setIsRoot(String isRoot) {
		this.isRoot = isRoot;
	}

	public String getIsWhole() {
		return isWhole;
	}

	public void setIsWhole(String isWhole) {
		this.isWhole = isWhole;
	}

	public String getDstAddress() {
		return dstAddress;
	}

	public void setDstAddress(String dstAddress) {
		this.dstAddress = dstAddress;
	}
	
}

