package com.sun.showcase.client.query.basic;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.sun.showcase.client.domain.basic.UploadFile;
import com.sun.showcase.pojo.SearchModel;

public class UploadFileQuery extends SearchModel<UploadFile> implements Serializable {
    
  private static final long serialVersionUID = 3148176768559230877L;
    

	  /**
     * id       db_column: ID 
     */	
	private java.lang.Long id;
	  /**
     * 文件名称       db_column: FILE_NAME 
     */	
	private java.lang.String fileName;
	  /**
     * 存取名称       db_column: SAVE_FILE_NAME 
     */	
	private java.lang.String saveFileName;
	  /**
     * 存取路径        db_column: FILE_PATH 
     */	
	private java.lang.String filePath;
	  /**
     * 1.系统文件;0.远程文件       db_column: type 
     */	
	private java.lang.Integer type;
	  /**
     * 文件描述         db_column: REMARKS 
     */	
	private java.lang.String remarks;
	  /**
     * 1=有效，0=无效       db_column: ACTIVE_FLAG 
     */	
	private java.lang.String activeFlag;
	  /**
     * 创建人Id        db_column: CREATED_BY 
     */	
	private java.lang.String createdBy;
	  /**
     * 创建日期       db_column: CREATED 
     */	
	private java.util.Date created;
	  /**
     * 修改人Id       db_column: LAST_UPD_BY 
     */	
	private java.lang.String lastUpdBy;
	  /**
     * 修改日期         db_column: LAST_UPD 
     */	
	private java.util.Date lastUpd;
	/**
	 * 封装修改时的where条件  key为数据库字段值  value为条件值
	 * */
	private Map<String,Object> searchMap = new HashMap<String,Object>();
	/**
	 * 数据表主键id的数组
	 * */
	private java.lang.Long ids[];

	public java.lang.Long getId() {
		return this.id;
	}
	
	public void setId(java.lang.Long value) {
		this.id = value;
	}
	
	public java.lang.String getFileName() {
		return this.fileName;
	}
	
	public void setFileName(java.lang.String value) {
		this.fileName = value;
	}
	
	public java.lang.String getSaveFileName() {
		return this.saveFileName;
	}
	
	public void setSaveFileName(java.lang.String value) {
		this.saveFileName = value;
	}
	
	public java.lang.String getFilePath() {
		return this.filePath;
	}
	
	public void setFilePath(java.lang.String value) {
		this.filePath = value;
	}
	
	public java.lang.Integer getType() {
		return this.type;
	}
	
	public void setType(java.lang.Integer value) {
		this.type = value;
	}
	
	public java.lang.String getRemarks() {
		return this.remarks;
	}
	
	public void setRemarks(java.lang.String value) {
		this.remarks = value;
	}
	
	public java.lang.String getActiveFlag() {
		return this.activeFlag;
	}
	
	public void setActiveFlag(java.lang.String value) {
		this.activeFlag = value;
	}
	
	public java.lang.String getCreatedBy() {
		return this.createdBy;
	}
	
	public void setCreatedBy(java.lang.String value) {
		this.createdBy = value;
	}
	
	public java.util.Date getCreated() {
		return this.created;
	}
	
	public void setCreated(java.util.Date value) {
		this.created = value;
	}
	
	public java.lang.String getLastUpdBy() {
		return this.lastUpdBy;
	}
	
	public void setLastUpdBy(java.lang.String value) {
		this.lastUpdBy = value;
	}
	
	public java.util.Date getLastUpd() {
		return this.lastUpd;
	}
	
	public void setLastUpd(java.util.Date value) {
		this.lastUpd = value;
	}
	
	public Map<String, Object> getSearchMap() {
		return searchMap;
	}
	
	public void setSearchMap(Map<String, Object> searchMap) {
		this.searchMap = searchMap;
	}
	
	public java.lang.Long[] getIds() {
		return ids;
	}
	
	public void setIds(java.lang.Long[] ids) {
		this.ids = ids;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.DEFAULT_STYLE);
	}
	
}

