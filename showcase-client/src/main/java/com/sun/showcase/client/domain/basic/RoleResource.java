package com.sun.showcase.client.domain.basic;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class RoleResource implements java.io.Serializable{
	private static final long serialVersionUID = 5454155825314635342L;
	
	//alias
	public static final String TABLE_ALIAS = "RoleResource";
    /**
     * 角色表ID       db_column: ROLE_ID 
     */	
	private java.lang.String roleId;
    /**
     * 资源表ID       db_column: RESOURCE_ID 
     */	
	private java.lang.String resourceId;
	//columns END

	public RoleResource(){
	}

	public RoleResource(
		java.lang.String roleId,
		java.lang.String resourceId
	){
		this.roleId = roleId;
		this.resourceId = resourceId;
	}

	public void setRoleId(java.lang.String value) {
		this.roleId = value;
	}
	
	public java.lang.String getRoleId() {
		return this.roleId;
	}
	public void setResourceId(java.lang.String value) {
		this.resourceId = value;
	}
	
	public java.lang.String getResourceId() {
		return this.resourceId;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
	public int hashCode() {
		return new HashCodeBuilder()
			.append(getRoleId())
			.append(getResourceId())
			.toHashCode();
	}
	
	public boolean equals(Object obj) {
		if(obj instanceof RoleResource == false) return false;
		if(this == obj) return true;
		RoleResource other = (RoleResource)obj;
		return new EqualsBuilder()
			.append(getRoleId(),other.getRoleId())
			.append(getResourceId(),other.getResourceId())
			.isEquals();
	}
}

