package com.sun.showcase.client.domain.basic;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.sun.showcase.utils.DateUtils;

public class ResourceInfo implements java.io.Serializable{
	private static final long serialVersionUID = 5454155825314635342L;
	
	public static final String TABLE_ALIAS = "ResourceInfo";
    /**
     * 菜单ID       db_column: ID 
     */	
	private java.lang.String id;
    /**
     * 资源名称       db_column: NAME 
     */	
	private java.lang.String name;
    /**
     * 资源描述       db_column: DESCRIPTION 
     */	
	private java.lang.String description;
    /**
     * 资源地址       db_column: URL 
     */	
	private java.lang.String url;
    /**
     * 资源类型       db_column: TYPE 
     */	
	private java.lang.String type;
    /**
     * 资源状态 0:无效  1:有效       db_column: STATUS 
     */	
	private java.lang.String status;
    /**
     * 资源编码       db_column: CODE 
     */	
	private java.lang.String code;
    /**
     * 排序号（小的在前)       db_column: ORDER_INDEX 
     */	
	private java.lang.Integer orderIndex;
    /**
     * 父资源ID       db_column: PARENT_ID 
     */	
	private java.lang.String parentId;
	/**
     * 图标名称       db_column: IMG_NAME 
     */	
	private String imgName;
    /**
     * 创建时间       db_column: CREATE_DATE 
     */	
	private java.util.Date createDate;
    /**
     * 创建人       db_column: CREATE_BY 
     */	
	private java.lang.String createBy;
    /**
     * 修改时间       db_column: MODIFIED_DATE 
     */	
	private java.util.Date modifiedDate;
    /**
     * 修改人       db_column: MODIFIED_BY 
     */	
	private java.lang.String modifiedBy;
	/**
	 * 所在模块名称 db_column:PARENT_NAME;
	 */
	private java.lang.String parentName;
	
	private String groupName;//组名称
	//columns END

	public ResourceInfo(){
	}

	public java.lang.String getParentName() {
		return parentName;
	}

	public void setParentName(java.lang.String parentName) {
		this.parentName = parentName;
	}

	public ResourceInfo(
		java.lang.String id
	){
		this.id = id;
	}

	public void setId(java.lang.String value) {
		this.id = value;
	}
	
	public java.lang.String getId() {
		return this.id;
	}
	public void setName(java.lang.String value) {
		this.name = value;
	}
	
	public java.lang.String getName() {
		return this.name;
	}
	public void setDescription(java.lang.String value) {
		this.description = value;
	}
	
	public java.lang.String getDescription() {
		return this.description;
	}
	public void setUrl(java.lang.String value) {
		this.url = value;
	}
	
	public java.lang.String getUrl() {
		return this.url;
	}
	public void setType(java.lang.String value) {
		this.type = value;
	}
	
	public java.lang.String getType() {
		return this.type;
	}
	public void setStatus(java.lang.String value) {
		this.status = value;
	}
	
	public java.lang.String getStatus() {
		return this.status;
	}
	public void setCode(java.lang.String value) {
		this.code = value;
	}
	
	public java.lang.String getCode() {
		return this.code;
	}
	public void setOrderIndex(java.lang.Integer value) {
		this.orderIndex = value;
	}
	
	public java.lang.Integer getOrderIndex() {
		return this.orderIndex;
	}
	public void setParentId(java.lang.String value) {
		this.parentId = value;
	}
	
	public java.lang.String getParentId() {
		return this.parentId;
	}
	public String getImgName() {
		return imgName;
	}
	public void setImgName(String imgName) {
		this.imgName = imgName;
	}

	public String getCreateDateString() {
		//return DateConvertUtils.format(getCreateDate(), FORMAT_CREATE_DATE);
		return  DateUtils.format(DateUtils.format2,getCreateDate());
	}
	public void setCreateDateString(String value) {
		setCreateDate(DateUtils.parse(value,DateUtils.format2,java.util.Date.class));
	}
	
	public void setCreateDate(java.util.Date value) {
		this.createDate = value;
	}
	
	public java.util.Date getCreateDate() {
		return this.createDate;
	}
	public void setCreateBy(java.lang.String value) {
		this.createBy = value;
	}
	
	public java.lang.String getCreateBy() {
		return this.createBy;
	}
	public String getModifiedDateString() {
		//return DateConvertUtils.format(getModifiedDate(), FORMAT_MODIFIED_DATE);
		return  DateUtils.format(DateUtils.format2,getModifiedDate());
	}
	public void setModifiedDateString(String value) {
		setModifiedDate(DateUtils.parse(value,DateUtils.format2,java.util.Date.class));
	}
	
	public void setModifiedDate(java.util.Date value) {
		this.modifiedDate = value;
	}
	
	public java.util.Date getModifiedDate() {
		return this.modifiedDate;
	}
	public void setModifiedBy(java.lang.String value) {
		this.modifiedBy = value;
	}
	
	public java.lang.String getModifiedBy() {
		return this.modifiedBy;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
	public int hashCode() {
		return new HashCodeBuilder()
			.append(getId())
			.toHashCode();
	}
	
	public boolean equals(Object obj) {
		if(obj instanceof ResourceInfo == false) return false;
		if(this == obj) return true;
		ResourceInfo other = (ResourceInfo)obj;
		return new EqualsBuilder()
			.append(getId(),other.getId())
			.isEquals();
	}
}

