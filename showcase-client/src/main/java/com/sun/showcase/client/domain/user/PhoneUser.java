package com.sun.showcase.client.domain.user;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class PhoneUser implements java.io.Serializable{
	private static final long serialVersionUID = 5454155825314635342L;
    /**
     * ID       db_column: id 
     */	
	private java.lang.Long id;
    /**
     * 用户名       db_column: user_name 
     */	
	private java.lang.String userName;
    /**
     * 密码       db_column: password 
     */	
	private java.lang.String password;
    /**
     * 手机号       db_column: user_phone 
     */	
	private java.lang.String userPhone;
    /**
     * 有效标记(默认为1有效 0无效）       db_column: active_flag 
     */	
	private java.lang.String activeFlag;
    /**
     * 创建人ID       db_column: create_by 
     */	
	private java.lang.String createBy;
    /**
     * 创建人姓名       db_column: create_by_name 
     */	
	private java.lang.String createByName;
    /**
     * 创建时间       db_column: create_date 
     */	
	private java.util.Date createDate;
    /**
     * 修改人ID       db_column: modified_by 
     */	
	private java.lang.String modifiedBy;
    /**
     * 修改人姓名       db_column: modified_by_name 
     */	
	private java.lang.String modifiedByName;
    /**
     * 修改时间       db_column: modified_date 
     */	
	private java.util.Date modifiedDate;
	//columns END

	public PhoneUser(){
	}

	public PhoneUser(
		java.lang.Long id
	){
		this.id = id;
	}

	public void setId(java.lang.Long value) {
		this.id = value;
	}
	
	public java.lang.Long getId() {
		return this.id;
	}
	public void setUserName(java.lang.String value) {
		this.userName = value;
	}
	
	public java.lang.String getUserName() {
		return this.userName;
	}
	public void setPassword(java.lang.String value) {
		this.password = value;
	}
	
	public java.lang.String getPassword() {
		return this.password;
	}
	public void setUserPhone(java.lang.String value) {
		this.userPhone = value;
	}
	
	public java.lang.String getUserPhone() {
		return this.userPhone;
	}
	public void setActiveFlag(java.lang.String value) {
		this.activeFlag = value;
	}
	
	public java.lang.String getActiveFlag() {
		return this.activeFlag;
	}
	public void setCreateBy(java.lang.String value) {
		this.createBy = value;
	}
	
	public java.lang.String getCreateBy() {
		return this.createBy;
	}
	public void setCreateByName(java.lang.String value) {
		this.createByName = value;
	}
	
	public java.lang.String getCreateByName() {
		return this.createByName;
	}
	public void setCreateDate(java.util.Date value) {
		this.createDate = value;
	}
	
	public java.util.Date getCreateDate() {
		return this.createDate;
	}
	public void setModifiedBy(java.lang.String value) {
		this.modifiedBy = value;
	}
	
	public java.lang.String getModifiedBy() {
		return this.modifiedBy;
	}
	public void setModifiedByName(java.lang.String value) {
		this.modifiedByName = value;
	}
	
	public java.lang.String getModifiedByName() {
		return this.modifiedByName;
	}
	public void setModifiedDate(java.util.Date value) {
		this.modifiedDate = value;
	}
	
	public java.util.Date getModifiedDate() {
		return this.modifiedDate;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
	public int hashCode() {
		return new HashCodeBuilder()
			.append(getId())
			.toHashCode();
	}
	
	public boolean equals(Object obj) {
		if(obj instanceof PhoneUser == false) return false;
		if(this == obj) return true;
		PhoneUser other = (PhoneUser)obj;
		return new EqualsBuilder()
			.append(getId(),other.getId())
			.isEquals();
	}
}

