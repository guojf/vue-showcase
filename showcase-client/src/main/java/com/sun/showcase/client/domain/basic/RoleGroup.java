package com.sun.showcase.client.domain.basic;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
public class RoleGroup implements java.io.Serializable{
	private static final long serialVersionUID = 5454155825314635342L;
	
	public static final String TABLE_ALIAS = "RoleGroup";
	
    /**
     * 角色ID       db_column: ROLE_ID 
     */	
	private java.lang.String roleId;
    /**
     * 组ID       db_column: GROUP_ID 
     */	
	private java.lang.String groupId;
	
	private String name;
	
	private String description;
	
	//columns END

	public RoleGroup(){
	}

	public RoleGroup(
		java.lang.String roleId,
		java.lang.String groupId
	){
		this.roleId = roleId;
		this.groupId = groupId;
	}

	public void setRoleId(java.lang.String value) {
		this.roleId = value;
	}
	
	public java.lang.String getRoleId() {
		return this.roleId;
	}
	public void setGroupId(java.lang.String value) {
		this.groupId = value;
	}
	
	public java.lang.String getGroupId() {
		return this.groupId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
	public int hashCode() {
		return new HashCodeBuilder()
			.append(getRoleId())
			.append(getGroupId())
			.toHashCode();
	}
	
	public boolean equals(Object obj) {
		if(obj instanceof RoleGroup == false) return false;
		if(this == obj) return true;
		RoleGroup other = (RoleGroup)obj;
		return new EqualsBuilder()
			.append(getRoleId(),other.getRoleId())
			.append(getGroupId(),other.getGroupId())
			.isEquals();
	}
}

