package com.sun.showcase.client.domain.basic;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class UserGroup  implements java.io.Serializable{
	private static final long serialVersionUID = 5454155825314635342L;
	
	//alias
	public static final String TABLE_ALIAS = "UserGroup";
	
    /**
     * 用户ID       db_column: USER_ID 
     */	
	private java.lang.String userId;
    /**
     * 组ID       db_column: GROUP_ID 
     */	
	private java.lang.String groupId;
    /**
     * 是否为管理员 0：不是 1：是       db_column: IS_ADMIN 
     */	
	private java.lang.String isAdmin;
	
	private String name;
	
	private String status;
	
	private String nickName;
	
	private String email;
	
	
	//columns END

	public UserGroup(){
	}

	public UserGroup(
		java.lang.String userId,
		java.lang.String groupId
	){
		this.userId = userId;
		this.groupId = groupId;
	}

	public void setUserId(java.lang.String value) {
		this.userId = value;
	}
	
	public java.lang.String getUserId() {
		return this.userId;
	}
	public void setGroupId(java.lang.String value) {
		this.groupId = value;
	}
	
	public java.lang.String getGroupId() {
		return this.groupId;
	}
	public void setIsAdmin(java.lang.String value) {
		this.isAdmin = value;
	}
	
	public java.lang.String getIsAdmin() {
		return this.isAdmin;
	}

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
	public int hashCode() {
		return new HashCodeBuilder()
			.append(getUserId())
			.append(getGroupId())
			.toHashCode();
	}
	
	public boolean equals(Object obj) {
		if(obj instanceof UserGroup == false) return false;
		if(this == obj) return true;
		UserGroup other = (UserGroup)obj;
		return new EqualsBuilder()
			.append(getUserId(),other.getUserId())
			.append(getGroupId(),other.getGroupId())
			.isEquals();
	}
}

