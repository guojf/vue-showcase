package com.sun.showcase.client.domain.basic;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.sun.showcase.utils.DateUtils;

public class GroupInfo implements java.io.Serializable{
	private static final long serialVersionUID = 5454155825314635342L;
	
	public static final String TABLE_ALIAS = "GroupInfo";
	
    /**
     * id       db_column: ID 
     */	
	private java.lang.String id;
    /**
     * 组名       db_column: NAME 
     */	
	private java.lang.String name;
    /**
     * 组描述       db_column: DESCRIPTION 
     */	
	private java.lang.String description;
    /**
     * 创建时间       db_column: CREATE_DATE 
     */	
	private java.util.Date createDate;
    /**
     * 创建人ID       db_column: CREATE_BY 
     */	
	private java.lang.String createBy;
    /**
     * 修改时间       db_column: MODIFIED_DATE 
     */	
	private java.util.Date modifiedDate;
    /**
     * 修改人       db_column: MODIFIED_BY 
     */	
	private java.lang.String modifiedBy;
	
	private String checked;
	//columns END

	public GroupInfo(){
	}

	public GroupInfo(
		java.lang.String id
	){
		this.id = id;
	}

	public void setId(java.lang.String value) {
		this.id = value;
	}
	
	public java.lang.String getId() {
		return this.id;
	}
	public void setName(java.lang.String value) {
		this.name = value;
	}
	
	public java.lang.String getName() {
		return this.name;
	}
	public void setDescription(java.lang.String value) {
		this.description = value;
	}
	
	public java.lang.String getDescription() {
		return this.description;
	}
	public String getCreateDateString() {
		//return DateConvertUtils.format(getCreateDate(), FORMAT_CREATE_DATE);
		return  DateUtils.format(DateUtils.format2,getCreateDate());
	}
	public void setCreateDateString(String value) {
		setCreateDate(DateUtils.parse(value,DateUtils.format2,java.util.Date.class));
	}
	
	public void setCreateDate(java.util.Date value) {
		this.createDate = value;
	}
	
	public java.util.Date getCreateDate() {
		return this.createDate;
	}
	public void setCreateBy(java.lang.String value) {
		this.createBy = value;
	}
	
	public java.lang.String getCreateBy() {
		return this.createBy;
	}
	public String getModifiedDateString() {
		//return DateConvertUtils.format(getModifiedDate(), FORMAT_MODIFIED_DATE);
		return  DateUtils.format(DateUtils.format2,getModifiedDate());
	}
	public void setModifiedDateString(String value) {
		setModifiedDate(DateUtils.parse(value,DateUtils.format2,java.util.Date.class));
	}
	
	public void setModifiedDate(java.util.Date value) {
		this.modifiedDate = value;
	}
	
	public java.util.Date getModifiedDate() {
		return this.modifiedDate;
	}
	public void setModifiedBy(java.lang.String value) {
		this.modifiedBy = value;
	}
	
	public java.lang.String getModifiedBy() {
		return this.modifiedBy;
	}

	public String getChecked() {
		return checked;
	}

	public void setChecked(String checked) {
		this.checked = checked;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
	public int hashCode() {
		return new HashCodeBuilder()
			.append(getId())
			.toHashCode();
	}
	
	public boolean equals(Object obj) {
		if(obj instanceof GroupInfo == false) return false;
		if(this == obj) return true;
		GroupInfo other = (GroupInfo)obj;
		return new EqualsBuilder()
			.append(getId(),other.getId())
			.isEquals();
	}
}

