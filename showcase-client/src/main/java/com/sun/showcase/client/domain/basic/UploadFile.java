package com.sun.showcase.client.domain.basic;

public class UploadFile implements java.io.Serializable{

	private static final long serialVersionUID = 3054895238305105445L;
	
	/**
     * 行号       db_column: ID 
     */
	private Long id;
	/**
     * 文件名称       db_column: FILE_NAME 
     */
	private java.lang.String fileName;
	/**
     * 存取名称       db_column: SAVE_FILE_NAME 
     */
	private java.lang.String saveFileName;
	/**
     * 存取路径       db_column: FILE_PATH 
     */
	private java.lang.String filePath;
	/**
     * 1.系统文件;0.远程文件      db_column: TYPE
     */
	private java.lang.Integer type;
	/**
     * 文件描述       db_column: REMARKS 
     */
	private java.lang.String remarks;
	/**
     * 1=有效，0=无效       db_column: ACTIVE_FLAG 
     */	
	private java.lang.String activeFlag;
    /**
     * 创建人Id       db_column: CREATED_BY 
     */	
	private java.lang.String createdBy;
    /**
     * 创建日期       db_column: CREATED 
     */	
	private java.util.Date created;
    /**
     * 修改人Id       db_column: LAST_UPD_BY 
     */	
	private java.lang.String lastUpdBy;
    /**
     * 修改日期       db_column: LAST_UPD 
     */	
	private java.util.Date lastUpd;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public java.lang.String getFileName() {
		return fileName;
	}
	
	public void setFileName(java.lang.String fileName) {
		this.fileName = fileName;
	}
	
	public java.lang.String getSaveFileName() {
		return saveFileName;
	}
	
	public void setSaveFileName(java.lang.String saveFileName) {
		this.saveFileName = saveFileName;
	}
	
	public java.lang.String getFilePath() {
		return filePath;
	}
	
	public void setFilePath(java.lang.String filePath) {
		this.filePath = filePath;
	}
	
	public java.lang.Integer getType() {
		return type;
	}
	
	public void setType(java.lang.Integer type) {
		this.type = type;
	}
	
	public java.lang.String getRemarks() {
		return remarks;
	}
	
	public void setRemarks(java.lang.String remarks) {
		this.remarks = remarks;
	}
	
	public java.lang.String getActiveFlag() {
		return activeFlag;
	}
	
	public void setActiveFlag(java.lang.String activeFlag) {
		this.activeFlag = activeFlag;
	}
	
	public java.lang.String getCreatedBy() {
		return createdBy;
	}
	
	public void setCreatedBy(java.lang.String createdBy) {
		this.createdBy = createdBy;
	}
	
	public java.util.Date getCreated() {
		return created;
	}
	
	public void setCreated(java.util.Date created) {
		this.created = created;
	}
	
	public java.lang.String getLastUpdBy() {
		return lastUpdBy;
	}
	
	public void setLastUpdBy(java.lang.String lastUpdBy) {
		this.lastUpdBy = lastUpdBy;
	}
	
	public java.util.Date getLastUpd() {
		return lastUpd;
	}
	
	public void setLastUpd(java.util.Date lastUpd) {
		this.lastUpd = lastUpd;
	}



}
