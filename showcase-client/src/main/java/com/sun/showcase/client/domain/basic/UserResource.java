package com.sun.showcase.client.domain.basic;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
public class UserResource  implements java.io.Serializable{
	private static final long serialVersionUID = 5454155825314635342L;
	
	public static final String TABLE_ALIAS = "UserResource";
    /**
     * 用户ID       db_column: user_id 
     */	
	private java.lang.String userId;
    /**
     * 资源ID       db_column: resource_id 
     */	
	private java.lang.String resourceId;
	//columns END

	public UserResource(){
	}

	public UserResource(
		java.lang.String userId,
		java.lang.String resourceId
	){
		this.userId = userId;
		this.resourceId = resourceId;
	}

	public void setUserId(java.lang.String value) {
		this.userId = value;
	}
	
	public java.lang.String getUserId() {
		return this.userId;
	}
	public void setResourceId(java.lang.String value) {
		this.resourceId = value;
	}
	
	public java.lang.String getResourceId() {
		return this.resourceId;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
	public int hashCode() {
		return new HashCodeBuilder()
			.append(getUserId())
			.append(getResourceId())
			.toHashCode();
	}
	
	public boolean equals(Object obj) {
		if(obj instanceof UserResource == false) return false;
		if(this == obj) return true;
		UserResource other = (UserResource)obj;
		return new EqualsBuilder()
			.append(getUserId(),other.getUserId())
			.append(getResourceId(),other.getResourceId())
			.isEquals();
	}
}

