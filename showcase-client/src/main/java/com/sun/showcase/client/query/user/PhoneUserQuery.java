package com.sun.showcase.client.query.user;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.validation.constraints.NotBlank;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.validator.constraints.Length;

import com.sun.showcase.client.domain.user.PhoneUser;
import com.sun.showcase.client.validator.groups.RegistValidatorGroup;
import com.sun.showcase.pojo.SearchModel;

public class PhoneUserQuery extends SearchModel<PhoneUser> implements Serializable {
    
  private static final long serialVersionUID = 3148176768559230877L;
    

	  /**
     * ID       db_column: id 
     */	
	private java.lang.Long id;
	  /**
     * 用户名       db_column: user_name 
     */	
	@NotBlank(message="用户名不能为空",groups={RegistValidatorGroup.class})
	private java.lang.String userName;
	  /**
     * 密码       db_column: password 
     */	
    @NotBlank(message="密码不能为空")
    @Length(max = 12, min = 6, message = "请输入6~12位密码")  
	private java.lang.String password;
	  /**
     * 手机号       db_column: user_phone 
     */	
    @NotBlank(message="手机号不能为空")
    @Length(max = 11, min = 11, message = "手机号格式不对")  
	private java.lang.String userPhone;
	  /**
     * 有效标记(默认为1有效 0无效）       db_column: active_flag 
     */	
	private java.lang.String activeFlag;
	  /**
     * 创建人ID       db_column: create_by 
     */	
	private java.lang.String createBy;
	  /**
     * 创建人姓名       db_column: create_by_name 
     */	
	private java.lang.String createByName;
	  /**
     * 创建时间       db_column: create_date 
     */	
	private java.util.Date createDate;
	  /**
     * 修改人ID       db_column: modified_by 
     */	
	private java.lang.String modifiedBy;
	  /**
     * 修改人姓名       db_column: modified_by_name 
     */	
	private java.lang.String modifiedByName;
	  /**
     * 修改时间       db_column: modified_date 
     */	
	private java.util.Date modifiedDate;
	/**
	 * 封装修改时的where条件  key为数据库字段值  value为条件值
	 * */
	private Map<String,Object> searchMap = new HashMap<String,Object>();
	/**
	 * 数据表主键id的数组
	 * */
	private java.lang.Long ids[];

	public java.lang.Long getId() {
		return this.id;
	}
	
	public void setId(java.lang.Long value) {
		this.id = value;
	}
	
	public java.lang.String getUserName() {
		return this.userName;
	}
	
	public void setUserName(java.lang.String value) {
		this.userName = value;
	}
	
	public java.lang.String getPassword() {
		return this.password;
	}
	
	public void setPassword(java.lang.String value) {
		this.password = value;
	}
	
	public java.lang.String getUserPhone() {
		return this.userPhone;
	}
	
	public void setUserPhone(java.lang.String value) {
		this.userPhone = value;
	}
	
	public java.lang.String getActiveFlag() {
		return this.activeFlag;
	}
	
	public void setActiveFlag(java.lang.String value) {
		this.activeFlag = value;
	}
	
	public java.lang.String getCreateBy() {
		return this.createBy;
	}
	
	public void setCreateBy(java.lang.String value) {
		this.createBy = value;
	}
	
	public java.lang.String getCreateByName() {
		return this.createByName;
	}
	
	public void setCreateByName(java.lang.String value) {
		this.createByName = value;
	}
	
	public java.util.Date getCreateDate() {
		return this.createDate;
	}
	
	public void setCreateDate(java.util.Date value) {
		this.createDate = value;
	}
	
	public java.lang.String getModifiedBy() {
		return this.modifiedBy;
	}
	
	public void setModifiedBy(java.lang.String value) {
		this.modifiedBy = value;
	}
	
	public java.lang.String getModifiedByName() {
		return this.modifiedByName;
	}
	
	public void setModifiedByName(java.lang.String value) {
		this.modifiedByName = value;
	}
	
	public java.util.Date getModifiedDate() {
		return this.modifiedDate;
	}
	
	public void setModifiedDate(java.util.Date value) {
		this.modifiedDate = value;
	}
	
	public Map<String, Object> getSearchMap() {
		return searchMap;
	}
	
	public void setSearchMap(Map<String, Object> searchMap) {
		this.searchMap = searchMap;
	}
	
	public java.lang.Long[] getIds() {
		return ids;
	}
	
	public void setIds(java.lang.Long[] ids) {
		this.ids = ids;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.DEFAULT_STYLE);
	}
	
}

