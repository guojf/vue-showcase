package com.sun.showcase.client.query.basic;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.sun.showcase.client.domain.basic.UserInfo;
import com.sun.showcase.pojo.SearchModel;

public class UserInfoQuery extends SearchModel<UserInfo> implements Serializable {
    
  private static final long serialVersionUID = 3148176768559230877L;
    

	  /**
     * ID主键       db_column: ID 
     */	
	private java.lang.String id;
	  /**
     * 账号       db_column: NAME 
     */	
	private java.lang.String name;
	  /**
     * 密码       db_column: PASSWORD 
     */	
	private java.lang.String password;
	  /**
     * 昵称       db_column: NICK_NAME 
     */	
	private java.lang.String nickName;
	  /**
     * 邮箱       db_column: EMAIL 
     */	
	private java.lang.String email;
	  /**
     * 电话       db_column: PHONE 
     */	
	private java.lang.String phone;
	  /**
     * 账户状态1:启用;0:禁用;       db_column: STATUS 
     */	
	private java.lang.String status;
	  /**
     * 最后一次登录IP       db_column: LAST_LOGIN_IP 
     */	
	private java.lang.String lastLoginIp;
	  /**
     * 最后一次登录时间       db_column: LAST_LOGIN_TIME 
     */	
	private java.util.Date lastLoginTime;
	  /**
     * 密码过期时间       db_column: PASSWORD_EXPIRE_TIME 
     */	
	private java.util.Date passwordExpireTime;
	  /**
     * 有效状态（1：有效 0：无效）       db_column: ACTIVE_FLAG 
     */	
	private java.lang.String activeFlag;
	  /**
     * 创建人ID       db_column: CREATE_BY 
     */	
	private java.lang.String createBy;
	  /**
     * 创建人姓名       db_column: CREATE_BY_NAME 
     */	
	private java.lang.String createByName;
	  /**
     * 创建时间       db_column: CREATE_DATE 
     */	
	private java.util.Date createDate;
	  /**
     * 修改人ID       db_column: MODIFIED_BY 
     */	
	private java.lang.String modifiedBy;
	  /**
     * 修改人姓名       db_column: MODIFIED_BY_NAME 
     */	
	private java.lang.String modifiedByName;
	  /**
     * 修改时间       db_column: MODIFIED_DATE 
     */	
	private java.util.Date modifiedDate;
	/**
	 * 封装修改时的where条件  key为数据库字段值  value为条件值
	 * */
	private Map<String,Object> searchMap = new HashMap<String,Object>();
	/**
	 * 数据表主键id的数组
	 * */
	private java.lang.Long ids[];
	
	private String resourceIds;//资源集合
	
	private String groupInfoIds;//组集合
	
	public String getResourceIds() {
		return resourceIds;
	}

	public void setResourceIds(String resourceIds) {
		this.resourceIds = resourceIds;
	}

	public String getGroupInfoIds() {
		return groupInfoIds;
	}

	public void setGroupInfoIds(String groupInfoIds) {
		this.groupInfoIds = groupInfoIds;
	}

	public java.lang.String getId() {
		return this.id;
	}
	
	public void setId(java.lang.String value) {
		this.id = value;
	}
	
	public java.lang.String getName() {
		return this.name;
	}
	
	public void setName(java.lang.String value) {
		this.name = value;
	}
	
	public java.lang.String getPassword() {
		return this.password;
	}
	
	public void setPassword(java.lang.String value) {
		this.password = value;
	}
	
	public java.lang.String getNickName() {
		return this.nickName;
	}
	
	public void setNickName(java.lang.String value) {
		this.nickName = value;
	}
	
	public java.lang.String getEmail() {
		return this.email;
	}
	
	public void setEmail(java.lang.String value) {
		this.email = value;
	}
	
	public java.lang.String getPhone() {
		return this.phone;
	}
	
	public void setPhone(java.lang.String value) {
		this.phone = value;
	}
	
	public java.lang.String getStatus() {
		return this.status;
	}
	
	public void setStatus(java.lang.String value) {
		this.status = value;
	}
	
	public java.lang.String getLastLoginIp() {
		return this.lastLoginIp;
	}
	
	public void setLastLoginIp(java.lang.String value) {
		this.lastLoginIp = value;
	}
	
	public java.util.Date getLastLoginTime() {
		return this.lastLoginTime;
	}
	
	public void setLastLoginTime(java.util.Date value) {
		this.lastLoginTime = value;
	}
	
	public java.util.Date getPasswordExpireTime() {
		return this.passwordExpireTime;
	}
	
	public void setPasswordExpireTime(java.util.Date value) {
		this.passwordExpireTime = value;
	}
	
	public java.lang.String getActiveFlag() {
		return this.activeFlag;
	}
	
	public void setActiveFlag(java.lang.String value) {
		this.activeFlag = value;
	}
	
	public java.lang.String getCreateBy() {
		return this.createBy;
	}
	
	public void setCreateBy(java.lang.String value) {
		this.createBy = value;
	}
	
	public java.lang.String getCreateByName() {
		return this.createByName;
	}
	
	public void setCreateByName(java.lang.String value) {
		this.createByName = value;
	}
	
	public java.util.Date getCreateDate() {
		return this.createDate;
	}
	
	public void setCreateDate(java.util.Date value) {
		this.createDate = value;
	}
	
	public java.lang.String getModifiedBy() {
		return this.modifiedBy;
	}
	
	public void setModifiedBy(java.lang.String value) {
		this.modifiedBy = value;
	}
	
	public java.lang.String getModifiedByName() {
		return this.modifiedByName;
	}
	
	public void setModifiedByName(java.lang.String value) {
		this.modifiedByName = value;
	}
	
	public java.util.Date getModifiedDate() {
		return this.modifiedDate;
	}
	
	public void setModifiedDate(java.util.Date value) {
		this.modifiedDate = value;
	}
	
	public Map<String, Object> getSearchMap() {
		return searchMap;
	}
	
	public void setSearchMap(Map<String, Object> searchMap) {
		this.searchMap = searchMap;
	}
	
	public java.lang.Long[] getIds() {
		return ids;
	}
	
	public void setIds(java.lang.Long[] ids) {
		this.ids = ids;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.DEFAULT_STYLE);
	}
	
}

