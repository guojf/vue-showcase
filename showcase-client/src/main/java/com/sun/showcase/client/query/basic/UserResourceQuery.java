package com.sun.showcase.client.query.basic;
import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.sun.showcase.client.domain.basic.UserResource;
import com.sun.showcase.pojo.SearchModel;

public class UserResourceQuery extends SearchModel<UserResource> implements Serializable {
    
  private static final long serialVersionUID = 3148176768559230877L;
    

	  /**
     * 用户ID       db_column: user_id 
     */	
	private java.lang.String userId;
	  /**
     * 资源ID       db_column: resource_id 
     */	
	private java.lang.String resourceId;
	private java.lang.String id;
	
	public java.lang.String getId() {
		return id;
	}

	public void setId(java.lang.String id) {
		this.id = id;
	}

	public java.lang.String getUserId() {
		return this.userId;
	}
	
	public void setUserId(java.lang.String value) {
		this.userId = value;
	}
	
	public java.lang.String getResourceId() {
		return this.resourceId;
	}
	
	public void setResourceId(java.lang.String value) {
		this.resourceId = value;
	}
	
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.DEFAULT_STYLE);
	}
	
}

