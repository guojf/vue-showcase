package com.sun.showcase.client.query.basic;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.sun.showcase.client.domain.basic.RoleInfo;
import com.sun.showcase.pojo.SearchModel;
public class RoleInfoQuery extends SearchModel<RoleInfo> implements Serializable {
    
  private static final long serialVersionUID = 3148176768559230877L;
    

	  /**
     * 角色ID       db_column: ID 
     */	
	private java.lang.String id;
	  /**
     * 角色名称       db_column: NAME 
     */	
	private java.lang.String name;
	  /**
     * 角色描述       db_column: DESCRIPTION 
     */	
	private java.lang.String description;
	  /**
     * 创建时间       db_column: CREATE_DATE 
     */	
	private java.util.Date createDate;
	  /**
     * 创建人       db_column: CREATE_BY 
     */	
	private java.lang.String createBy;
	  /**
     * 修改时间       db_column: MODIFIED_DATE 
     */	
	private java.util.Date modifiedDate;
	  /**
     * 修改人       db_column: MODIFIED_BY 
     */	
	private java.lang.String modifiedBy;
	
	private String ids[];
	//所有资源ids
	private String rsIds;
	
	//角色已有资源ids
	private String upRsIds[];
	//更新时 资源树数据
	private String updateIds;

	public java.lang.String getId() {
		return this.id;
	}
	
	public void setId(java.lang.String value) {
		this.id = value;
	}
	
	public java.lang.String getName() {
		return this.name;
	}
	
	public void setName(java.lang.String value) {
		this.name = value;
	}
	
	public java.lang.String getDescription() {
		return this.description;
	}
	
	public void setDescription(java.lang.String value) {
		this.description = value;
	}
	
	public java.util.Date getCreateDate() {
		return this.createDate;
	}
	
	public void setCreateDate(java.util.Date value) {
		this.createDate = value;
	}
	
	public java.lang.String getCreateBy() {
		return this.createBy;
	}
	
	public void setCreateBy(java.lang.String value) {
		this.createBy = value;
	}
	
	public java.util.Date getModifiedDate() {
		return this.modifiedDate;
	}
	
	public void setModifiedDate(java.util.Date value) {
		this.modifiedDate = value;
	}
	
	public java.lang.String getModifiedBy() {
		return this.modifiedBy;
	}
	
	public void setModifiedBy(java.lang.String value) {
		this.modifiedBy = value;
	}
	

	public String[] getIds() {
		return ids;
	}

	public void setIds(String[] ids) {
		this.ids = ids;
	}

	public String getRsIds() {
		return rsIds;
	}

	public void setRsIds(String rsIds) {
		this.rsIds = rsIds;
	}

	public String[] getUpRsIds() {
		return upRsIds;
	}

	public void setUpRsIds(String[] upRsIds) {
		this.upRsIds = upRsIds;
	}

	public String getUpdateIds() {
		return updateIds;
	}

	public void setUpdateIds(String updateIds) {
		this.updateIds = updateIds;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.DEFAULT_STYLE);
	}
	
}

