package com.sun.showcase.client.query.basic;
import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.sun.showcase.client.domain.basic.UserGroup;
import com.sun.showcase.pojo.SearchModel;
public class UserGroupQuery extends SearchModel<UserGroup> implements Serializable {
    
  private static final long serialVersionUID = 3148176768559230877L;
    

	  /**
     * 用户ID       db_column: USER_ID 
     */	
	private java.lang.String userId;
	  /**
     * 组ID       db_column: GROUP_ID 
     */	
	private java.lang.String groupId;
	  /**
     * 是否为管理员 0：不是 1：是       db_column: IS_ADMIN 
     */	
	private java.lang.String isAdmin;

    private String name;
	
	private String status;
	
	private String nickName;
	
	private String email;
	
	public java.lang.String getUserId() {
		return this.userId;
	}
	
	public void setUserId(java.lang.String value) {
		this.userId = value;
	}
	
	public java.lang.String getGroupId() {
		return this.groupId;
	}
	
	public void setGroupId(java.lang.String value) {
		this.groupId = value;
	}
	
	public java.lang.String getIsAdmin() {
		return this.isAdmin;
	}
	
	public void setIsAdmin(java.lang.String value) {
		this.isAdmin = value;
	}
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.DEFAULT_STYLE);
	}
	
}

