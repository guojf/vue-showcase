package com.sun.showcase.client.domain.basic;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.sun.showcase.utils.DateUtils;
public class BaseInfo implements java.io.Serializable{
	private static final long serialVersionUID = 5454155825314635342L;
	public static final String TABLE_ALIAS = "BaseseInfo";
    /**
     * 行号       db_column: ID 
     */	
	private java.lang.String id;
    /**
     * 名称       db_column: ITEM_NAME 
     */	
	private java.lang.String itemName;
    /**
     * 编码       db_column: ITEM_CODE 
     */	
	private java.lang.String itemCode;
	
	private String itemCodeCopy;//编码副本
    /**
     * 父类名称       db_column: PARENT_NAME 
     */	
	private java.lang.String parentName;
    /**
     * 父类编码       db_column: PARENT_CODE 
     */	
	private java.lang.String parentCode;
    /**
     * 是否叶子节点1:是；0:否；       db_column: IS_LEAF 
     */	
	private java.lang.String isLeaf;
    /**
     * 序号 序号小的在前       db_column: ORDER_NUM 
     */	
	private java.lang.Integer orderNum;
	/**
	 * 文件ID
	 */
	private Integer fileId;
	/**
	 * 文件名称
	 */
	private String fileName;
	/**
	 * 文件路径
	 */
	private String filePath;
    /**
     * 备注       db_column: REMARK 
     */	
	private java.lang.String remark;
    /**
     * 有效状态  0：无效  1：有效       db_column: ACTIVE_FLAG 
     */	
	private java.lang.String activeFlag;
    /**
     * 创建时间       db_column: CREATE_DATE 
     */	
	private java.util.Date createDate;
    /**
     * 创建人       db_column: CREATE_BY 
     */	
	private java.lang.String createBy;
    /**
     * 修改时间       db_column: MODIFIED_DATE 
     */	
	private java.util.Date modifiedDate;
    /**
     * 修改人       db_column: MODIFIED_BY 
     */	
	private java.lang.String modifiedBy;
	//columns END

	public BaseInfo(){
	}

	public BaseInfo(
		java.lang.String id
	){
		this.id = id;
	}

	public void setId(java.lang.String value) {
		this.id = value;
	}
	
	public java.lang.String getId() {
		return this.id;
	}
	public void setItemName(java.lang.String value) {
		this.itemName = value;
	}
	
	public java.lang.String getItemName() {
		return this.itemName;
	}
	public void setItemCode(java.lang.String value) {
		this.itemCode = value;
	}
	
	public java.lang.String getItemCode() {
		return this.itemCode;
	}
	public void setParentName(java.lang.String value) {
		this.parentName = value;
	}
	
	public String getItemCodeCopy() {
		return itemCodeCopy;
	}

	public void setItemCodeCopy(String itemCodeCopy) {
		this.itemCodeCopy = itemCodeCopy;
	}

	public java.lang.String getParentName() {
		return this.parentName;
	}
	public void setParentCode(java.lang.String value) {
		this.parentCode = value;
	}
	
	public java.lang.String getParentCode() {
		return this.parentCode;
	}
	public void setIsLeaf(java.lang.String value) {
		this.isLeaf = value;
	}
	
	public java.lang.String getIsLeaf() {
		return this.isLeaf;
	}
	public void setOrderNum(java.lang.Integer value) {
		this.orderNum = value;
	}
	
	public java.lang.Integer getOrderNum() {
		return this.orderNum;
	}
	
	public Integer getFileId() {
		return fileId;
	}

	public void setFileId(Integer fileId) {
		this.fileId = fileId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public void setRemark(java.lang.String value) {
		this.remark = value;
	}
	
	public java.lang.String getRemark() {
		return this.remark;
	}
	public void setActiveFlag(java.lang.String value) {
		this.activeFlag = value;
	}
	
	public java.lang.String getActiveFlag() {
		return this.activeFlag;
	}
	public String getCreateDateString() {
		//return DateConvertUtils.format(getCreateDate(), FORMAT_CREATE_DATE);
		return  DateUtils.format(DateUtils.format2,getCreateDate());
	}
	public void setCreateDateString(String value) {
		setCreateDate(DateUtils.parse(value,DateUtils.format2,java.util.Date.class));
	}
	
	public void setCreateDate(java.util.Date value) {
		this.createDate = value;
	}
	
	public java.util.Date getCreateDate() {
		return this.createDate;
	}
	public void setCreateBy(java.lang.String value) {
		this.createBy = value;
	}
	
	public java.lang.String getCreateBy() {
		return this.createBy;
	}
	public String getModifiedDateString() {
		//return DateConvertUtils.format(getModifiedDate(), FORMAT_MODIFIED_DATE);
		return  DateUtils.format(DateUtils.format2,getModifiedDate());
	}
	public void setModifiedDateString(String value) {
		setModifiedDate(DateUtils.parse(value,DateUtils.format2,java.util.Date.class));
	}
	
	public void setModifiedDate(java.util.Date value) {
		this.modifiedDate = value;
	}
	
	public java.util.Date getModifiedDate() {
		return this.modifiedDate;
	}
	public void setModifiedBy(java.lang.String value) {
		this.modifiedBy = value;
	}
	
	public java.lang.String getModifiedBy() {
		return this.modifiedBy;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
	public int hashCode() {
		return new HashCodeBuilder()
			.append(getId())
			.toHashCode();
	}
	
	public boolean equals(Object obj) {
		if(obj instanceof BaseInfo == false) return false;
		if(this == obj) return true;
		BaseInfo other = (BaseInfo)obj;
		return new EqualsBuilder()
			.append(getId(),other.getId())
			.isEquals();
	}
}

