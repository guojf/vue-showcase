package com.sun.showcase.biz.basic.excel;

import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import com.sun.showcase.client.query.basic.UserInfoQuery;
import com.sun.showcase.excel.AbstractExcelExportTemplate;

public class UserInfoTemplate extends AbstractExcelExportTemplate<List<UserInfoQuery>>{

	@Override
	public String[] getSheetNames() {
		return new String[]{"用户列表"};
	}

	@Override
	public String[][] getTitles() {
		return new String[][]{{"ID ","账号","昵称"
			}};
	}

	@Override
	protected void buildBody(int sheetIndex) {
		List<UserInfoQuery> list = this.parameters;
		
		Sheet sheet = getSheet(sheetIndex);
		
		UserInfoQuery query = null;
		
		int startIndex = this.getBodyStartIndex(sheetIndex),
				i=0,
				index=0;
		
		for(Iterator<UserInfoQuery> it = list.iterator();it.hasNext();){
			
			Row row = sheet.createRow(i+startIndex);
			row.setHeight((short)300);
			
			index = 0;
			i=i+1;
			
		    query = it.next();
		    
		    createStyledCell(row,index++,query.getId());//订单号
		    createStyledCell(row,index++,query.getName());//账号
		    createStyledCell(row,index++,query.getNickName());//昵称
		}
		
	}

}
