package com.sun.showcase.biz.basic.service.impl;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sun.showcase.biz.basic.dao.RoleInfoDao;
import com.sun.showcase.biz.basic.service.RoleInfoService;
import com.sun.showcase.pojo.DataGrid;
import com.sun.showcase.pojo.Pager;
import com.sun.showcase.client.domain.basic.RoleInfo;
import com.sun.showcase.client.domain.basic.RoleResource;
import com.sun.showcase.client.query.basic.RoleInfoQuery;
import com.sun.showcase.utils.ExecuteResult;
import com.sun.showcase.utils.LoginContextHolder;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
@Service("roleInfoService")
@Transactional
public class RoleInfoServiceImpl implements RoleInfoService{
	@Resource
	private RoleInfoDao roleInfoDao;
	

	public DataGrid datagrid(RoleInfoQuery roleInfoQuery) {
		DataGrid j = new DataGrid();
		Pager<RoleInfo> pager  = find(roleInfoQuery);
		j.setRows(getQuerysFromEntitys(pager.getRecords()));
		j.setTotal(pager.getTotalRecords());
		return j;
	}

	private List<RoleInfoQuery> getQuerysFromEntitys(List<RoleInfo> roleInfos) {
		List<RoleInfoQuery> roleInfoQuerys = new ArrayList<RoleInfoQuery>();
		if (roleInfos != null && roleInfos.size() > 0) {
			for (RoleInfo tb : roleInfos) {
				RoleInfoQuery b = new RoleInfoQuery();
				BeanUtils.copyProperties(tb, b);
				roleInfoQuerys.add(b);
			}
		}
		return roleInfoQuerys;
	}

	private Pager<RoleInfo> find(RoleInfoQuery roleInfoQuery) {
		Page<RoleInfo> page = PageHelper.startPage(roleInfoQuery.getPage().intValue(), roleInfoQuery.getRows().intValue());
		List<RoleInfo> list = roleInfoDao.findList(roleInfoQuery);
		Pager<RoleInfo> pager = new Pager<RoleInfo>();
		pager.setRecords(list);
		pager.setTotalRecords(page.getTotal());
		return pager;
		
	}
	


	public ExecuteResult<RoleInfo> add(RoleInfoQuery roleInfoQuery) {
		ExecuteResult<RoleInfo> executeResult = new ExecuteResult<RoleInfo>();
		List<RoleInfo> listRole=roleInfoDao.getByName(roleInfoQuery);
		if(listRole.size()>0){
			executeResult.addErrorMessage("角色["+roleInfoQuery.getName()+"]已经存在.");
			return executeResult;
		}
		RoleInfo roleInfo = new RoleInfo();
		BeanUtils.copyProperties(roleInfoQuery, roleInfo);
		roleInfo.setCreateBy(LoginContextHolder.get().getUserName());
		roleInfo.setCreateDate(new Date());
		roleInfoDao.save(roleInfo);
		//角色添加成功后  新增角色资源表关联数据
		String resourceIds[]=roleInfoQuery.getRsIds().split(",");
		for(int i=0;i<resourceIds.length;i++){
			if(!(resourceIds[i]==null||"".equals(resourceIds[i]))){
				RoleResource roleResource = new RoleResource();
				roleResource.setRoleId(roleInfo.getId());
				roleResource.setResourceId(resourceIds[i]);
				roleInfoDao.saveRoleResource(roleResource);
			}
		}
		roleInfoQuery.setId(roleInfo.getId());
		executeResult.setResult(roleInfo);
		return executeResult;
		
	}

	public ExecuteResult<RoleInfo> update(RoleInfoQuery roleInfoQuery) {
		ExecuteResult<RoleInfo> executeResult = new ExecuteResult<RoleInfo>();
		
		RoleInfo roleInfo = roleInfoDao.getById(roleInfoQuery.getId());
	    if(roleInfo != null) {
	    	BeanUtils.copyProperties(roleInfoQuery, roleInfo);
		}else{
			executeResult.addErrorMessage("不存在的角色信息!");
			return executeResult;
		}
	    if(!roleInfo.getName().equals(roleInfoQuery.getName())){//与原名不一样
	    	List<RoleInfo> listRole=roleInfoDao.getByName(roleInfoQuery);
			if(listRole.size()>0){//数据库中已存在现在的名字
				executeResult.addErrorMessage("角色["+roleInfoQuery.getName()+"]已经存在.");
				return executeResult;
			}
	    }
	    roleInfo.setModifiedBy(LoginContextHolder.get().getUserName());
	    roleInfo.setModifiedDate(new Date());
	    roleInfoDao.update(roleInfo);
	    //删除角色资源关系表数据
	    roleInfoDao.delRoleResourcce(roleInfo.getId());
	    //新增角色资源关系表数据
	    String resourceIds[]=roleInfoQuery.getRsIds().split(",");
		for(int i=0;i<resourceIds.length;i++){
			if(!(resourceIds[i]==null||"".equals(resourceIds[i]))){
				RoleResource roleResource = new RoleResource();
				roleResource.setRoleId(roleInfo.getId());
				roleResource.setResourceId(resourceIds[i]);
				roleInfoDao.saveRoleResource(roleResource);
			}
		}
	    executeResult.setResult(roleInfo);
	    return executeResult;
	}

	public void delete(java.lang.String[] ids) {
		if (ids != null) {
			for(java.lang.String id : ids){
				RoleInfo t = roleInfoDao.getById(id);
				if (t != null) {
					roleInfoDao.deleteById(id);
					//删除角色资源关系表数据
				    roleInfoDao.delRoleResourcce(id);
				}
			}
		}
	}

	public RoleInfo get(RoleInfoQuery roleInfoQuery) {
		return roleInfoDao.getById(roleInfoQuery.getId());
	}

	public RoleInfo get(String id) {
		return roleInfoDao.getById(id);
	}

	
	public List<RoleInfoQuery> listAll(RoleInfoQuery roleInfoQuery) {
	    List<RoleInfo> list = roleInfoDao.findList(roleInfoQuery);
		List<RoleInfoQuery> listQuery =getQuerysFromEntitys(list) ;
		return listQuery;
	}

	@Override
	public List<RoleResource> getRoleResourceByRoleID(
			RoleInfoQuery roleInfoQuery) {
		return roleInfoDao.getRoleResourceByRoleID(roleInfoQuery);
	}

	@Override
	public Integer delResourcceRole(String resourceId) {
		return roleInfoDao.delResourcceRole(resourceId);
	}
	
	
}
