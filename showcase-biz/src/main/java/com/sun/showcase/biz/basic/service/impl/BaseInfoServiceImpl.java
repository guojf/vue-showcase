package com.sun.showcase.biz.basic.service.impl;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sun.showcase.biz.basic.dao.BaseInfoDao;
import com.sun.showcase.biz.basic.service.BaseInfoService;
import com.sun.showcase.client.domain.basic.BaseInfo;
import com.sun.showcase.client.query.basic.BaseInfoQuery;
import com.sun.showcase.pojo.DataGrid;
import com.sun.showcase.pojo.Pager;
import com.sun.showcase.utils.TreeNode;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
@Service("baseInfoService")
@Transactional
public class BaseInfoServiceImpl implements BaseInfoService{
	@Autowired
	private BaseInfoDao baseInfoDao;

	public DataGrid datagrid(BaseInfoQuery baseInfoQuery) {
		DataGrid j = new DataGrid();
		Pager<BaseInfo> pager  = find(baseInfoQuery);
		j.setRows(getQuerysFromEntitys(pager.getRecords()));
		j.setTotal(pager.getTotalRecords());
		return j;
	}

	private List<BaseInfoQuery> getQuerysFromEntitys(List<BaseInfo> baseInfos) {
		List<BaseInfoQuery> baseInfoQuerys = new ArrayList<BaseInfoQuery>();
		if (baseInfos != null && baseInfos.size() > 0) {
			for (BaseInfo tb : baseInfos) {
				BaseInfoQuery b = new BaseInfoQuery();
				BeanUtils.copyProperties(tb, b);
				baseInfoQuerys.add(b);
			}
		}
		return baseInfoQuerys;
	}

	private Pager<BaseInfo> find(BaseInfoQuery baseInfoQuery) {
		Page<BaseInfo> page = PageHelper.startPage(baseInfoQuery.getPage().intValue(), baseInfoQuery.getRows().intValue());
		
		List<BaseInfo> list = baseInfoDao.findPage(baseInfoQuery);
		Pager<BaseInfo> pager = new Pager<BaseInfo>();
		pager.setRecords(list);
		pager.setTotalRecords(page.getTotal());
		return pager;
		
	}


	public BaseInfo add(BaseInfoQuery baseInfoQuery) {
		
		BaseInfo t = new BaseInfo();
		
		BaseInfoQuery tmp = new BaseInfoQuery();
		tmp.setParentCode(baseInfoQuery.getParentCode());
		List<BaseInfoQuery> list = this.listAll(tmp);
		if(list.size() == 0){
			baseInfoQuery.setItemCode(baseInfoQuery.getParentCode()+"01");
		}else{
			BaseInfoQuery brother = list.get(0);
			int i = Integer.parseInt(brother.getItemCode().substring(brother.getItemCode().length()-1))+1;
			String code = baseInfoQuery.getParentCode()+"0"+i;
			if(i>9){
				code = baseInfoQuery.getParentCode()+i;
			}
			baseInfoQuery.setItemCode(code);
		}
		BeanUtils.copyProperties(baseInfoQuery, t);
		t.setCreateBy(baseInfoQuery.getCreateBy());
		baseInfoDao.save(t);
		baseInfoQuery.setId(t.getId());
		
		updateLeaf(t.getParentCode(),"0");
		
		return t;
		
	}

	private void updateLeaf(String parentCode,String isLeaf){
		
		BaseInfo info = baseInfoDao.getByCode(parentCode);
		
		if(info!=null&&!isLeaf.equals(info.getIsLeaf())){
			BaseInfoQuery query = new BaseInfoQuery();
			
			query.setItemCode(parentCode);
			query.setIsLeaf(isLeaf);
			query.setId(info.getId());
			
			baseInfoDao.updateDynamic(query);
		}
	}
	
	public void update(BaseInfoQuery baseInfoQuery) {
		BaseInfo t = baseInfoDao.getById(baseInfoQuery.getId());
	    if(t != null) {
	    	BeanUtils.copyProperties(baseInfoQuery, t);
		}
	    baseInfoDao.update(t);
	}

	public void delete(BaseInfoQuery baseInfoQuery) {
		
		if (baseInfoQuery != null) {
			baseInfoDao.updateDynamic(baseInfoQuery);
			
			BaseInfo t = baseInfoDao.getById(baseInfoQuery.getId());
			
			if(t!=null){
				//检索父类信息
				BaseInfoQuery parent = new BaseInfoQuery();
				
				parent.setParentCode(t.getParentCode());
				parent.setActiveFlag("1");
				
				List<BaseInfoQuery> list = listAll(parent);
				
				if(list.size()==0){
					updateLeaf(parent.getParentCode(),"1");
				}
			}
		}
	}

	public BaseInfo get(BaseInfoQuery baseInfoQuery) {
		return baseInfoDao.getById(baseInfoQuery.getId());
	}

	public BaseInfo get(String id) {
		return baseInfoDao.getById(id);
	}

	
	public List<BaseInfoQuery> listAll(BaseInfoQuery baseInfoQuery) {
	    List<BaseInfo> list = baseInfoDao.findList(baseInfoQuery);
		List<BaseInfoQuery> listQuery =getQuerysFromEntitys(list) ;
		
		if(listQuery!=null&&listQuery.size()>0&&"1".equals(baseInfoQuery.getIsWhole())){
			BaseInfoQuery whole = new BaseInfoQuery();
			whole.setItemName("全部");
			listQuery.add(0, whole);
		
		}
		return listQuery;
	}

	@Override
	public List<TreeNode> findTrees(BaseInfoQuery baseInfoQuery) {
		List<BaseInfo> lists = baseInfoDao.findTrees(baseInfoQuery);
		List<TreeNode> tree =null;
		
		if(lists!=null&&lists.size()>0){
			
			tree = new ArrayList<TreeNode>();
			
			for(BaseInfo temp : lists){
				
				TreeNode node = new TreeNode();
				
				node.setId(temp.getItemCode());
				
				node.setText(temp.getItemName());
				
				//node.setObj(temp);
				
				BaseInfoQuery t = new BaseInfoQuery();
				t.setParentCode(temp.getItemCode());
				Set<TreeNode> childrens =  recurrence(t);
				
				if(childrens!=null&&childrens.size()>0){
					
					node.setChildren(childrens);
					node.setState("closed");
				}
				
				tree.add(node);
			}
		}
		
		return tree;
	}
	
private Set<TreeNode> recurrence(BaseInfoQuery baseInfoQuery){
		
		List<BaseInfo> lists = baseInfoDao.findTrees(baseInfoQuery);
		
		Set<TreeNode> tree = null;
		
		if(lists!=null&&lists.size()>0){
			
			tree = new LinkedHashSet<TreeNode>();
			
			for(BaseInfo temp : lists){
				
				TreeNode node = new TreeNode();
				
				node.setId(temp.getItemCode());
				
				node.setText(temp.getItemName());
				
				node.setObj(temp);
				
				BaseInfoQuery t = new BaseInfoQuery();
				t.setParentCode(temp.getItemCode());
				Set<TreeNode> childrens = recurrence(t);
				
				if(childrens!=null&&childrens.size()>0){
					node.setChildren(childrens);
					node.setState("closed");
				}
				
				tree.add(node);
			}
		}
		return tree;
	}

	@Override
	public boolean findValidates(BaseInfoQuery baseInfoQuery) {
		boolean flag = false;
		
		List<BaseInfo> list = baseInfoDao.findValidates(baseInfoQuery);
		
		if(list!=null&&list.size()>0) flag = true;
		
		return flag;
	}

	@Override
	public BaseInfo getByItemName(String itemName) {
		return baseInfoDao.getByItemName(itemName);
	}
	
}
