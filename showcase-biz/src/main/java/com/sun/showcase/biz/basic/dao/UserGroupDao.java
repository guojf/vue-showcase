package com.sun.showcase.biz.basic.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.sun.showcase.client.domain.basic.UserGroup;
import com.sun.showcase.client.query.basic.UserGroupQuery;
import com.sun.showcase.dao.BaseDao;
@Mapper
public interface UserGroupDao extends BaseDao<UserGroup,java.lang.String>{
	
	public void delete(UserGroupQuery userGroupQuery) ;

	public UserGroup get(UserGroupQuery userGroupQuery) ;

	public void deleteByType(UserGroupQuery delUserGroupQuery);
	
	public List<UserGroup> findList(UserGroupQuery query);
	
	public List<UserGroup> findPage(UserGroupQuery query);
}
