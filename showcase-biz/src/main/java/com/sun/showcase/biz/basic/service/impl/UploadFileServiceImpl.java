
package com.sun.showcase.biz.basic.service.impl;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sun.showcase.biz.basic.dao.UploadFileDao;
import com.sun.showcase.biz.basic.service.UploadFileService;
import com.sun.showcase.client.domain.basic.UploadFile;
import com.sun.showcase.client.query.basic.UploadFileQuery;
import com.sun.showcase.pojo.DataGrid;
import com.sun.showcase.pojo.Pager;
import com.sun.showcase.utils.LoginContextHolder;
import com.sun.showcase.utils.StringConstant;
@Service("uploadFileService")
@Transactional
public class UploadFileServiceImpl implements UploadFileService{
	@Autowired
	private UploadFileDao uploadFileDao;
	
	public void setUploadFileDao(UploadFileDao dao) {
		this.uploadFileDao = dao;
	}

	public DataGrid datagrid(UploadFileQuery uploadFileQuery) {
		DataGrid j = new DataGrid();
		Pager<UploadFile> pager  = find(uploadFileQuery);
		j.setRows(getQuerysFromEntitys(pager.getRecords()));
		j.setTotal(pager.getTotalRecords());
		return j;
	}

	private List<UploadFileQuery> getQuerysFromEntitys(List<UploadFile> uploadFiles) {
		List<UploadFileQuery> uploadFileQuerys = new ArrayList<UploadFileQuery>();
		if (uploadFiles != null && uploadFiles.size() > 0) {
			for (UploadFile tb : uploadFiles) {
				UploadFileQuery b = new UploadFileQuery();
				BeanUtils.copyProperties(tb, b);
				uploadFileQuerys.add(b);
			}
		}
		return uploadFileQuerys;
	}

	private Pager<UploadFile> find(UploadFileQuery uploadFileQuery) {
		Page<UploadFile> page = PageHelper.startPage(uploadFileQuery.getPage().intValue(), uploadFileQuery.getRows().intValue());
		List<UploadFile> list = uploadFileDao.findList(uploadFileQuery);
		Pager<UploadFile> pager = new Pager<UploadFile>();
		pager.setRecords(list);
		pager.setTotalRecords(page.getTotal());
		return  pager;
		
	}
	


	public UploadFile add(UploadFileQuery uploadFileQuery) {
		UploadFile t = new UploadFile();
		BeanUtils.copyProperties(uploadFileQuery, t);
		t.setActiveFlag(StringConstant.ONE);
		t.setCreatedBy(LoginContextHolder.get().getUserId());
		t.setCreated(new Date());
	    t.setLastUpdBy(LoginContextHolder.get().getUserId());
	    t.setLastUpd(new Date());
		uploadFileDao.save(t);
		uploadFileQuery.setId(t.getId());
		return t;
		
	}

	public void update(UploadFileQuery uploadFileQuery) {
		UploadFile t = uploadFileDao.getById(uploadFileQuery.getId());
	    if(t != null) {
	    	BeanUtils.copyProperties(uploadFileQuery, t);
	    	t.setLastUpdBy(LoginContextHolder.get().getUserId());
	 	    t.setLastUpd(new Date());
	    	uploadFileDao.update(t);
		}
	}
	
	/**
	 * 根据条件更新部分字段   新建一个query封装需要更新的字段
	 * searchMap封装更新条件
	 * */
	public void updatePart(UploadFileQuery uploadFileQuery) {
	    uploadFileDao.updatePart(uploadFileQuery);
	}

	public void delete(java.lang.Long[] ids) {
		uploadFileDao.deleteAc(ids);
	}
	
	//逻辑删除 更新状态位
	public void deletePt(java.lang.Long[] ids) {
		uploadFileDao.deletePt(ids);
	}

	public UploadFile get(UploadFileQuery uploadFileQuery) {
		return uploadFileDao.getById(new Long(uploadFileQuery.getId()));
	}

	public UploadFile get(Long id) {
		return uploadFileDao.getById(id);
	}

	
	public List<UploadFileQuery> listAll(UploadFileQuery uploadFileQuery) {
	    List<UploadFile> list = uploadFileDao.findList(uploadFileQuery);
		List<UploadFileQuery> listQuery =getQuerysFromEntitys(list) ;
		return listQuery;
	}

}
