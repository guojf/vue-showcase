package com.sun.showcase.biz.basic.dao;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.sun.showcase.client.domain.basic.BaseInfo;
import com.sun.showcase.client.query.basic.BaseInfoQuery;
import com.sun.showcase.dao.BaseDao;
@Mapper
public interface BaseInfoDao extends BaseDao<BaseInfo,java.lang.String>{
	
	public List<BaseInfo> findPage(BaseInfoQuery query);
	
	public List<BaseInfo> findList(BaseInfoQuery query);

	public List<BaseInfo> findValidates(BaseInfoQuery query);
	 
	public List<BaseInfo> findTrees(BaseInfoQuery query);
	
	public void updateDynamic(BaseInfoQuery query);
	
	public BaseInfo getByCode(String code);
	
	public BaseInfo getByItemName(String itemName);
}
