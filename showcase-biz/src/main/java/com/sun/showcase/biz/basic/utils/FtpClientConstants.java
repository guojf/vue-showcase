package com.sun.showcase.biz.basic.utils;
import java.io.File;

import javax.annotation.PostConstruct;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
@Configuration  
@ConfigurationProperties(prefix = "ftp")
@PropertySource(value = "classpath:ftp_upload_file.properties")
@Component
public class FtpClientConstants {

	private String host;//ip地址
	
	private String port;//端口号
	
	private String username;//登陆名
	
	private String password;//密码
	
	private String disk;//盘符
	
	private String windowsPath;//windows 路径
	
	private String linuxPath;//linux 路径

	private String prefixUrl;
	
	//初始化本地副本
	@PostConstruct
	private void init(){
		
		String os = System.getProperty("os.name");
		
		//windows操作系统
		if(os.toLowerCase().startsWith("win")){
			
			this.setDisk(this.getWindowsPath());
			

		}else{
			
			this.setDisk(this.getLinuxPath());
		}
		
		File root = new File(this.getDisk());
		
		if(!root.exists()&&!root.isDirectory())
			root.mkdirs();
	}
	
	public String getPrefixUrl() {
		return prefixUrl;
	}

	public void setPrefixUrl(String prefixUrl) {
		this.prefixUrl = prefixUrl;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDisk() {
		return this.disk;
	}

	public void setDisk(String disk) {
		this.disk = disk;
	}

	public String getWindowsPath() {
		return windowsPath;
	}

	public void setWindowsPath(String windowsPath) {
		this.windowsPath = windowsPath;
	}

	public String getLinuxPath() {
		return linuxPath;
	}

	public void setLinuxPath(String linuxPath) {
		this.linuxPath = linuxPath;
	}
	
	
}
