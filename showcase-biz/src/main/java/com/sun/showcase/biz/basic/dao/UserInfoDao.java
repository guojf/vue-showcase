package com.sun.showcase.biz.basic.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.sun.showcase.client.domain.basic.UserInfo;
import com.sun.showcase.client.query.basic.UserInfoQuery;
import com.sun.showcase.dao.BaseDao;

@Mapper
public interface UserInfoDao extends BaseDao<UserInfo,java.lang.String>{
	
	public List<UserInfo> findList(UserInfoQuery query);
	
	public UserInfo getByName(String name);

	public void editPassword(UserInfoQuery userInfoQuery) ;

	public void modify(UserInfoQuery userInfoQuery);
	 
}
