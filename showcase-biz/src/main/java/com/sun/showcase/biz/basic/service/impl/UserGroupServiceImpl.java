package com.sun.showcase.biz.basic.service.impl;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sun.showcase.biz.basic.dao.GroupInfoDao;
import com.sun.showcase.biz.basic.dao.UserGroupDao;
import com.sun.showcase.biz.basic.service.UserGroupService;
import com.sun.showcase.pojo.DataGrid;
import com.sun.showcase.pojo.Pager;
import com.sun.showcase.client.domain.basic.GroupInfo;
import com.sun.showcase.client.domain.basic.UserGroup;
import com.sun.showcase.client.query.basic.GroupInfoQuery;
import com.sun.showcase.client.query.basic.UserGroupQuery;
import com.sun.showcase.utils.TreeNode;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
@Service("userGroupService")
@Transactional
public class UserGroupServiceImpl implements UserGroupService{
	@Autowired
	private UserGroupDao userGroupDao;
	
	@Autowired
	private GroupInfoDao groupInfoDao;
	

	public void setGroupInfoDao(GroupInfoDao groupInfoDao) {
		this.groupInfoDao = groupInfoDao;
	}

	public void setUserGroupDao(UserGroupDao dao) {
		this.userGroupDao = dao;
	}

	public DataGrid datagrid(UserGroupQuery userGroupQuery) {
		DataGrid j = new DataGrid();
		Pager<UserGroup> pager  = find(userGroupQuery);
		j.setRows(getQuerysFromEntitys(pager.getRecords()));
		j.setTotal(pager.getTotalRecords());
		return j;
	}

	private List<UserGroupQuery> getQuerysFromEntitys(List<UserGroup> userGroups) {
		List<UserGroupQuery> userGroupQuerys = new ArrayList<UserGroupQuery>();
		if (userGroups != null && userGroups.size() > 0) {
			for (UserGroup tb : userGroups) {
				UserGroupQuery b = new UserGroupQuery();
				BeanUtils.copyProperties(tb, b);
				userGroupQuerys.add(b);
			}
		}
		return userGroupQuerys;
	}

	private Pager<UserGroup> find(UserGroupQuery userGroupQuery) {
		Page<UserGroup> page = PageHelper.startPage(userGroupQuery.getPage().intValue(), userGroupQuery.getRows().intValue());
		List<UserGroup> list = userGroupDao.findPage(userGroupQuery);
		Pager<UserGroup> pager = new Pager<UserGroup>();
		pager.setRecords(list);
		pager.setTotalRecords(page.getTotal());
		return pager;
		
	}
	


	public UserGroup add(UserGroupQuery userGroupQuery) {
		UserGroup t = new UserGroup();
		BeanUtils.copyProperties(userGroupQuery, t);
		userGroupDao.save(t);
		userGroupQuery.setUserId(t.getUserId());
		return t;
		
	}

	public void update(UserGroupQuery userGroupQuery) {
		UserGroup t = userGroupDao.get(userGroupQuery);
	    if(t != null) {
	    	BeanUtils.copyProperties(userGroupQuery, t);
		}
	    userGroupDao.update(t);
	}

	public void delete(UserGroupQuery userGroupQuery) {
		if (userGroupQuery != null) {
			userGroupDao.delete(userGroupQuery);
		}
	}

	public UserGroup get(UserGroupQuery userGroupQuery) {
		return userGroupDao.get(userGroupQuery);
	}

	public UserGroup get(String id) {
		return userGroupDao.getById(id);
	}

	
	public List<UserGroupQuery> listAll(UserGroupQuery userGroupQuery) {
	    List<UserGroup> list = userGroupDao.findList(userGroupQuery);
		List<UserGroupQuery> listQuery =getQuerysFromEntitys(list) ;
		return listQuery;
	}

	@Override
	public List<TreeNode> findGroupsInfo(UserGroupQuery userGroupQuery) {
		
		List<TreeNode> trees = null;
		List<GroupInfo> groupInfoQuerys = null;
		
		GroupInfoQuery groupInfoQuery = new GroupInfoQuery();
		if(StringUtils.isNotBlank(userGroupQuery.getUserId())){
			groupInfoQuery.setUserId(userGroupQuery.getUserId());
			groupInfoQuerys = groupInfoDao.findGroupInfoByUserId(groupInfoQuery);
		}else{
			groupInfoQuerys = groupInfoDao.findList(groupInfoQuery);
		}
		
		if(groupInfoQuerys!=null&&groupInfoQuerys.size()>0){
			
			TreeNode node;
			trees = new ArrayList<TreeNode>();
			
			for(int i=0;i<groupInfoQuerys.size();i++){
				
				node = new TreeNode();
				
				node.setId(groupInfoQuerys.get(i).getId());
				node.setText(groupInfoQuerys.get(i).getName());
				node.setChecked(groupInfoQuerys.get(i).getChecked());
				
				trees.add(node);
			}
			
		}
		
		return trees;
	}

	@Override
	public void deleteByType(UserGroupQuery delUserGroupQuery) {
		
		userGroupDao.deleteByType(delUserGroupQuery);
	}
	
	
}
