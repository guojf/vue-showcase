package com.sun.showcase.biz.basic.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.sun.showcase.client.domain.basic.GroupInfo;
import com.sun.showcase.client.query.basic.GroupInfoQuery;
import com.sun.showcase.dao.BaseDao;

/**
 * database table: group_info
 * database table comments: GroupInfo
 *
 */
@Mapper
public interface GroupInfoDao extends BaseDao<GroupInfo,java.lang.String>{
	
	public List<GroupInfo> findList(GroupInfoQuery query);
	
	public List<GroupInfo> findGroupInfoByUserId(GroupInfoQuery query);
	

}
