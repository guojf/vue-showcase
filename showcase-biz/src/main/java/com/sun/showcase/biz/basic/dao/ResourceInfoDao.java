package com.sun.showcase.biz.basic.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.sun.showcase.client.domain.basic.ResourceInfo;
import com.sun.showcase.client.query.basic.ResourceInfoQuery;
import com.sun.showcase.dao.BaseDao;

@Mapper
public interface ResourceInfoDao extends BaseDao<ResourceInfo,java.lang.String>{
	
	public List<ResourceInfo> findList(ResourceInfoQuery query);
	public List<ResourceInfo> findByUserId(String userId);
	//根据用户id 查询父菜单列表  左侧菜单使用
	public List<ResourceInfo> findParentByUserId(String userId);
	//根据菜单ID 查询该菜单下的子菜单  左侧菜单使用
	public List<ResourceInfo> findChildByParentId(ResourceInfoQuery query);
	//创建角色时 查询资源树
	public List<ResourceInfo> findParentResource();
	//创建角色时  根据父资源的id 查询子资源
	public List<ResourceInfo> findChildResourceByparentId(String parentId);
	//根据用户id 查询用户资源及组内信息
	public List<ResourceInfo> findByGroup(String userId);
	//根据用户个人资源查询资源信息
	public List<ResourceInfo> findByPersonResource(String userId);
}
