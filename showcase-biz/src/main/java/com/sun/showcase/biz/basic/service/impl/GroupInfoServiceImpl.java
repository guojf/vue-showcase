package com.sun.showcase.biz.basic.service.impl;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sun.showcase.biz.basic.dao.GroupInfoDao;
import com.sun.showcase.biz.basic.service.GroupInfoService;
import com.sun.showcase.biz.basic.service.UserGroupService;
import com.sun.showcase.pojo.DataGrid;
import com.sun.showcase.pojo.Pager;
import com.sun.showcase.client.domain.basic.GroupInfo;
import com.sun.showcase.client.query.basic.GroupInfoQuery;
import com.sun.showcase.client.query.basic.UserGroupQuery;
import com.sun.showcase.utils.LoginContextHolder;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
@Service("groupInfoService")
@Transactional
public class GroupInfoServiceImpl implements GroupInfoService{
	@Autowired
	private GroupInfoDao groupInfoDao;
	
	@Autowired
	private UserGroupService userGroupService;
	
	
	public void setUserGroupService(UserGroupService userGroupService) {
		this.userGroupService = userGroupService;
	}

	public void setGroupInfoDao(GroupInfoDao dao) {
		this.groupInfoDao = dao;
	}

	public DataGrid datagrid(GroupInfoQuery groupInfoQuery) {
		DataGrid j = new DataGrid();
		Pager<GroupInfo> pager  = find(groupInfoQuery);
		j.setRows(getQuerysFromEntitys(pager.getRecords()));
		j.setTotal(pager.getTotalRecords());
		return j;
	}

	private List<GroupInfoQuery> getQuerysFromEntitys(List<GroupInfo> groupInfos) {
		List<GroupInfoQuery> groupInfoQuerys = new ArrayList<GroupInfoQuery>();
		if (groupInfos != null && groupInfos.size() > 0) {
			for (GroupInfo tb : groupInfos) {
				GroupInfoQuery b = new GroupInfoQuery();
				BeanUtils.copyProperties(tb, b);
				groupInfoQuerys.add(b);
			}
		}
		return groupInfoQuerys;
	}

	private Pager<GroupInfo> find(GroupInfoQuery groupInfoQuery) {
		Page<GroupInfo> page = PageHelper.startPage(groupInfoQuery.getPage().intValue(), groupInfoQuery.getRows().intValue());
		
		List<GroupInfo> list = groupInfoDao.findList(groupInfoQuery);
		Pager<GroupInfo> pager = new Pager<GroupInfo>();
		pager.setRecords(list);
		pager.setTotalRecords(page.getTotal());
		return pager;
	}
	


	public GroupInfo add(GroupInfoQuery groupInfoQuery) {
		
		String userId = LoginContextHolder.get().getUserId();
		
		groupInfoQuery.setCreateBy(userId);
		GroupInfo t = new GroupInfo();
		BeanUtils.copyProperties(groupInfoQuery, t);
		groupInfoDao.save(t);
		groupInfoQuery.setId(t.getId());
		
		//添加组管理员权限
		UserGroupQuery userGroupQuery = new UserGroupQuery();
		
		userGroupQuery.setGroupId(t.getId());
		userGroupQuery.setUserId(userId);
		userGroupQuery.setIsAdmin("1");
		userGroupService.add(userGroupQuery);
		
		return t;
		
	}

	public void update(GroupInfoQuery groupInfoQuery) {
	
		GroupInfo t = groupInfoDao.getById(groupInfoQuery.getId());
	    if(t != null) {
	    	BeanUtils.copyProperties(groupInfoQuery, t);
	    	t.setModifiedBy(LoginContextHolder.get().getUserId());
		}
	    groupInfoDao.update(t);
	}

	public void delete(java.lang.String id) {
		if (id != null) {
			GroupInfo t = groupInfoDao.getById(id);
			if (t != null) {
				groupInfoDao.deleteById(id);
			}
		}
	}

	public GroupInfo get(GroupInfoQuery groupInfoQuery) {
		return groupInfoDao.getById(groupInfoQuery.getId());
	}

	public GroupInfo get(String id) {
		return groupInfoDao.getById(id);
	}

	
	public List<GroupInfoQuery> listAll(GroupInfoQuery groupInfoQuery) {
	    List<GroupInfo> list = groupInfoDao.findList(groupInfoQuery);
		List<GroupInfoQuery> listQuery =getQuerysFromEntitys(list) ;
		return listQuery;
	}
	
	
}
