package com.sun.showcase.biz.basic.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.sun.showcase.client.domain.basic.UserResource;
import com.sun.showcase.client.query.basic.UserResourceQuery;
import com.sun.showcase.dao.BaseDao;
@Mapper
public interface UserResourceDao extends BaseDao<UserResource,java.lang.String> {
	 
	public void deleteByResource(String resourceId);
	
	public List<UserResource> findList(UserResourceQuery query);
	
}
