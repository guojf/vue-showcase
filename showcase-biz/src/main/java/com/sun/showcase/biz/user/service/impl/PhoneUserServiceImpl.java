
package com.sun.showcase.biz.user.service.impl;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.groups.Default;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sun.showcase.biz.user.dao.PhoneUserDao;
import com.sun.showcase.biz.user.service.PhoneUserService;
import com.sun.showcase.client.domain.user.PhoneUser;
import com.sun.showcase.client.query.user.PhoneUserQuery;
import com.sun.showcase.client.validator.groups.RegistValidatorGroup;
import com.sun.showcase.pojo.DataGrid;
import com.sun.showcase.pojo.Pager;
import com.sun.showcase.pojo.Result;
import com.sun.showcase.utils.JwtUtils;
import com.sun.showcase.utils.LoginContextHolder;
import com.sun.showcase.utils.StringConstant;
import com.sun.showcase.utils.ValidatorUtils;
@Service("phoneUserService")
@Transactional
public class PhoneUserServiceImpl implements PhoneUserService{
	@Autowired
	private PhoneUserDao phoneUserDao;
	@Autowired
	private PasswordEncoder passwordEncoder;
	@Autowired
	private JwtUtils jwtUtils;
	public void setPhoneUserDao(PhoneUserDao dao) {
		this.phoneUserDao = dao;
	}

	public DataGrid datagrid(PhoneUserQuery phoneUserQuery) {
		DataGrid j = new DataGrid();
		Pager<PhoneUser> pager  = find(phoneUserQuery);
		j.setRows(getQuerysFromEntitys(pager.getRecords()));
		j.setTotal(pager.getTotalRecords());
		return j;
	}

	private List<PhoneUserQuery> getQuerysFromEntitys(List<PhoneUser> phoneUsers) {
		List<PhoneUserQuery> phoneUserQuerys = new ArrayList<PhoneUserQuery>();
		if (phoneUsers != null && phoneUsers.size() > 0) {
			for (PhoneUser tb : phoneUsers) {
				PhoneUserQuery b = new PhoneUserQuery();
				BeanUtils.copyProperties(tb, b);
				phoneUserQuerys.add(b);
			}
		}
		return phoneUserQuerys;
	}

	private Pager<PhoneUser> find(PhoneUserQuery phoneUserQuery) {
		Page<PhoneUser> page = PageHelper.startPage(phoneUserQuery.getPage().intValue(), phoneUserQuery.getRows().intValue());
		List<PhoneUser> list = phoneUserDao.findList(phoneUserQuery);
		Pager<PhoneUser> pager = new Pager<PhoneUser>();
		pager.setRecords(list);
		pager.setTotalRecords(page.getTotal());
		return  pager;
		
	}
	


	public PhoneUser add(PhoneUserQuery phoneUserQuery) {
		PhoneUser t = new PhoneUser();
		BeanUtils.copyProperties(phoneUserQuery, t);
		t.setActiveFlag(StringConstant.ONE);
		t.setCreateDate(new Date());
	    t.setModifiedDate(new Date());
	    t.setPassword(passwordEncoder.encode(phoneUserQuery.getPassword()));
		phoneUserDao.save(t);
		phoneUserQuery.setId(t.getId());
		return t;
		
	}

	public void update(PhoneUserQuery phoneUserQuery) {
		PhoneUser t = phoneUserDao.getById(phoneUserQuery.getId());
	    if(t != null) {
	    	BeanUtils.copyProperties(phoneUserQuery, t);
		    t.setModifiedBy(LoginContextHolder.get().getUserId());
		    t.setModifiedByName(LoginContextHolder.get().getUserName());
		    t.setModifiedDate(new Date());
	    	phoneUserDao.update(t);
		}
	}
	
	/**
	 * 根据条件更新部分字段   新建一个query封装需要更新的字段
	 * searchMap封装更新条件
	 * */
	public void updatePart(PhoneUserQuery phoneUserQuery) {
	    phoneUserDao.updatePart(phoneUserQuery);
	}

	public void delete(java.lang.Long[] ids) {
		phoneUserDao.deleteAc(ids);
	}
	
	//逻辑删除 更新状态位
	public void deletePt(java.lang.Long[] ids) {
		phoneUserDao.deletePt(ids);
	}

	public PhoneUser get(PhoneUserQuery phoneUserQuery) {
		return phoneUserDao.getById(new Long(phoneUserQuery.getId()));
	}

	public PhoneUser get(Long id) {
		return phoneUserDao.getById(id);
	}

	
	public List<PhoneUserQuery> listAll(PhoneUserQuery phoneUserQuery) {
	    List<PhoneUser> list = phoneUserDao.findList(phoneUserQuery);
		List<PhoneUserQuery> listQuery =getQuerysFromEntitys(list) ;
		return listQuery;
	}

	@Override
	public Result login(PhoneUserQuery phoneUserQuery) {
		ValidatorUtils.validate(phoneUserQuery);
		Result res = new Result();
		PhoneUser user = phoneUserDao.getByUserPhone(phoneUserQuery.getUserPhone());
		if(user == null){
			res.setMsg("无效用户!");
			return res;
		}
		if(!passwordEncoder.matches(phoneUserQuery.getPassword(), user.getPassword())){
			res.setMsg("手机号或密码错误!");
			return res;
		}
		String token = jwtUtils.generateToken(user.getId());
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("token", token);
		map.put("expire", jwtUtils.getExpire());
		res.setData(map);
		res.setSuccess(true);
		return res;
	}

	@Override
	public Result regist(PhoneUserQuery phoneUserQuery) {
		ValidatorUtils.validate(phoneUserQuery,RegistValidatorGroup.class,Default.class);
		Result res = new Result();
		PhoneUser user = phoneUserDao.getByUserPhone(phoneUserQuery.getUserPhone());
		if(user != null){
			res.setMsg("该用户已注册!");
			return res;
		}
		this.add(phoneUserQuery);
		res.setSuccess(true);
		return res;
	}

	@Override
	public PhoneUser getByUserPhone(String userPhone) {
		return phoneUserDao.getByUserPhone(userPhone);
	}
	
	
}
