package com.sun.showcase.biz.basic.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.sun.showcase.client.domain.basic.UploadFile;
import com.sun.showcase.client.query.basic.UploadFileQuery;
import com.sun.showcase.dao.BaseDao;

@Mapper
public interface UploadFileDao extends BaseDao<UploadFile,java.lang.Long>{
	/**
	 * 查询列表
	 */
	public List<UploadFile> findList(UploadFileQuery uploadFileQuery);
	
	/**
	 * 根据条件更新部分字段   新建一个query封装需要更新的字段
	 * searchMap封装更新条件
	 * */
	public void updatePart(UploadFileQuery uploadFileQuery);
	/**
	 * 逻辑删除
	 * @param ids
	 */
	public void deletePt(Long[] ids);
	/**
	 * 物理删除
	 * @param ids
	 */
	public void deleteAc(Long[] ids);
	/**
	 * 
	 */
	public List<UploadFile> getByIds(List<Long> list);
}
