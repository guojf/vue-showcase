package com.sun.showcase.biz.basic.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.sun.showcase.client.domain.basic.RoleGroup;
import com.sun.showcase.client.query.basic.RoleGroupQuery;
import com.sun.showcase.dao.BaseDao;
@Mapper
public interface RoleGroupDao extends BaseDao<RoleGroup,java.lang.String>{
	
	public List<RoleGroup> findList(RoleGroupQuery query);
	
	public List<RoleGroup> findPage(RoleGroupQuery query);
	
	public void delete(RoleGroupQuery query);
	
	public RoleGroup getById(RoleGroupQuery query) ;
}
