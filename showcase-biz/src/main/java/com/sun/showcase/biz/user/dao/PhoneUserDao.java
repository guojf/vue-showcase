package com.sun.showcase.biz.user.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.sun.showcase.client.domain.user.PhoneUser;
import com.sun.showcase.client.query.user.PhoneUserQuery;
import com.sun.showcase.dao.BaseDao;

@Mapper
public interface PhoneUserDao extends BaseDao<PhoneUser,java.lang.Long>{
	/**
	 * 查询列表
	 */
	public List<PhoneUser> findList(PhoneUserQuery phoneUserQuery);
	
	/**
	 * 根据条件更新部分字段   新建一个query封装需要更新的字段
	 * searchMap封装更新条件
	 * */
	public void updatePart(PhoneUserQuery phoneUserQuery);
	/**
	 * 逻辑删除
	 * @param ids
	 */
	public void deletePt(Long[] ids);
	/**
	 * 物理删除
	 * @param ids
	 */
	public void deleteAc(Long[] ids);
	/**
	 * 更加手机查询
	 */
	public PhoneUser getByUserPhone(String userPhone);
}
