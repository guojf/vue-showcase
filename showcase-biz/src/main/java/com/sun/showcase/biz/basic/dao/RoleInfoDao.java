package com.sun.showcase.biz.basic.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.sun.showcase.client.domain.basic.RoleInfo;
import com.sun.showcase.client.domain.basic.RoleResource;
import com.sun.showcase.client.query.basic.RoleInfoQuery;
import com.sun.showcase.dao.BaseDao;
@Mapper
public interface RoleInfoDao extends BaseDao<RoleInfo,java.lang.String>{
	
	public List<RoleInfo> findList(RoleInfoQuery query);
	
	public List<RoleInfo> getByName(RoleInfoQuery query);
	//保存角色资源表数据
	public void saveRoleResource(RoleResource roleResource);
	//通过角色id 查询角色资源表的数据
	public List<RoleResource> getRoleResourceByRoleID(RoleInfoQuery roleInfoQuery);
	//删除角色资源表数据
	public Integer delRoleResourcce(String roleId);
	//通过资源id删除角色资源表数据
	public Integer delResourcceRole(String resourceId);
}
