# vue-showcase 

#### 项目介绍
> **前后端不不不分离**的后台管理框架。
> 解决主要痛点：纯后端人员，使用前端框架 vue +element-UI，不需要学习大量的前端技术栈，像使用jquery+easyUI一样。
>

代码生成器地址：https://gitee.com/sunsrc/vue-showcase-generator

#### 技术栈

- spring boot 2.0
- spring security 
- vue 
- element-UI
- layer (弹窗组件)
- thymeleaf
- quartz （也可使用[xxl-job/](http://www.xuxueli.com/xxl-job/)  或 [elastic-job](https://gitee.com/elasticjob/elastic-job)）
- redis (TODO)
- elasticsearch (TODO)
- rabbitMq (TODO) 

##### showcase-auth2权限认证模块

**授权码模式**

1. 获取code

   http://localhost:9999/api/oauth/authorize?client_id=client&redirect_uri=http://baidu.com&response_type=code&scope=user_info

2. 通过code获取access_token

   http://localhost:9999/api/oauth/token?grant_type=authorization_code&client_id=client&client_secret=123456&redirect_uri=http://baidu.com&code=eCYN6g

3. 获取用户信息

   http://localhost:9999/api/authUser/auth_userinfo?access_token=384cfd69-6555-4bfc-bc63-2c4279d8f2ae

其他模块集成权限认证模块，需要一个UserDetailsService

```
	@Bean
	UserDetailsService userDetailsService(){
        return new PhoneUserDetailsServiceImpl();
    }
```



##### showcase-app 移动端接口模块

​	对有@LoginAuth注解的接口登录拦截（http://localhost:9999/api/hello），登录成功返回token, 访问有@LoginAuth注解的接口，header中要携带token。

​	由于token采用jwt方案，存在过期时间，jwt.advance配置提前advance秒生成新的new_token，要求客户端替换token=new_token

application.properties配置说明

```
####密钥#####
jwt.secret=f4e2e52034348f86b67cde581c0f9eb5
####有效时间（秒）#####
jwt.expire=120
####token名#####
jwt.header=token
####提前advance秒生成新的new_token#####
jwt.advance=60
```

- 登录接口http://localhost:9999/api/phone/login?userPhone=13211111111&password=123456
- 注册接口http://localhost:9999/api/phone/regist